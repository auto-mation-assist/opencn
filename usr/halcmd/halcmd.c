/** This file, 'halcmd.c', is a HAL component that provides a simple
 command line interface to the hal.  It is a user space component.
 For detailed instructions, see "man halcmd".
 */

/** Copyright (C) 2003 John Kasunich
 <jmkasunich AT users DOT sourceforge DOT net>

 Other contributers:
 Martin Kuhnle
 <mkuhnle AT users DOT sourceforge DOT net>
 Alex Joni
 <alex_joni AT users DOT sourceforge DOT net>
 Benn Lipkowitz
 <fenn AT users DOT sourceforge DOT net>
 Stephen Wille Padnos
 <swpadnos AT users DOT sourceforge DOT net>
 */

/** This program is free software; you can redistribute it and/or
 modify it under the terms of version 2 of the GNU General
 Public License as published by the Free Software Foundation.
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 THE AUTHORS OF THIS LIBRARY ACCEPT ABSOLUTELY NO LIABILITY FOR
 ANY HARM OR LOSS RESULTING FROM ITS USE.  IT IS _EXTREMELY_ UNWISE
 TO RELY ON SOFTWARE ALONE FOR SAFETY.  Any machinery capable of
 harming persons must have provisions for completely removing power
 from all motors, etc, before persons enter any danger area.  All
 machinery must be designed to comply with local and national safety
 codes, and the authors of this software can not, and do not, take
 any responsibility for such compliance.

 This code was written as part of the EMC HAL project.  For more
 information, go to www.linuxcnc.org.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <fnmatch.h>
#include <search.h>
#include <stdbool.h>
#include <semaphore.h>

#define MAX_EXTEND_LINES 20

typedef enum {
	ERR_NONE = 0x00,
	ERR_NOT_OPEN = 0x01,
	ERR_SECTION_NOT_FOUND = 0x02,
	ERR_TAG_NOT_FOUND = 0x04,
	ERR_CONVERSION = 0x08,
	ERR_LIMITS = 0x10,
	ERR_OVER_EXTENDED = 0x20
} ErrorCode;

#ifndef NO_INI
FILE *halcmd_inifile = NULL;
#endif

static sem_t child_sync;
static bool child_ok;

#include "halcmd_commands.h"

#include <uapi/hal.h>

/***********************************************************************
 *                  LOCAL FUNCTION DECLARATIONS                         *
 ************************************************************************/

/* These functions are used internally by this file.  The code is at
 the end of the file.  */

/***********************************************************************
 *                         GLOBAL VARIABLES                             *
 ************************************************************************/

int comp_id = -1; /* -1 means hal_init() not called yet */
int hal_flag = 0; /* used to indicate that halcmd might have the
 hal mutex, so the sig handler can't just
 exit, instead it must set 'done' */
int halcmd_done = 0; /* used to break out of processing loop */
int scriptmode = 0; /* used to make output "script friendly" (suppress headers) */
int echo_mode = 0;
char comp_name[HAL_NAME_LEN + 1]; /* name for this instance of halcmd */

static void quit(int);

bool checkIfOpen(void) {
	if (halcmd_inifile != NULL)
		return (true);

	return ERR_NOT_OPEN;
}

/*! Expands the tilde to $(HOME) and concatenates file to it. If the first char
 If file does not start with ~/, file will be copied into path as-is.

 @param the input filename

 @param pointer for returning the resulting expanded name

 */
int tildeExpansion(const char *file, char *path, size_t size) {
	char *home;

	int res = snprintf(path, size, "%s", file);
	if (res < 0 || (size_t) res >= size)
		return ERR_CONVERSION;

	if (strlen(file) < 2 || !(file[0] == '~' && file[1] == '/'))
		/* no tilde expansion required, or unsupported
		 tilde expansion type requested */
		return ERR_NONE;

	home = getenv("HOME");
	if (!home)
		return ERR_CONVERSION;

	res = snprintf(path, size, "%s%s", home, file + 1);
	if (res < 0 || (size_t) res >= size)
		return ERR_CONVERSION;

	return ERR_NONE;
}

/* Return TRUE if the line has a line-ending problem */
static bool check_line_endings(const char *s) {
	if (!s)
		return false;
	for (; *s; s++) {
		if (*s == '\r') {
			char c = s[1];
			if (c == '\n' || c == '\0') {
				static bool warned = 0;
				if (!warned)
					fprintf(stderr, "inifile: warning: File contains DOS-style line endings.\n");
				warned = true;
				continue;
			}
			fprintf(stderr, "inifile: error: File contains ambiguous carriage returns\n");
			return true;
		}
	}
	return false;
}

/*! Finds the first non-white character on a new line and returns a
 pointer. Ignores any line that starts with a comment char i.e. a ';' or
 '#'.

 @return NULL if not found or a valid pointer.

 Called By: find() and section() only. */
char *skipWhite(const char *string) {
	while (true) {
		if (*string == 0) {
			return (NULL);
		}

		if ((*string == ';') || (*string == '#')) {
			return (NULL);
		}

		if (*string != ' ' && *string != '\t' && *string != '\r' && *string != '\n') {
			return ((char *) string);
		}

		string++;
	}
}

/*! Ignoring any tabs, spaces or other white spaces, finds the first
 character after the '=' delimiter.

 @param string Pointer to the tag

 @return NULL or pointer to first non-white char after the delimiter

 Called By: find() and section() only. */
char *afterEqual(const char *string) {
	const char *spot = string;

	for (;;) {
		if (*spot == '=') {
			/* = is there-- return next non-white, or NULL if not there */
			for (;;) {
				spot++;
				if (0 == *spot) {
					/* ran out */
					return (NULL);
				} else if (*spot != ' ' && *spot != '\t' && *spot != '\r' && *spot != '\n') {
					/* matched! */
					return ((char *) spot);
				} else {
					/* keep going for the text */
					continue;
				}
			}
		} else if (*spot == 0) {
			/* end of string */
			return (NULL);
		} else {
			/* haven't seen '=' yet-- keep going */
			spot++;
			continue;
		}
	}
}

/*! Finds the nth tag in section.

 @param tag Entry in the ini file to find.

 @param section The section to look for the tag.

 @param num (optionally) the Nth occurrence of the tag.

 @return pointer to the the variable after the '=' delimiter */
const char *iniFind(FILE *fp, const char *tag, const char *section, int *lineno) {
	/* WTF, return a pointer to the middle of a local buffer? */
	/* FIX: this is totally non-reentrant. */
	static char line[LINELEN + 2] = ""; /* 1 for newline, 1 for NULL */
	char bracketSection[LINELEN + 2] = "";
	char *nonWhite;
	int newLinePos; /* position of newline to strip */
	int len;
	char tagEnd;
	char *valueString;
	char *endValueString;

	char eline[(LINELEN + 2) * (MAX_EXTEND_LINES + 1)];
	char *elineptr;
	char *elinenext = NULL;
	int extend_ct = 0;

	unsigned int lineNo;
	int num = 1;

	/* For exceptions. */
	lineNo = 0;

	/* check valid file */
	if (!checkIfOpen())
		return (NULL);

	/* start from beginning */
	rewind(halcmd_inifile);

	/* check for section first-- if it's non-NULL, then position file at
	 line after [section] */
	if (section != NULL) {
		sprintf(bracketSection, "[%s]", section);

		/* find [section], and position halcmd_inifile just after it */
		while (!feof(halcmd_inifile)) {

			if (NULL == fgets(line, LINELEN + 1, halcmd_inifile)) {
				/* got to end of file without finding it */
				printf("%s: error %d\n", __func__, ERR_SECTION_NOT_FOUND);
				return NULL;
			}

			if (check_line_endings(line)) {
				printf("%s: error %d\n", __func__, ERR_CONVERSION);
				return NULL;
			}

			/* got a line */
			lineNo++;

			/* strip off newline */
			newLinePos = strlen(line) - 1; /* newline is on back from 0 */
			if (newLinePos < 0) {
				newLinePos = 0;
			}
			if (line[newLinePos] == '\n') {
				line[newLinePos] = 0; /* make the newline 0 */
			}

			if (NULL == (nonWhite = skipWhite(line))) {
				/* blank line-- skip */
				continue;
			}

			/* not a blank line, and nonwhite is first char */
			if (strncmp(bracketSection, nonWhite, strlen(bracketSection)) != 0) {
				/* not on this line */
				continue;
			}

			/* it matches-- halcmd_inifile is now set up for search on tag */
			break;
		}
	}

	while (!feof(halcmd_inifile)) {
		/* check for end of file */
		if (NULL == fgets(line, LINELEN + 1, (FILE *) halcmd_inifile))
			/* got to end of file without finding it */
			/* ERR_TAG_NOT_FOUND); */
			return (NULL);

		if (check_line_endings(line))
			/* ERR_CONVERSION */
			return (NULL);

		/* got a line */
		lineNo++;

		/* strip off newline */
		newLinePos = strlen(line) - 1; /* newline is on back from 0 */
		if (newLinePos < 0) {
			newLinePos = 0;
		}
		if (line[newLinePos] == '\n') {
			line[newLinePos] = 0; /* make the newline 0 */
		}
		// honor backslash (\) as line-end escape
		if (newLinePos > 0 && line[newLinePos - 1] == '\\') {
			newLinePos = newLinePos - 1;
			line[newLinePos] = 0;
			if (!extend_ct) {
				elineptr = (char*) eline; //first time
				strncpy(elineptr, line, newLinePos);
				elinenext = elineptr + newLinePos;
			} else {
				strncpy(elinenext, line, newLinePos);
				elinenext = elinenext + newLinePos;
			}
			*elinenext = 0;
			extend_ct++;
			if (extend_ct > MAX_EXTEND_LINES) {
				fprintf(stderr, "INIFILE lineno=%d:Too many backslash line extends (limit=%d)\n", lineNo,
				MAX_EXTEND_LINES);
				/* ERR_OVER_EXTENDED */
				return NULL;
			}
			continue; /* get next line to extend */
		} else {
			if (extend_ct) {
				strncpy(elinenext, line, newLinePos);
				elinenext = elinenext + newLinePos;
				*elinenext = 0;
			}
		}
		if (!extend_ct) {
			elineptr = (char*) line;
		}
		extend_ct = 0;

		/* skip leading whitespace */
		if ((nonWhite = skipWhite(elineptr)) == NULL) {
			/* blank line-- skip */
			continue;
		}

		/* check for '[' char-- if so, it's a section tag, and we're out
		 of our section */
		if ((section != NULL) && nonWhite[0] == '[')
			/* ERR_TAG_NOT_FOUND */
			return (NULL);

		len = strlen(tag);
		if (strncmp(tag, nonWhite, len) != 0) {
			/* not on this line */
			continue;
		}

		/* it matches the first part of the string-- if whitespace or = is
		 next char then call it a match */
		tagEnd = nonWhite[len];
		if (tagEnd == ' ' || tagEnd == '\r' || tagEnd == '\t' || tagEnd == '\n' || tagEnd == '=') {
			/* it matches-- return string after =, or NULL */
			if (--num > 0) {
				/* Not looking for this one, so skip it... */
				continue;
			}
			nonWhite += len;
			valueString = afterEqual(nonWhite);
			/* Eliminate white space at the end of a line also. */
			if (NULL == valueString)
				/* ERR_TAG_NOT_FOUND */
				return (NULL);

			endValueString = valueString + strlen(valueString) - 1;
			while (*endValueString == ' ' || *endValueString == '\t' || *endValueString == '\r') {
				*endValueString = 0;
				endValueString--;
			}
			if (lineno)
				*lineno = lineNo;
			return (valueString);
		}
		/* else continue */
	}

	return (NULL);
}

int halcmd_startup(int quiet) {
	int msg_lvl_save = rtapi_get_msg_level();

	/* register signal handlers - if the process is killed
	 we need to call hal_exit() to free the shared memory */
	signal(SIGINT, quit);
	signal(SIGTERM, quit);
	signal(SIGPIPE, SIG_IGN);

	/* at this point all options are parsed, connect to HAL */
	/* create a unique module name, to allow for multiple halcmd's */
	snprintf(comp_name, sizeof(comp_name), "halcmd%d", getpid());

	/* tell the signal handler that we might have the mutex */
	hal_flag = 1;
	if (quiet)
		rtapi_set_msg_level(RTAPI_MSG_NONE);
#if 0
	/* connect to the HAL */
	comp_id = hal_init(comp_name);
#endif
	if (quiet)
		rtapi_set_msg_level(msg_lvl_save);

	/* done with mutex */
	hal_flag = 0;
#if 0
	/* check result */
	if (comp_id < 0) {
		if (!quiet) {
			fprintf(stderr, "halcmd: hal_init() failed: %d\n", comp_id);
			fprintf(stderr, "NOTE: 'rtapi' kernel module must be loaded\n");
		}
		return -EINVAL;
	}
	hal_ready(comp_id);
#endif

	return 0;
}

void halcmd_shutdown(void) {
	/* tell the signal handler we might have the mutex */
	hal_flag = 1;

	close(devfd);

#if 0
	hal_exit(comp_id);
#endif
}

/* parse_cmd() decides which command has been invoked, gathers any
 arguments that are needed, and calls a function to execute the
 command.
 */

#define FUNCT(x) ((halcmd_func_t)x)

struct halcmd_command halcmd_commands[] = { { "setp", FUNCT(do_setp_cmd), A_TWO },
					    { "net", FUNCT(do_net_cmd), A_ONE | A_PLUS | A_REMOVE_ARROWS },
					    { "load", FUNCT(do_load_cmd), A_ONE | A_PLUS },
					    { "source", FUNCT(do_source_cmd), A_ONE | A_TILDE},
					    { "show", FUNCT(do_show_cmd), A_ONE | A_OPTIONAL | A_PLUS},
					    { "addf", FUNCT(do_addf_cmd), A_TWO | A_PLUS},
					    { "start", FUNCT(do_start_cmd), A_ZERO},
					    { "stop", FUNCT(do_stop_cmd), A_ZERO},
					  };

#if 0 /* opencn - incrementally adding... */
struct halcmd_command halcmd_commands[] = {  {"alias", FUNCT(do_alias_cmd),
		A_THREE}, {"delf", FUNCT(do_delf_cmd), A_TWO | A_OPTIONAL}, {
		"delsig", FUNCT(do_delsig_cmd), A_ONE}, {"echo", FUNCT(
										do_echo_cmd), A_ZERO}, {"getp", FUNCT(do_getp_cmd), A_ONE}, {
		"gets", FUNCT(do_gets_cmd), A_ONE}, {"ptype", FUNCT(
										do_ptype_cmd), A_ONE}, {"stype", FUNCT(do_stype_cmd), A_ONE}, {
		"help", FUNCT(do_help_cmd), A_ONE | A_OPTIONAL}, {"linkpp",
		FUNCT(do_linkpp_cmd), A_TWO | A_REMOVE_ARROWS}, {"linkps",
		FUNCT(do_linkps_cmd), A_TWO | A_REMOVE_ARROWS}, {"linksp",
		FUNCT(do_linksp_cmd), A_TWO | A_REMOVE_ARROWS}, {"list", FUNCT(
										do_list_cmd), A_ONE | A_PLUS}, {"loadrt", FUNCT(do_loadrt_cmd),
		A_ONE | A_PLUS}, {"loadusr", FUNCT(do_loadusr_cmd), A_PLUS
		| A_TILDE}, {"lock", FUNCT(do_lock_cmd), A_ONE | A_OPTIONAL}, {
		"net", FUNCT(do_net_cmd), A_ONE | A_PLUS | A_REMOVE_ARROWS}, {
		"newsig", FUNCT(do_newsig_cmd), A_TWO}, {"save", FUNCT(
										do_save_cmd), A_TWO | A_OPTIONAL | A_TILDE}, {
		"setexact_for_test_suite_only", FUNCT(do_setexact_cmd), A_ZERO},
	{	"setp", FUNCT(do_setp_cmd), A_TWO},
	{	"sets", FUNCT(do_sets_cmd), A_TWO},
	{	"source", FUNCT(do_source_cmd), A_ONE | A_TILDE},
	{	"start", FUNCT(do_start_cmd), A_ZERO},
	{	"status", FUNCT(do_status_cmd), A_ONE | A_OPTIONAL},
	{	"unalias", FUNCT(do_unalias_cmd), A_TWO},
	{	"unecho", FUNCT(do_unecho_cmd), A_ZERO},
	{	"unlinkp", FUNCT(do_unlinkp_cmd), A_ONE},
	{	"unload", FUNCT(do_unload_cmd), A_ONE},
	{	"unloadrt", FUNCT(do_unloadrt_cmd), A_ONE},
	{	"unloadusr", FUNCT(do_unloadusr_cmd), A_ONE},
	{	"unlock", FUNCT(do_unlock_cmd), A_ONE | A_OPTIONAL},
	{	"waitusr", FUNCT(do_waitusr_cmd), A_ONE},};

#endif

int halcmd_ncommands = (sizeof(halcmd_commands) / sizeof(halcmd_commands[0]));

static int sort_command(const void *a, const void *b) {
	const struct halcmd_command *ca = a, *cb = b;
	return strcmp(ca->name, cb->name);
}

static int compare_command(const void *namep, const void *commandp) {
	const char *name = namep;
	const struct halcmd_command *command = commandp;
	return strcmp(name, command->name);
}

pid_t hal_systemv_nowait(char * const argv[]) {
	pid_t pid;
	int n;

	/* now we need to fork, and then exec .... */

#if 0
	/* disconnect from the HAL shmem area before forking */
	hal_exit(comp_id);
#endif

	comp_id = 0;
	/* now the fork() */
	pid = fork();
	if (pid < 0) {
		/* fork failed */
		halcmd_error("fork() failed\n");
		return -1;
	}
	if (pid == 0) {
		/* child process */
		/* print debugging info if "very verbose" (-V) */
		for (n = 0; argv[n] != NULL; n++) {
			rtapi_print_msg(RTAPI_MSG_DBG, "%s ", argv[n]);
		}
		if (n == 0) {
			halcmd_error("hal_systemv_nowait: empty argv array passed in\n");
			exit(1);
		}
		rtapi_print_msg(RTAPI_MSG_DBG, "\n");

		/* call execv() to invoke command */
		execvp(argv[0], argv);
		/* should never get here */
		halcmd_error("execv(%s): %s\n", argv[0], strerror(errno));

		exit(1);
	}
	/* parent process */

#if 0
	/* reconnect to the HAL shmem area */
	comp_id = hal_init(comp_name);
#endif

	return pid;
}


void wait_for_child(int sig) {

	if (sig == SIGUSR1) {
		sem_post(&child_sync);
		child_ok = true;
	}

	if (sig == SIGUSR2) {
		sem_post(&child_sync);
		child_ok = false;
	}
}


/*
 * Used to synchronize parent and child process without waiting for child end.
 */
bool checkpoint(void) {

	signal(SIGUSR1, wait_for_child);
	signal(SIGUSR2, wait_for_child);

	sem_init(&child_sync, 0, 0);

	sem_wait(&child_sync);

	sem_destroy(&child_sync);

	return child_ok;
}

int hal_systemv(char * const argv[]) {
	pid_t pid;
	int status;
	int retval = 0;

	/* do the fork */
	pid = hal_systemv_nowait(argv);

	/*
	 * We do a synchronisation checkpoint with our child in order to wait for various initializations
	 * within the kernel and other. We expect to receive a SIGUSR1 from the child to inform us that we can proceed.
	 */

	sem_init(&child_sync, 0, 0);

	printf("%s: waiting for child %s\n", __func__, argv[0]);

	if (!checkpoint()) {
		exit(-1);
	}

	printf("%s: OK, we can go ahead...\n", __func__);

#if 0
	/* this is the parent process, wait for child to end */
	retval = waitpid(pid, &status, 0);
	if (comp_id < 0) {
		fprintf(stderr, "halcmd: hal_init() failed after systemv: %d\n", comp_id);
		exit(-1);
	}
#endif

#if 0
	hal_ready(comp_id);
#endif

	/* check result of waitpid() */
	if (retval < 0) {
		halcmd_error("waitpid(%d) failed: %s\n", pid, strerror(errno));
		return -1;
	}
	if (WIFEXITED(status) == 0) {
		halcmd_error("child did not exit normally\n");
		return -1;
	}
	retval = WEXITSTATUS(status);
	if (retval != 0) {
		halcmd_error("exit value: %d\n", retval);
		return -1;
	}
	return 0;
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/* signal handler */
static void quit(int sig) {
	if (hal_flag) {
		/* this process might have the hal mutex, so just set the
		 'done' flag and return, exit after mutex work finishes */
		halcmd_done = 1;
	} else {
		/* don't have to worry about the mutex, but if we just
		 return, we might return into the fgets() and wait
		 all day instead of exiting.  So we exit from here. */
#if 0
		if (comp_id > 0) {
			hal_exit(comp_id);
		}
#endif

		_exit(1);
	}
}
/* main() is responsible for parsing command line options, and then
 parsing either a single command from the command line or a series
 of commands from a file or standard input.  It breaks the command[s]
 into tokens, and passes them to parse_cmd() which does the actual
 work for each command.
 */

static int count_args(char **argv) {
	int i = 0;
	while (argv[i] && argv[i][0])
		i++;
	return i;
}

#define ARG(i) (argc > i ? argv[i] : 0)
#define REST(i) (argc > i ? argv + i : argv + argc)

static int parse_cmd1(char **argv) {
	struct halcmd_command *command = bsearch(argv[0], halcmd_commands, halcmd_ncommands, sizeof(struct halcmd_command), compare_command);
	int argc = count_args(argv);

	if (argc == 0)
		return 0;

	if (!command) {
		// special case: pin/param = newvalue
		if (argc == 3 && !strcmp(argv[1], "=")) {
			return do_setp_cmd(argv[0], argv[2]);
		} else {
			halcmd_error("Unknown command '%s'\n", argv[0]);
			return -EINVAL;
		}
	} else {
		int result = -EINVAL;
		int is_optional = command->type & A_OPTIONAL, is_plus = command->type & A_PLUS, nargs = command->type & 0xff, posargs;

		if (command->type & A_REMOVE_ARROWS) {
			int s, d;
			for (s = d = 0; argv[s] && argv[s][0]; s++) {
				if (!strcmp(argv[s], "<=") || !strcmp(argv[s], "=>") || !strcmp(argv[s], "<=>")) {
					continue;
				} else {
					argv[d++] = argv[s];
				}
			}
			argv[d] = 0;
			argc = d;
		}

		posargs = argc - 1;
		if (posargs < nargs && !is_optional) {
			halcmd_error("%s requires %s%d arguments, %d given\n", command->name, is_plus ? "at least " : "", nargs,
											posargs);
			return -EINVAL;
		}

		if (posargs > nargs && !is_plus) {
			halcmd_error("%s requires %s%d arguments, %d given\n", command->name, is_optional ? "at most " : "",
											nargs, posargs);
			return -EINVAL;
		}

#ifndef NO_INI
		if (command->type & A_TILDE) {
			int i;
			for (i = 0; i < argc; i++) {
				char *buf = malloc(LINELEN);

				tildeExpansion(argv[i], buf, LINELEN);

				argv[i] = buf;
			}
		}
#endif
		if (!strcmp(command->name, "echo")) {
			echo_mode = 1;
		}
		if (!strcmp(command->name, "unecho")) {
			echo_mode = 0;
		}
		switch (nargs | is_plus) {
			case A_ZERO: {
				result = command->func();
				break;
			}

			case A_PLUS: {
				int (*f)(char **args) = (int(*)(char**))command->func;
				result = f(REST(1));
				break;
			}

			case A_ONE: {
				int(*f)(char *arg) = (int(*)(char*))command->func;
				result = f(ARG(1));
				break;
			}

			case A_ONE | A_PLUS: {
				int(*f)(char *arg, char **rest) =
				(int(*)(char*,char**))command->func;
				result = f(ARG(1), REST(2));
				break;
			}

			case A_TWO: {
				int(*f)(char *arg, char *arg2) =
				(int(*)(char*,char*))command->func;
				result = f(ARG(1), ARG(2));
				break;
			}

			case A_TWO | A_PLUS: {
				int(*f)(char *arg, char *arg2, char **rest) =
				(int(*)(char*,char*,char**))command->func;
				result = f(ARG(1), ARG(2), REST(3));
				break;
			}

			case A_THREE: {
				int(*f)(char *arg, char *arg2, char *arg3) =
				(int(*)(char*,char*,char*))command->func;
				result = f(ARG(1), ARG(2), ARG(3));
				break;
			}

			case A_THREE | A_PLUS: {
				int(*f)(char *arg, char *arg2, char *arg3, char **rest) =
				(int(*)(char*,char*,char*,char**))command->func;
				result = f(ARG(1), ARG(2), ARG(3), REST(4));
				break;
			}

			default:
			halcmd_error("BUG: unchandled case: command=%s type=0x%x", command->name, command->type);
			result = -EINVAL;
		}

#ifndef NO_TILDE
		if (command->type & A_TILDE) {
			int i;
			for (i = 0; i < argc; i++) {
				free(argv[i]);
			}
		}
#endif

		return result;
	}
}

int halcmd_parse_cmd(char *tokens[]) {
	int retval;
	static int first_time = 1;

	if (first_time) {
		/* ensure that commands is sorted when it is searched later */
		qsort(halcmd_commands, halcmd_ncommands, sizeof(struct halcmd_command), sort_command);
		first_time = 0;
	}

	hal_flag = 1;
	retval = parse_cmd1(tokens);
	hal_flag = 0;

	return retval;
}

/* tokenize() sets an array of pointers to each non-whitespace
 token in the input line.  It expects that variable substitution
 and comment removal have already been done, and that any
 trailing newline has been removed.
 */
static int tokenize(char *cmd_buf, char **tokens) {
	enum {
		BETWEEN_TOKENS, IN_TOKEN, SINGLE_QUOTE, DOUBLE_QUOTE, END_OF_LINE
	} state;
	char *cp1;
	int m;
	/* convert a line of text into individual tokens */
	m = 0;
	cp1 = cmd_buf;
	state = BETWEEN_TOKENS;
	while (m < MAX_TOK) {
		if (*cp1 == '\r') {
			char nextc = *(cp1 + 1);
			if (nextc == '\n' || nextc == '\0') {
				static int warned = 0;
				if (!warned)
					halcmd_warning("File contains DOS-style line endings.\n");
				warned = 1;
			} else {
				halcmd_error("File contains embedded carriage returns.\n");
				return -1;
			}
		}
		switch (state) {
			case BETWEEN_TOKENS:
				if (*cp1 == '\0') {
					/* end of the line */
					state = END_OF_LINE;
				} else if (isspace(*cp1)) {
					/* whitespace, skip it */
					cp1++;
				} else if (*cp1 == '\'') {
					/* start of a quoted string and a new token */
					tokens[m] = cp1++;
					state = SINGLE_QUOTE;
				} else if (*cp1 == '\"') {
					/* start of a quoted string and a new token */
					tokens[m] = cp1++;
					state = DOUBLE_QUOTE;
				} else {
					/* first char of a token */
					tokens[m] = cp1++;
					state = IN_TOKEN;
				}
				break;
			case IN_TOKEN:
				if (*cp1 == '\0') {
					/* end of the line */
					m++;
					state = END_OF_LINE;
				} else if (*cp1 == '\'') {
					/* start of a quoted string */
					cp1++;
					state = SINGLE_QUOTE;
				} else if (*cp1 == '\"') {
					/* start of a quoted string */
					cp1++;
					state = DOUBLE_QUOTE;
				} else if (isspace(*cp1)) {
					/* end of the current token */
					*cp1++ = '\0';
					m++;
					state = BETWEEN_TOKENS;
				} else {
					/* ordinary character */
					cp1++;
				}
				break;
			case SINGLE_QUOTE:
				if (*cp1 == '\0') {
					/* end of the line */
					m++;
					state = END_OF_LINE;
				} else if (*cp1 == '\'') {
					/* end of quoted string */
					cp1++;
					state = IN_TOKEN;
				} else {
					/* ordinary character */
					cp1++;
				}
				break;
			case DOUBLE_QUOTE:
				if (*cp1 == '\0') {
					/* end of the line */
					m++;
					state = END_OF_LINE;
				} else if (*cp1 == '\"') {
					/* end of quoted string */
					cp1++;
					state = IN_TOKEN;
				} else {
					/* ordinary character, copy to buffer */
					cp1++;
				}
				break;
			case END_OF_LINE:
				tokens[m++] = cp1;
				break;
			default:
				/* should never get here */
				state = BETWEEN_TOKENS;
		}
	}
	if (state != END_OF_LINE) {
		halcmd_error("too many tokens on line\n");
		return -1;
	}
	return 0;
}

/* strip_comments() removes any comment in the string.  It also
 removes any trailing newline.  Single- or double-quoted strings
 are respected - a '#' inside a quoted string does not indicate
 a comment.
 */
static int strip_comments(char *buf) {
	enum {
		NORMAL, SINGLE_QUOTE, DOUBLE_QUOTE
	} state;
	char *cp1;

	cp1 = buf;
	state = NORMAL;
	while (1) {
		switch (state) {
			case NORMAL:
				if ((*cp1 == '#') || (*cp1 == '\n') || (*cp1 == '\0')) {
					/* end of the line */
					*cp1 = '\0';
					return 0;
				} else if (*cp1 == '\'') {
					/* start of a quoted string */
					cp1++;
					state = SINGLE_QUOTE;
				} else if (*cp1 == '\"') {
					/* start of a quoted string */
					cp1++;
					state = DOUBLE_QUOTE;
				} else {
					/* normal character */
					cp1++;
				}
				break;
			case SINGLE_QUOTE:
				if ((*cp1 == '\n') || (*cp1 == '\0')) {
					/* end of the line, unterminated quoted string */
					*cp1 = '\0';
					return -1;
				} else if (*cp1 == '\'') {
					/* end of quoted string */
					cp1++;
					state = NORMAL;
				} else {
					/* ordinary character */
					cp1++;
				}
				break;
			case DOUBLE_QUOTE:
				if ((*cp1 == '\n') || (*cp1 == '\0')) {
					/* end of the line, unterminated quoted string */
					*cp1 = '\0';
					return -1;
				} else if (*cp1 == '\"') {
					/* end of quoted string */
					cp1++;
					state = NORMAL;
				} else {
					/* ordinary character */
					cp1++;
				}
				break;
			default:
				/* should never get here */
				state = NORMAL;
		}
	}
}

/* strlimcpy:
 a wrapper for strncpy which has two improvements:
 1) takes two variables for limits, and uses the lower of the two for the limit
 2) subtracts the number of characters copied from the second limit

 This allows one to keep track of remaining buffer size and use the correct limits
 simply

 Parameters are:
 pointer to destination string pointer
 source string pointer
 source length to copy
 pointer to remaining destination space

 return value:
 -1 if the copy was limited by destination space remaining
 0 otherwise
 Side Effects:
 the value of *destspace will be reduced by the copied length
 The value of *dest will will be advanced by the copied length

 */
static int strlimcpy(char **dest, char *src, int srclen, int *destspace) {
	if (*destspace < srclen) {
		return -1;
	} else {
		strncpy(*dest, src, srclen);
		(*dest)[srclen] = '\0';
		srclen = strlen(*dest); /* use the actual number of bytes copied */
		*destspace -= srclen;
		*dest += srclen;
	}
	return 0;
}

/* replace_vars:
 replaces environment and ini var references in source_str.
 This routine does string replacement only
 return value is 0 on success (ie, no variable lookups failed)
 The source string is not modified

 In case of an error, dest_str will contain everything that was
 successfully replaced, but will not contain anything past the
 var that caused the error.

 environment vars are in the following formats:
 $envvar<whitespace>
 $(envvar)<any char>

 ini vars are in the following formats:
 [SECTION]VAR<whitespace>
 [SECTION](VAR)<any char>

 return values:
 0	success
 -1	missing close parenthesis
 -2	null variable name (either environment or ini varname)
 -3	missing close square bracket
 -4	environment variable not found
 -5	ini variable not found
 -6	replacement would overflow output buffer
 -7	var name exceeds limit
 */
static int replace_vars(char *source_str, char *dest_str, int max_chars, char **detail) {
	int retval = 0, loopcount = 0;
	int next_delim, remaining, buf_space;
	char *replacement, sec[128], var[128];
	static char info[256];
	char *sp = source_str, *dp = dest_str, *secP, *varP;
	const char * words = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-_{}";

	dest_str[max_chars - 1] = '\0'; /* make sure there's a terminator */
	*dest_str = '\0'; /* return null string if input is null string */
	buf_space = max_chars - 1; /* leave space for terminating null */

	while ((remaining = strlen(sp)) > 0) {
		loopcount++;
		next_delim = strcspn(sp, "$[");
		if (strlimcpy(&dp, sp, next_delim, &buf_space) < 0)
			return -6;
		sp += next_delim;
		if (next_delim < remaining) { /* a new delimiter was found */
			switch (*sp++) { /* this is the found delimiter, since sp was incremented */
				case '$':
					varP = sp;
					if (*sp == '(') { /* look for a parenthesized var */
						varP = ++sp;
						next_delim = strcspn(varP, ")");
						if (next_delim >= strlen(varP)) /* error - no matching parens */
							return -1;
						sp++;
					} else
						next_delim = strspn(varP, words);
					if (next_delim == 0) /* null var name */
						return -2;
					if (next_delim > 127) /* var name too long */
						return -7;
					strncpy(var, varP, next_delim);
					var[next_delim] = '\0';
					replacement = getenv(var);
					if (replacement == NULL) {
						snprintf(info, sizeof(info), "%s", var);
						*detail = info;
						return -4;
					}
					if (strlimcpy(&dp, replacement, strlen(replacement), &buf_space) < 0)
						return -6;
					sp += next_delim;
					break;
				case '[':
					secP = sp;
					next_delim = strcspn(secP, "]");
					if (next_delim >= strlen(secP)) /* error - no matching square bracket */
						return -3;
					if (next_delim > 127) /* section name too long */
						return -7;
					strncpy(sec, secP, next_delim);
					sec[next_delim] = '\0';
					sp += next_delim + 1;
					varP = sp; /* should point past the ']' now */
					if (*sp == '(') { /* look for a parenthesized var */
						varP = ++sp;
						next_delim = strcspn(varP, ")");
						if (next_delim > strlen(varP)) /* error - no matching parens */
							return -1;
						sp++;
					} else
						next_delim = strspn(varP, words);
					if (next_delim == 0)
						return -2;
					if (next_delim > 127) /* var name too long */
						return -7;
					strncpy(var, varP, next_delim);
					var[next_delim] = '\0';
					if (strlen(sec) > 0) {
						/* get value from ini file */
						/* cast to char ptr, we are discarding the 'const' */
						replacement = (char *) iniFind(halcmd_inifile, var, sec, NULL);
					} else {
						/* no section specified */
						replacement = (char *) iniFind(halcmd_inifile, var, NULL, NULL);
					}
					if (replacement == NULL) {
						*detail = info;
						snprintf(info, sizeof(info), "[%s]%s", sec, var);
						return -5;
					}
					if (strlimcpy(&dp, replacement, strlen(replacement), &buf_space) < 0)
						return -6;
					sp += next_delim;
					break;
			}
		}
	}
	return retval;
}

static const char *replace_errors[] = { "Missing close parenthesis.\n", "Empty variable name.\n",
								"Missing close square bracket.\n",
								"Environment variable '%s' not found.\n",
								"Ini variable '%s' not found.\n", "Line too long.\n",
								"Variable name too long.\n", };

int halcmd_preprocess_line(char *line, char **tokens) {
	int retval;
	char *detail = NULL;
	static char cmd_buf[2 * MAX_CMD_LEN];

	/* strip comments and trailing newline (if any) */
	retval = strip_comments(line);
	if (retval != 0) {
		halcmd_error("unterminated quoted string\n");
		return -1;
	}
	/* copy to cmd_buf while doing variable replacements */
	retval = replace_vars(line, cmd_buf, sizeof(cmd_buf) - 2, &detail);
	if (retval != 0) {
		if ((retval < 0) && (retval >= -7)) { /* print better replacement errors */
			if (detail) {
				halcmd_error(replace_errors[(-retval) - 1], detail);
			} else {
				halcmd_error("%s", replace_errors[(-retval) - 1]);
			}
		} else {
			halcmd_error("unknown variable replacement error\n");
		}
		return -2;
	}
	/* split cmd_buff into tokens */
	retval = tokenize(cmd_buf, tokens);
	if (retval != 0) {
		return -3;
	}
	/* tokens[] contains MAX_TOK+1 elements so there is always
	 at least one empty one at the end... make it empty now */
	tokens[MAX_TOK] = "";
	return 0;
}

int halcmd_parse_line(char *line) {
	char *tokens[MAX_TOK + 1];
	int result = halcmd_preprocess_line(line, tokens);
	if (result < 0)
		return result;
	return halcmd_parse_cmd(tokens);
}

static int linenumber = 0;
static char *filename = NULL;

void halcmd_set_filename(const char *new_filename) {
	if (filename)
		free(filename);
	filename = strdup(new_filename);
}
const char *halcmd_get_filename(void) {
	return filename;
}

void halcmd_set_linenumber(int new_linenumber) {
	linenumber = new_linenumber;
}
int halcmd_get_linenumber(void) {
	return linenumber;
}

