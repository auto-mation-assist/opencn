add_executable(halcmd halcmd.c halcmd_main.c halcmd_commands.c halcmd_completion.c)

target_link_libraries(halcmd opencn)
set_target_properties(halcmd PROPERTIES COMPILE_FLAGS "-Wall -Wno-long-long -Wno-variadic-macros") # -pedantic -Wextra -Wcast-align -Wcast-qual -Wmissing-declarations -Wsign-conversion -Wswitch-default -Wundef -Werror -W no-unused
set_target_properties(halcmd PROPERTIES LINK_FLAGS "-lc -lreadline -lncurses -lpthread")
