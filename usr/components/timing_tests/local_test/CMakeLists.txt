cmake_minimum_required(VERSION 3.7.0)

project(timing_tests C CXX)

find_package(GSL REQUIRED)

file(GLOB MATLAB_GEN_SRC
    ${timing_tests_SOURCE_DIR}/../../../../matlab/gen_matlab_us/*.c
    )

#message(${MATLAB_GEN_SRC})
#list(REMOVE_ITEM MATLAB_GEN_SRC ${timing_tests_SOURCE_DIR}/../../../matlab/gen_matlab_us/FeedoptPlan.c)
#list(REMOVE_ITEM MATLAB_GEN_SRC ${timing_tests_SOURCE_DIR}/../../../matlab/gen_matlab_us/FeedoptTypes_initialize.c)
#list(REMOVE_ITEM MATLAB_GEN_SRC ${timing_tests_SOURCE_DIR}/../../../matlab/gen_matlab_us/SmoothCurvStructs.c)
#list(REMOVE_ITEM MATLAB_GEN_SRC ${timing_tests_SOURCE_DIR}/../../../matlab/gen_matlab_us/Queue.c)

set(BLA_VENDOR OpenBLAS)
find_package(BLAS REQUIRED)

set(MATLAB_SRC
    ${timing_tests_SOURCE_DIR}/../../../../matlab/src/c_spline.c
    ${timing_tests_SOURCE_DIR}/../../../../matlab/src/functions.c
    ${timing_tests_SOURCE_DIR}/../../../../matlab/src/cpp_simplex.cpp)

add_executable(timing_tests ../main.cpp  ${MATLAB_GEN_SRC} ${MATLAB_SRC})
target_link_libraries(timing_tests m Clp CoinUtils GSL::gsl ${BLAS_LIBRARIES} -L/usr/local/lib)
target_compile_options(timing_tests PUBLIC -march=native -Ofast)
target_compile_definitions(timing_tests PUBLIC -D_POSIX_C_SOURCE=199309L -DUSE_FULL_MATLAB_GEN -DCLP_USE_OPENBLAS=4)
target_include_directories(timing_tests
    PUBLIC ${PROJECT_SOURCE_DIR}
    ${timing_tests_SOURCE_DIR}/../../../../matlab/gen_matlab_us
    ${timing_tests_SOURCE_DIR}/../../../../matlab/src
    ${timing_tests_SOURCE_DIR}/../../../include
    )


set_property(SOURCE ${MATLAB_GEN_SRC} PROPERTY COMPILE_FLAGS -Wno-maybe-uninitialized)
