add_executable(threads threads.c)

target_link_libraries(threads opencn)
set_target_properties(threads PROPERTIES COMPILE_FLAGS ${_components_cflags})
set_target_properties(threads PROPERTIES LINK_FLAGS ${_components_ldflags})
target_include_directories(threads PRIVATE ${_components_include_directories})
