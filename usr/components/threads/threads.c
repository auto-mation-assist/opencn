/********************************************************************
 * Description:  threads.c

 *
 * Copyright (c) 2019 Daniel Rossier, REDS Institute (daniel.rossier@heig-vd.ch)
 *
 ********************************************************************/

/** This file, 'streamer_usr.c', is the user part of a HAL component
 that allows numbers stored in a file to be "streamed" onto HAL
 pins at a uniform realtime sample rate.  When the realtime module
 is loaded, it creates a stream in shared memory.  Then, the user
 space program 'hal_stream' is invoked.  'hal_stream' takes
 input from stdin and writes it to the stream, and the realtime
 part transfers the data from the stream to HAL pins.

 Invoking:


 'chan_num', if present, specifies the streamer channel to use.
 The default is channel zero.  Since hal_stream takes its data
 from stdin, it will almost always either need to have stdin
 redirected from a file, or have data piped into it from some
 other program.
 */

/** This program is free software; you can redistribute it and/or
 modify it under the terms of version 2 of the GNU General
 Public License as published by the Free Software Foundation.
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 THE AUTHORS OF THIS LIBRARY ACCEPT ABSOLUTELY NO LIABILITY FOR
 ANY HARM OR LOSS RESULTING FROM ITS USE.  IT IS _EXTREMELY_ UNWISE
 TO RELY ON SOFTWARE ALONE FOR SAFETY.  Any machinery capable of
 harming persons must have provisions for completely removing power
 from all motors, etc, before persons enter any danger area.  All
 machinery must be designed to comply with local and national safety
 codes, and the authors of this software can not, and do not, take
 any responsibility for such compliance.

 This code was written as part of the EMC HAL project.  For more
 information, go to www.linuxcnc.org.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <cmdline.h>

#include <uapi/threads.h>

#include <debug.h>

static char *name1 = "thread1";	/* name of thread */
static int fp1 = 1;		/* use floating point? default = yes */
static long period1 = 1000000;	/* thread period - default = 1ms thread */
static char *name2 = "";	/* name of thread */
static int fp2 = 1;		/* use floating point? default = yes */
static long period2 = 0;	/* thread period - default = no thread */
static char *name3 = "";	/* name of thread */
static int fp3 = 1;		/* use floating point? default = yes */
static long period3 = 0;	/* thread period - default = no thread */

int devfd;

/*
 * Connect to the kernel HAL.
 * @mod_name: name of the component (unique)
 * @returns a comp_id (component ID).
 */
int threads_connect(char *name1, long period1, int fp1, char *name2, long period2, int fp2, char *name3, long period3, int fp3) {
	threads_connect_args_t args = {0};
	int ret;

	devfd = open(THREADS_DEV_NAME, O_RDWR);

	if(name1) strncpy(args.name1, name1, sizeof(args.name1));
	args.period1 = period1;
	args.fp1 = fp1;

	if(name2) strncpy(args.name2, name2, sizeof(args.name2));
	args.period2 = period2;
	args.fp2 = fp2;

	if(name3) strncpy(args.name3, name3, sizeof(args.name3));
	args.period3 = period3;
	args.fp3 = fp3;

	ret = ioctl(devfd, THREADS_IOCTL_CONNECT, &args);
	if (ret != 0)
		BUG()

	printf("threads_connect returned with ret = %d\n", ret);

	return ret;
}


/*
 * Syntax: threads name1=<name> period1=1000 ...
 */
int main(int argc, char **argv) {
	int n;
	/* char *cp; */
	char *params = NULL, *value = NULL;

	printf("OpenCN: Threads component starting...\n");

	for (n = 1; n < argc; n++) {
		/* cp = argv[n]; */

		/* First parse the option with "=" */

		next_arg(argv[n], &params, &value);

		if (params) {
			if (!strcmp(params, "name1")) {
				name1 = value;
				continue ;
			}

			if (!strcmp(params, "name2")) {
				name2 = value;
				continue ;
			}

			if (!strcmp(params, "name3")) {
				name3 = value;
				continue ;
			}

			if (!strcmp(params, "period1")) {
				period1 = atol(value);
				continue ;
			}

			if (!strcmp(params, "period2")) {
				period2 = atol(value);
				continue ;
			}

			if (!strcmp(params, "period3")) {
				period3 = atol(value);
				continue ;
			}

			if (!strcmp(params, "fp1")) {
				fp1 = atoi(value);
				continue ;
			}

			if (!strcmp(params, "fp2")) {
				fp2 = atoi(value);
				continue ;
			}

			if (!strcmp(params, "fp3")) {
				fp3 = atoi(value);
				continue ;
			}

		}
	}

	threads_connect(name1, period1, fp1, name2, period2, fp2, name3, period3, fp3);

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	return 0;
}
