
#include <ctype.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <cstring>

#include <cmdline.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <queue>

extern "C" {
#include "tools.h"
#include <debug.h>
#include <gsl/gsl_bspline.h>
#include <uapi/feedopt.h>

#include <matlab/common/src/c_spline.h>

#include <af.h>


#include <sys/time.h>
}

#include "feedopt.hpp"

#include "rs274ngc/rs274ngc_interp.hh"
#include "rs274ngc/rs274ngc_return.hh"

#include <cblas.h>


#define RETURN_IF_NULL(expr, returnvalue) if ((expr) == NULL) {                                             \
    fprintf(stderr, "%s%s:%d: CHECK_NOT_NULL failed%s\n", ASCII_RED, __FUNCTION__, __LINE__, ASCII_RESET);  \
    return returnvalue;                                                                                     \
}

static int exitval = 1;				  /* program return code - 1 means error */
static int ignore_sig = 0;				  /* used to flag critical regions */
//static int linenumber = 0;				  /* used to print linenumber on errors */
static char comp_name[HAL_NAME_LEN + 1]; /* name for this instance of streamer */

static int devfd = 0;

static timeval tv_tic, tv_toc;

static void tic()
{
    gettimeofday(&tv_tic, nullptr);
}

static double toc() {
    gettimeofday(&tv_toc, nullptr);
	return (tv_toc.tv_sec - tv_tic.tv_sec) + (tv_toc.tv_usec - tv_tic.tv_usec) / 1e6;
}


/*
 * Connect to the kernel HAL.
 * @mod_name: name of the component (unique)
 * @returns a comp_id (component ID).
 */
int feedopt_connect(const char *comp_name, const char *devname)
{
    FNIN
    feedopt_connect_args_t args = {};
	int ret;

	devfd = open(devname, O_RDWR);

	if(comp_name) strncpy(args.name, comp_name, sizeof(args.name));

	ret = ioctl(devfd, FEEDOPT_IOCTL_CONNECT, &args);
	if (ret != 0) BUG();

	printf("hal_connect returned with ret = %d\n", ret);
    FNOUT
	return ret;
}

void feedopt_disconnect(void)
{
    FNIN
	ioctl(devfd, FEEDOPT_IOCTL_DISCONNECT);
	close(devfd);
    FNOUT
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/* signal handler */
static sig_atomic_t stop;
static void quit(int/* sig*/)
{
    FNIN
	if (ignore_sig) {
		return;
	}
	stop = 1;
    FNOUT
}

#define BUF_SIZE 4000

// std::vector<CurvStruct> curv_structs;

enum FEEDOPT_STATE {
    INACTIVE,
    ACTIVE
};

struct hal_data {
    hal_bit_t* us_active = nullptr;
    hal_bit_t* us_start = nullptr;
    hal_bit_t* opt_us_reset = nullptr;
    hal_bit_t *commit_cfg = nullptr;
    hal_u32_t *queue_size = nullptr;
//    hal_bit_t *opt_us_stop = nullptr;
};


namespace
{

hal_data* hal = nullptr;
std::vector<CurvStruct> opt_curv_structs;
//



// hal_float_t* pin_opt_per_second = nullptr;

// shared memory for configuration
key_t config_key{0};
int config_shmid{0};
FeedoptConfigStruct* config_mem{nullptr};

key_t curv_key{0};
int curv_shmid{0};
SharedCurvStructs* curv_mem{nullptr};

Interp interp;
Fopt opt_state = Fopt_Init;
FEEDOPT_STATE state = FEEDOPT_STATE::INACTIVE;
bool curv_was_shared = false;

} // namespace


/**********************************/
/* MATLAB CODER INTERFACE - START */
/**********************************/

namespace {

std::vector<CurvStruct> matlab_queues[QueueId_COUNT];
}

PushStatus feedopt_push_opt(CurvStruct opt_struct, double* coeff, int ncoeff) {
    FNTRACE
//	fprintf(stderr, "feedopt_push_opt\n");
	OptStruct opt_curv;
	opt_curv.CurvStruct = opt_struct;
//	force_print("Coeff = ");
	for(int i = 0; i < g_FeedoptConfig.MaxNCoeff; i++) {
//		force_print("%.3f ", coeff[i]);
		opt_curv.Coeff[i] = coeff[i];
	}
//	force_print("\n");

	// fprintf(stderr, "FEEDOPT: Pushing new optimized curve\n");

    return static_cast<PushStatus>(write(devfd, &opt_curv, sizeof(opt_curv)));
    // fprintf(stderr, "FEEDOPT: Push completed\n");
}

void queue_push(int id, CurvStruct value) {
//	fprintf(stderr, "queue_push: id = %d\n", id);
	matlab_queues[id].push_back(value);
}

void queue_resize(int id, int n, CurvStruct default_value) {
//	fprintf(stderr, "queue_resize: id = %d, n = %d\n", id, n);
	matlab_queues[id].resize(n, default_value);
}


CurvStruct queue_get(int id, int k) {
//	fprintf(stderr, "queue_get: id = %d, k = %d\n", id, k);
//	PrintCurvStruct(&matlab_queues[id][k]);
    if (k > queue_size(id) - 1) {
        fprintf(stderr, "%squeue_get: k = %d, size = %d%s\n", ASCII_RED, k, queue_size(id), ASCII_RESET);
        return CurvStruct{};
    }
    CurvStruct r = matlab_queues[id][k];
    return r;
}


void queue_set(int id, int k, CurvStruct value) {
//	fprintf(stderr, "queue_set: id = %d, k = %d\n", id, k);
	matlab_queues[id][k] = value;
}

void queue_clear(int id) {
//	fprintf(stderr, "queue_clear: id = %d\n", id);
    FNIN
	matlab_queues[id].clear();
    FNOUT
}

int queue_size(int id) {
	return matlab_queues[id].size();
}

int feedopt_read_gcode(int id)
{
	interp.read();
	int ret = (interp.execute() == INTERP_OK);
//	fprintf(stderr, "feedopt_read_gcode = %d\n", ret);
	return ret;
}


/********************************/
/* MATLAB CODER INTERFACE - END */
/********************************/

int init_opt_structs() {
    FNTRACE
	interp.init();
    fprintf(stderr, "FEEDOPT: Reading gcode file: %s\n", g_FeedoptConfig.source);
	switch(interp.open(g_FeedoptConfig.source)) {
		case INTERP_OK:
        fprintf(stderr, "OPENED %s\n", g_FeedoptConfig.source);
        return 1;
		default:
        fprintf(stderr, "%sFAILED to open '%s'%s\n", ASCII_RED, g_FeedoptConfig.source, ASCII_RESET);
	}
    return 0;
}

void reset_buttons() {
    *hal->opt_us_reset = 0;
    *hal->us_start = 0;
    *hal->commit_cfg = 0;
}


void send_reset() {
    FNTRACE
    fprintf(stderr, "FEEDOPT: sending FEEDOPT_IOCTL_RESET\n");
    ioctl(devfd, FEEDOPT_IOCTL_RESET, 0);
    fprintf(stderr, "FEEDOPT: completed FEEDOPT_IOCTL_RESET\n");
}

void feedopt_state_inactive() {
FNTRACE
    *hal->us_active = 0;

//    fprintf(stderr, "pin_us_active (feedopt_us.opt-us-reset) = %d\n", *hal->opt_us_reset);

    if (*hal->opt_us_reset) {
        send_reset();
        reset_buttons();
    }

    else if (*hal->commit_cfg) {
        reset_buttons();
        fprintf(stderr, "FEEDOPT: Commit\n");
        fprintf(stderr, "config_mem = %p\n", config_mem);
        if (config_mem) {
            g_FeedoptConfig = *config_mem;
            ioctl(devfd, FEEDOPT_IOCTL_WRITE_CFG, &g_FeedoptConfig);
        }

        curv_was_shared = false;
        opt_state = Fopt_Init;
        state = FEEDOPT_STATE::ACTIVE;
    }
}

void feedopt_state_active() {
FNTRACE
    *hal->us_active = 1;

    if (*hal->commit_cfg) {
        reset_buttons();
        return;
    }

    if (*hal->opt_us_reset) {
        reset_buttons();
        state = FEEDOPT_STATE::INACTIVE;
        send_reset();
        return;
    }

    if (opt_state == Fopt_Init) {
        fprintf(stderr, "FEEDOPT: Fopt_Init\n");
        if(!init_opt_structs()) {
            opt_state = Fopt_Init;
            state = FEEDOPT_STATE::INACTIVE;
            return;
        }
    }
    FeedoptPlan(&opt_state);

    if (curv_mem && opt_state == Fopt_Opt && !curv_was_shared) {

        sem_wait(&curv_mem->mutex);
        int count = std::min(static_cast<int>(matlab_queues[QueueId_Split].size()), MAX_SHARED_CURV_STRUCTS);
        std::memcpy(curv_mem->curv_structs, matlab_queues[QueueId_Split].data(), static_cast<size_t>(count)*sizeof(CurvStruct));
        curv_mem->curv_struct_count = count;
        curv_mem->generation++;
        fprintf(stderr, "FEEDOPT: Sending %d CurvStructs to shared mem, generation = %d\n", count, curv_mem->generation);
        sem_post(&curv_mem->mutex);
        curv_was_shared = true;
    }

    if (opt_state == Fopt_Finished && *hal->queue_size == 0) {
        fprintf(stderr, "FEEDOPT: Fopt_Finished\n");
        state = FEEDOPT_STATE::INACTIVE;
    }
}

void feedopt_us_worker()
{
    FNTRACE
    g_FeedoptConfig.DebugPrint = false;

	tic();
	while (!stop) {
        switch(state) {
        case FEEDOPT_STATE::INACTIVE:
            feedopt_state_inactive();
            usleep(100);
            break;
        case FEEDOPT_STATE::ACTIVE:
            feedopt_state_active();
            break;
        }
		double t = toc();
		if (t > 1.0) {
			// TODO: Bring back the OPT/sec metric
			tic();
        }
    }
}

// static bool dummy_pin_us_active;
// static bool dummy_pin_us_start;
// static bool dummy_pin_opt_us_reset;

int main(int /*argc*/, char **/*argv*/)
{
	int ret;

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(2, &cpuset);
    CPU_SET(3, &cpuset);

    sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);

	signal(SIGINT, quit);
	signal(SIGTERM, quit);
	signal(SIGPIPE, SIG_IGN);

	/* connect to HAL */
	/* create a unique module name, to allow for multiple streamers */
	snprintf(comp_name, sizeof(comp_name), "halfeedopt%d", getpid());

	/* connect to the HAL */
	ignore_sig = 1;
	ret = feedopt_connect(comp_name, FEEDOPT_DEV_NAME);
	ignore_sig = 0;

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	/* check result */
	if (ret < 0) {
		fprintf(stderr, "ERROR: feedopt_connect failed with ret = %d\n", ret);
		exitval = 0;

		ignore_sig = 1;

		feedopt_disconnect();

		return exitval;
	}

	// setup shared memory
	config_key = ftok(FEEDOPT_SHMEM_NAME, FEEDOPT_SHMEM_KEY_CONFIG);
	config_shmid = shmget(config_key, sizeof(FeedoptConfigStruct), 0666 | IPC_CREAT);
	config_mem = static_cast<FeedoptConfigStruct*>(shmat(config_shmid, (void*)0, 0));
    if ((int64_t)config_mem < 0) {
		fprintf(stderr, "Failed to create shared memory for CONFIG\n");
		config_mem = nullptr;
	}

	curv_key = ftok(FEEDOPT_SHMEM_NAME, FEEDOPT_SHMEM_KEY_CURV);
	curv_shmid = shmget(curv_key, sizeof(SharedCurvStructs), 0666 | IPC_CREAT);
	curv_mem = static_cast<SharedCurvStructs*>(shmat(curv_shmid, (void*)0, 0));
    if ((int64_t)curv_mem < 0) {
		fprintf(stderr, "Failed to create shared memory for CURV\n");
		curv_mem = nullptr;
	} 

	if (curv_mem) {
		sem_init(&curv_mem->mutex, 1, 1);
		sem_wait(&curv_mem->mutex);
		curv_mem->generation = 0;
		curv_mem->curv_struct_count = 0;
		sem_post(&curv_mem->mutex);
	}

	InitConfig();

	if(config_mem) {
		*config_mem = g_FeedoptConfig;
	}

    hal = static_cast<hal_data*>(malloc(sizeof(hal_data)));
    RETURN_IF_NULL(hal->us_active = pin_get_bit_ptr("feedopt.us-active"), -1)
    RETURN_IF_NULL(hal->us_start = pin_get_bit_ptr("feedopt.us-start"), -1)
    RETURN_IF_NULL(hal->opt_us_reset = pin_get_bit_ptr("feedopt.opt-us-reset"), -1)
    RETURN_IF_NULL(hal->commit_cfg = pin_get_bit_ptr("feedopt.commit-cfg"), -1)
    RETURN_IF_NULL(hal->queue_size = pin_get_u32_ptr("feedopt.queue-size"), -1)
//    RETURN_IF_NULL(hal->opt_us_stop = pin_get_bit_ptr("feedopt.opt-us-stop"), -1)

    reset_buttons();

	int num_threads = openblas_get_num_threads();
	printf("OPENBLAS: Using %d threads\n", num_threads);
	fflush(stdout);

	feedopt_us_worker();

	/* run was succesfull */
	exitval = 0;

	ignore_sig = 1;

	feedopt_disconnect();

	return exitval;
}

