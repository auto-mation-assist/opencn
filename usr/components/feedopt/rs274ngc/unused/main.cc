#include "rs274ngc_interp.hh"

int main(int argc, char const *argv[]) {
    /* code */
    Interp interp;
    interp.init();
    interp.open("test.gcode");
    interp.read();
    interp.execute();
    interp.read();
    interp.execute();
    interp.read();
    interp.execute();
    return 0;
}
