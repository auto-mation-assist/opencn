/********************************************************************
 * Description:  streamer.c
 *               User space part of "streamer", a HAL component that
 *		can be used to stream data from a file onto HAL pins
 *		 at a specific realtime sample rate.
 *
 * Author: John Kasunich <jmkasunich at sourceforge dot net>
 * License: GPL Version 2
 *
 * Copyright (c) 2006 All rights reserved.
 *
 ********************************************************************/
/** This file, 'streamer_usr.c', is the user part of a HAL component
 that allows numbers stored in a file to be "streamed" onto HAL
 pins at a uniform realtime sample rate.  When the realtime module
 is loaded, it creates a stream in shared memory.  Then, the user
 space program 'hal_stream' is invoked.  'hal_stream' takes
 input from stdin and writes it to the stream, and the realtime
 part transfers the data from the stream to HAL pins.

 Invoking:


 'chan_num', if present, specifies the streamer channel to use.
 The default is channel zero.  Since hal_stream takes its data
 from stdin, it will almost always either need to have stdin
 redirected from a file, or have data piped into it from some
 other program.
 */

/** This program is free software; you can redistribute it and/or
 modify it under the terms of version 2 of the GNU General
 Public License as published by the Free Software Foundation.
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 THE AUTHORS OF THIS LIBRARY ACCEPT ABSOLUTELY NO LIABILITY FOR
 ANY HARM OR LOSS RESULTING FROM ITS USE.  IT IS _EXTREMELY_ UNWISE
 TO RELY ON SOFTWARE ALONE FOR SAFETY.  Any machinery capable of
 harming persons must have provisions for completely removing power
 from all motors, etc, before persons enter any danger area.  All
 machinery must be designed to comply with local and national safety
 codes, and the authors of this software can not, and do not, take
 any responsibility for such compliance.

 This code was written as part of the EMC HAL project.  For more
 information, go to www.linuxcnc.org.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <cmdline.h>

#include <uapi/streamer.h>

#include <debug.h>

int exitval = 1; /* program return code - 1 means error */
int ignore_sig = 0; /* used to flag critical regions */
int linenumber = 0; /* used to print linenumber on errors */
char comp_name[HAL_NAME_LEN + 1]; /* name for this instance of streamer */

int devfd;

#warning Channel is probably similar to our minor now... to be checked!

/*
 * Connect to the kernel HAL.
 * @mod_name: name of the component (unique)
 * @returns a comp_id (component ID).
 */
int streamer_connect(char *comp_name, char *devname, int channel, int depth, char *cfg) {
	streamer_connect_args_t args = {0};
	int ret;

	devfd = open(devname, O_RDWR);

	if (comp_name)
		strncpy(args.name, comp_name, sizeof(args.name));
	args.channel = channel;
	args.depth = depth;
	if (cfg)
		strncpy(args.cfg, cfg, sizeof(args.cfg));

	ret = ioctl(devfd, STREAMER_IOCTL_CONNECT, &args);
	if (ret != 0)
		BUG()

	printf("hal_connect returned with ret = %d\n", ret);

	return ret;
}

void streamer_disconnect(void) {

	ioctl(devfd, STREAMER_IOCTL_DISCONNECT);

	close(devfd);
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/* signal handler */
static sig_atomic_t stop;
static void quit(int sig) {
	if (ignore_sig) {
		return;
	}
	stop = 1;
}

#define BUF_SIZE 4000

/*
 * Syntax: streamer depth=x cfg=y filename
 */
int main(int argc, char **argv) {
	int n, channel, line = 0;
	char *cp, *cp2;
	char buf[BUF_SIZE];
	const char *errmsg = NULL;
	int ret;
	int len;
	int reload;
	int depth = 0;
	char *cfg = "";
	char *params = NULL, *value = NULL;
	int fd;

	/* set return code to "fail", clear it later if all goes well */
	exitval = 1;
	channel = 0;
	for (n = 1; n < argc; n++) {
		cp = argv[n];

		/* First parse the option with "=" */

		next_arg(argv[n], &params, &value);

		if (params) {
			if (!strcmp(params, "depth")) {
				depth = atoi(value);
				continue ;
			}

			if (!strcmp(params, "cfg")) {
				cfg = value;
				continue ;
			}
		}

		if (*cp != '-') {
			break;
		}
		switch (*(++cp)) {
			case 'c':
				if ((*(++cp) == '\0') && (++n < argc)) {
					cp = argv[n];
				}
				channel = strtol(cp, &cp2, 10);
				if ((*cp2) || (channel < 0) || (channel >= MAX_STREAMERS)) {
					fprintf(stderr, "ERROR: invalid channel number '%s'\n", cp);
					exit(1);
				}
				break;
			default:
				fprintf(stderr, "ERROR: unknown option '%s'\n", cp);
				exit(1);
				break;
		}
	}
	if (n < argc) {
		if (argc > n + 1) {
			fprintf(stderr, "ERROR: At most one filename may be specified\n");
			exit(1);
		}
		/* make stdin be the named file */
		fd = open(argv[n], O_RDONLY);
		if (fd < 0) {
			perror(argv[n]);

			/* Inform the parent that something wrong happened. */
			kill(getppid(), SIGUSR2);
			exit(-1);

		}
		close(0);
		dup2(fd, 0);
	}
	/* register signal handlers - if the process is killed
	 we need to call hal_exit() to free the shared memory */
	signal(SIGINT, quit);
	signal(SIGTERM, quit);
	signal(SIGPIPE, SIG_IGN);
	/* connect to HAL */
	/* create a unique module name, to allow for multiple streamers */
	snprintf(comp_name, sizeof(comp_name), "halstreamer%d", getpid());

	/* connect to the HAL */
	ignore_sig = 1;
	ret = streamer_connect(comp_name, STREAMER_DEV_NAME, channel, depth, cfg);
	ignore_sig = 0;

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	/* check result */
	if (ret < 0) {
		fprintf(stderr, "ERROR: streamer_connect failed with ret = %d\n", ret);
		goto out;
	}

	while (!stop) {
		while (fgets(buf, BUF_SIZE, stdin)) {
			/* skip comment lines */
			if (buf[0] == '#') {
				line++;
				continue;
			}

			/* Sends the string to the kernel-side streamer. */
			len = write(devfd, buf, strlen(buf) + 1);
			if (len < 0) {
				/* print message */
				fprintf(stderr, "line %d, field %d: %s, skipping the line\n", line, n, errmsg);
				/** TODO - decide whether to skip this line and continue, or
				 abort the program.  Right now it skips the line. */
			}

			line++;
		}

		ioctl(devfd, STREAMER_IOCTL_RELOAD, &reload);
		while (!reload && !stop) {
			usleep(100000);
			ioctl(devfd, STREAMER_IOCTL_RELOAD, &reload);
		}
		printf("STREAMER: Reload(%d) or stop(%d) received, seeking to 0\n", reload, stop);
		/* Proceed with a new bunch of values. */
		fseek(stdin, 0, SEEK_SET);
	}

	/* run was succesfull */
	exitval = 0;

out:
	ignore_sig = 1;

	streamer_disconnect();

	return exitval;
}
