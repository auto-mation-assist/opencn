#include <uapi/debug.h>
#include <stdio.h>
#include <semaphore.h>

static int debug_indent_level = 0;

void debug_function_print(const char* function_name, int diff) {
    int i;

    if (diff > 0) {
        fprintf(stderr, "%s>%s", ASCII_GREEN, ASCII_RESET);
        i = 0;
    }
    if (diff < 0) {
        fprintf(stderr, "%s<%s", ASCII_RED, ASCII_RESET);
        i = 1;
    }
    for(; i < debug_indent_level; i++) {
        fprintf(stderr, "  ");
    }
    fprintf(stderr, "%s\n", function_name);
    debug_indent_level += diff;
}
