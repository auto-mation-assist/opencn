#ifndef TRAJECTORY_VIEWER_H
#define TRAJECTORY_VIEWER_H

#include <QMouseEvent>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QWheelEvent>

#include "axes.h"
#include "glcamera.h"
#include "part.h"
#include "tool.h"
#include "toolpath.h"

class TrajectoryViewer : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    TrajectoryViewer(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~TrajectoryViewer();

    Part *getPart();
    ToolPath *getPath();

protected:
    virtual void initializeGL();
    virtual void paintGL();
    virtual void resizeGL(int w, int h);

signals:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private slots:
    void mousePressed(QMouseEvent *event);
    void mouseReleased(QMouseEvent *event);
    void mouseMove(QMouseEvent *event);
    void wheel(QWheelEvent *event);

private:
    GlCamera _camera;
    bool _cameraAngleMoving;
    int _lastX, _lastY;
    Tool _tool;
    Axes _axes;
    Part _part;
    ToolPath _toolPath;

    static const float RotationSpeedFactor;
    static const float MouseTranslationSpeedFactor;
};

#endif /* TRAJECTORY_VIEWER_H */
