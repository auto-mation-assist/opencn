#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    af_init();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    int ret = a.exec();
    af_exit();
    return ret;
}
