#ifndef PART_H
#define PART_H

#include <GL/glu.h>
#include <QObject>
#include <QString>
#include <QVector3D>
#include <QtCore>

#include <af.h>

extern "C" {
#include <EvalCurvStruct.h>
}

class Part : public QObject
{

    Q_OBJECT
signals:
    void pointsUpdated();

public:
    Part();
    virtual ~Part();

    /*!
     * Load a binary file describing CurvStruct for now (GCode file in the future)
     * \param path Path to the binary file (GCode file in the future)
     */
    bool loadBinFile(const char *path); /* TODO QString */
    bool loadStreamingFile(const QString &path);

    /*!
     * To be called in the OpenGL loop ONLY
     */
    void draw();
    void loadCurvStruct(const CurvStruct *curvStruct, int numStruct);

public slots:
    void offsetXChange(double value);
    void offsetYChange(double value);
    void offsetZChange(double value);

private:
    int readStructCount(communicationChannel_t *channel, unsigned int timeoutMs = 1000u);
    bool readStructs(communicationChannel_t *channel, int structCount, unsigned int timeoutPerReadMs = 1000u);

private:
    QVector3D *_segmentPoints;
    std::vector<int> _segmentSourceId;
    int _segmentPointsSize;

    float offsetX = 0.0, offsetY = 0.0, offsetZ = 0.0;
    float _xmin = 0, _xmax = 0, _ymin = 0, _ymax = 0, _zmin = 0, _zmax = 0;

    GLuint display_list_index = 0;

    static const int SubDivisionCount;
};

#endif /* PART_H */
