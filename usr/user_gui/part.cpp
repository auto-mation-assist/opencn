#include "part.h"

#include <QDebug>
#include <QFile>
#include <cmath>

static const double xmin = -24e-3;
static const double xmax = 24e-3;
static const double ymin = -24e-3;
static const double ymax = 24e-3;

const int Part::SubDivisionCount = 10;

Part::Part() : _segmentPoints(nullptr), _segmentPointsSize(0) {}

Part::~Part()
{
    if (_segmentPoints != nullptr) {
        delete[] _segmentPoints;
    }
}

void Part::draw()
{

    if (!display_list_index) {
        display_list_index = glGenLists(1);
        glNewList(display_list_index, GL_COMPILE);
        glPushMatrix();
        glEnable(GL_COLOR_MATERIAL);

        glLineWidth(1.0f);

        glBegin(GL_LINES);

        int last_id = 0;
        GLfloat r = 0.0f, g = 1.0f, b = 0.0f;
        for (int i = 0; i < _segmentPointsSize - 1; i++) {
            if (_segmentSourceId[i] != last_id) {
                last_id = _segmentSourceId[i];
                if (last_id % 2 == 1) {
                    g = 0.0f;
                    b = 1.0f;
                } else {
                    g = 1.0f;
                    b = 0.0f;
                }
            }
            glColor3f(r,g,b);
            glVertex3f(_segmentPoints[i].x(), -_segmentPoints[i].y(), _segmentPoints[i].z());

            glColor3f(r,g,b);
            glVertex3f(_segmentPoints[i+1].x(), -_segmentPoints[i+1].y(), _segmentPoints[i+1].z());
        }

        glEnd();

        glDisable(GL_COLOR_MATERIAL);
        glPopMatrix();
        glEndList();
    } else {
        glPushMatrix();
        glTranslatef(offsetX, -offsetZ, offsetY);
        glCallList(display_list_index);
        glPopMatrix();
    }

    glEnable(GL_COLOR_MATERIAL);
    glPushMatrix();
    glTranslatef(offsetX, 0, offsetY);
    if (_xmin + offsetX <= xmin ||
        _xmax + offsetX >= xmax ||
        _ymin + offsetY <= ymin ||
        _ymax + offsetY >= ymax) {
        glColor3f(1.0, 0.0, 0.0);
    } else {
        glColor3f(0.0, 0.1, 0.0);
    }

    glLineWidth(3.0f);
    glBegin(GL_LINE_LOOP);
        glVertex3f(_xmin, 0, _ymin);
        glVertex3f(_xmax, 0, _ymin);
        glVertex3f(_xmax, 0, _ymax);
        glVertex3f(_xmin, 0, _ymax);
    glEnd();
    glPopMatrix();
    glDisable(GL_COLOR_MATERIAL);
}

void Part::offsetXChange(double value)
{
    offsetX = value / 1000.0;
    emit pointsUpdated();
}
void Part::offsetYChange(double value)
{
    offsetY = -value / 1000.0;
    emit pointsUpdated();
}
void Part::offsetZChange(double value)
{
    offsetZ = value / 1000.0;
    emit pointsUpdated();
}

bool Part::loadBinFile(const char *path)
{
    communicationChannel_t *pathChannel;
    communicationChannel_t *structChannel;

    int ret = open_GCodeFilePath_channel(&pathChannel);
    if (ret) {
        qDebug() << "open_GCodeFilePath_channel() Failed";
        return false;
    }

    ret = open_struct_channel(&structChannel);
    if (ret) {
        close_channel(pathChannel);
        qDebug() << "open_struct_channel() Failed";
        return false;
    }

    ssize_t rets = writeGCodeFilePath(pathChannel, path, strlen(path) + 1);
    if (rets != static_cast<ssize_t>(strlen(path) + 1)) {
        close_channel(structChannel);
        close_channel(pathChannel);
        qDebug() << "writeGCodeFilePath";
        return false;
    }

    int structCount = readStructCount(structChannel);
    if (structCount < 0) {
        close_channel(structChannel);
        close_channel(pathChannel);
        qDebug() << "readStruct() Failed";
        return false;
    }

    if (!readStructs(structChannel, structCount)) {
        close_channel(structChannel);
        close_channel(pathChannel);
        qDebug() << "readStructs() failed";
        return false;
    }

    close_channel(structChannel);
    close_channel(pathChannel);

    return true;
}

void Part::loadCurvStruct(const CurvStruct *curvStruct, int numStruct)
{
    if (display_list_index) {
        glDeleteLists(display_list_index, 1);
        display_list_index = 0;
    }

    if (_segmentPoints != nullptr) {
        delete[] _segmentPoints;
    }

    _segmentPoints = new QVector3D[numStruct * SubDivisionCount];
    _segmentSourceId.clear();
    _segmentSourceId.reserve(numStruct * SubDivisionCount);


    _xmin = std::numeric_limits<float>::max();
    _xmax = std::numeric_limits<float>::min();

    _ymin = std::numeric_limits<float>::max();
    _ymax = std::numeric_limits<float>::min();

    for (int i = 0; i < numStruct; i++) {
        int uVecSize[2] = {1, 1};
        double r0D[3], r1D[3], r2D[3], r3D[3];
        int r0D_size[2], r1D_size[2], r2D_size[2], r3D_size[2];

        // printf("dataSize: %dx%d\n", dataSize[0], dataSize[1]);

        for (int j = 0; j < SubDivisionCount; j++) {
            double u = 1.0 * static_cast<double>(j) / static_cast<double>(SubDivisionCount);
            // double u = 0;
            EvalCurvStruct(&curvStruct[i], &u, uVecSize, r0D, r0D_size, r1D, r1D_size, r2D, r2D_size, r3D,
                       r3D_size);
            // Warning: The OpenGL frame is right handed with Y upward whereas LinuxCNC frame is left handed with Z
            // upward
            float x = static_cast<float>( r0D[0] / 1000.0);  //  x
            float y = static_cast<float>(-r0D[2] / 1000.0);  //  z
            float z = static_cast<float>(-r0D[1] / 1000.0);  // -y
            _segmentPoints[i * SubDivisionCount + j] = QVector3D(x, y, z);
            _xmin = std::min(x, _xmin);
            _xmax = std::max(x, _xmax);

            _ymin = std::min(z, _ymin);
            _ymax = std::max(z, _ymax);

            _segmentSourceId[i * SubDivisionCount + j] = i;
        }        
    }

    _segmentPointsSize = numStruct * SubDivisionCount;
    qDebug() << "_segmentPointsSize: " << _segmentPointsSize;
    emit pointsUpdated();
}

int Part::readStructCount(communicationChannel_t *channel, unsigned int timeoutMs)
{
    size_t size;
    ssize_t rets = readStruct(channel, &size, sizeof(size), timeoutMs);
    if (rets != sizeof(size)) {
        return -1;
    }

    return static_cast<int>(size);
}

bool Part::readStructs(communicationChannel_t *channel, int structCount, unsigned int timeoutPerReadMs)
{
    CurvStruct *s = new CurvStruct[structCount];

    for (int i = 0; i < structCount; i++) {
        ssize_t rets = readStruct(channel, &s[i], sizeof(s[i]), timeoutPerReadMs);
        if (rets != sizeof(s[i])) {
            qDebug() << "readStructFailed";
            delete[] s;
            return false;
        }
    }

    loadCurvStruct(s, structCount);

    delete[] s;

    return true;
}

bool Part::loadStreamingFile(const QString &path)
{
    if (display_list_index) {
        glDeleteLists(display_list_index, 1);
        display_list_index = 0;
    }
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error reading " << path;
        return false;
    }

    QTextStream in(&file);

    // Count lines
    int lineCount = 0;
    while (!in.atEnd()) {
        QString line = in.readLine();
        lineCount++;
    }

    in.seek(0);

    if (_segmentPoints != nullptr) {
        delete[] _segmentPoints;
    }

    _segmentPoints = new QVector3D[lineCount];
    _xmin = std::numeric_limits<float>::max();
    _xmax = std::numeric_limits<float>::min();

    _ymin = std::numeric_limits<float>::max();
    _ymax = std::numeric_limits<float>::min();

    int i = 0;
    int k = 0;
    while (!in.atEnd() && i < 10000) {
        QString line = in.readLine();
        if (k++ % 100 != 0) {
            continue;
        }
        QStringList list = line.split(" ", QString::SkipEmptyParts);

        if (list.size() != 3) {
            delete[] _segmentPoints;
            _segmentPoints = nullptr;
            return false;
        }

        _segmentPoints[i].setX(list[0].toFloat() / 1000.0f);
        _segmentPoints[i].setY(list[2].toFloat() / 1000.0f);
        _segmentPoints[i].setZ(-list[1].toFloat() / 1000.0f);

        _xmin = std::min(_segmentPoints[i].x(), _xmin);
        _xmax = std::max(_segmentPoints[i].x(), _xmax);

        _ymin = std::min(_segmentPoints[i].z(), _ymin);
        _ymax = std::max(_segmentPoints[i].z(), _ymax);

//        if (i > 0 && _segmentPoints[i].distanceToPoint(_segmentPoints[i-1]) > 1e-3) {
//            i++;
//        }
//        if (i == 0) i = 1;
        i++;
        k++;
    }

    _segmentPointsSize = lineCount;
    emit pointsUpdated();

    return true;
}
