
/*
 * Simple app to demonstrate how to pin an application on CPU #2.
 */
#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sched.h>
#include <stdbool.h>
#include <pthread.h>

volatile int i,j;

void *t1_fn(void *args) {

	while (true) {
		if (i % 10000000 == 0)
			printf("## pid: %d t1 running on CPU %d\n", getpid(), sched_getcpu());
		i++;
	}
	return NULL;
}

void *t2_fn(void *args) {
	cpu_set_t cpuset;
#if 0
	CPU_ZERO(&cpuset);
	CPU_SET(2, &cpuset);

	printf("## setting affinity to CPU #2\n");
	sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);
#endif

	while (true) {
		if (j % 10000000 == 0)
			printf("## pid: %d t2 running on CPU %d\n", getpid(), sched_getcpu());
		j++;
	}

	return NULL;

}


int main(int argc, char *argv[]) {

	pthread_t t1, t2;
	cpu_set_t cpuset;
	int i;


	printf("## Starting a computation-intensive thread on CPU #2 and #3\n");

	CPU_ZERO(&cpuset);
	CPU_SET(2, &cpuset);
	CPU_SET(3, &cpuset);

	sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);

	pthread_create(&t1, NULL, t1_fn, NULL);
	pthread_create(&t2, NULL, t2_fn, NULL);

	while (true) {
		if (i % 10000000 == 0)
			printf("## main: %d running on CPU %d\n", getpid(), sched_getcpu());
		i++;
	}



}
