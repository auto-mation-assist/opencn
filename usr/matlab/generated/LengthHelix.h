/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: LengthHelix.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef LENGTHHELIX_H
#define LENGTHHELIX_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern double LengthHelix(const double CurvStruct_P0[3], const double
    CurvStruct_P1[3], const double CurvStruct_evec[3], double CurvStruct_theta,
    double CurvStruct_pitch);

#endif

/*
 * File trailer for LengthHelix.h
 *
 * [EOF]
 */
