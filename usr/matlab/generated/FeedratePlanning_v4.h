/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedratePlanning_v4.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef FEEDRATEPLANNING_V4_H
#define FEEDRATEPLANNING_V4_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void FeedratePlanning_v4(const CurvStruct CurvStructs0_data[], const
    double amax[3], const double jmax[3], double b_v_0, double b_at_0, double
    b_v_1, double b_at_1, const emxArray_real_T *b_BasisVal, const
    emxArray_real_T *b_BasisValD, const emxArray_real_T *b_BasisValDD, const
    emxArray_real_T *b_BasisIntegr, unsigned long Bl_handle, const double
    u_vec_data[], const int u_vec_size[2], int N_Hor, double Coeff_data[], int
    Coeff_size[2]);
extern void FeedratePlanning_v4_init(void);
extern void b_FeedratePlanning_v4(const CurvStruct CurvStructs0[10], const
    double amax[3], const double jmax[3], double *b_v_0, double *b_at_0, double
    b_v_1, double b_at_1, const double BasisVal_data[], const int BasisVal_size
    [2], const double BasisValD_data[], const int BasisValD_size[2], const
    double BasisValDD_data[], const int BasisValDD_size[2], const double
    BasisIntegr_data[], const int BasisIntegr_size[1], unsigned long Bl_handle,
    const double u_vec_data[], const int u_vec_size[2], int N_Hor, double
    Coeff_data[], int Coeff_size[2], int *b_NCoeff, int *success);

#endif

/*
 * File trailer for FeedratePlanning_v4.h
 *
 * [EOF]
 */
