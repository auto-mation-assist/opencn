/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CalcVAJ_v5.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "CalcVAJ_v5.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "EvalHelix.h"
#include "EvalTransP5.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "c_spline.h"
#include "sinspace.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * import splines.*
 * Arguments    : CurveType CurvStructs_Type
 *                const double CurvStructs_P0[3]
 *                const double CurvStructs_P1[3]
 *                const double CurvStructs_evec[3]
 *                double CurvStructs_theta
 *                double CurvStructs_pitch
 *                double CurvStructs_CoeffP5[6][3]
 *                unsigned long Bl_handle
 *                const double Coeff_data[]
 *                const int Coeff_size[1]
 *                double *v_norm
 *                double a[3]
 * Return Type  : void
 */
void CalcVAJ_v5(CurveType CurvStructs_Type, const double CurvStructs_P0[3],
                const double CurvStructs_P1[3], const double CurvStructs_evec[3],
                double CurvStructs_theta, double CurvStructs_pitch, double
                CurvStructs_CoeffP5[6][3], unsigned long Bl_handle, const double
                Coeff_data[], const int Coeff_size[1], double *v_norm, double a
                [3])
{
    double r1D[3];
    double r2D[3];
    int loop_ub;
    double X[3];
    double r3D[3];
    char message[30];
    double coeffs_data[1200];

    /*  Ntot = 0; */
    /*  for k = 1:Ncrv */
    /*      Ntot = Ntot + size(u_cell{k}, 2); */
    /*  end */
    /*  v_norm = []; */
    /*  a = []; */
    /*  j = []; */
    /*  */
    /*      qSpl = Function(Bl, Coeff(:, k)); */
    /*  */
    /*  */
    /*  */
    r1D[0] = 0.0;
    r2D[0] = 0.0;
    r1D[1] = 0.0;
    r2D[1] = 0.0;
    r1D[2] = 0.0;
    r2D[2] = 0.0;
    switch (CurvStructs_Type) {
      case CurveType_Line:
        /*  line (G01) */
        /*  */
        /*  parametrization of a straight line between P0 and P1 */
        /*  */
        /*  */
        r1D[0] = CurvStructs_P1[0] - CurvStructs_P0[0];
        r1D[1] = CurvStructs_P1[1] - CurvStructs_P0[1];
        r1D[2] = CurvStructs_P1[2] - CurvStructs_P0[2];

        /*  */
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        b_EvalHelix(CurvStructs_P0, CurvStructs_P1, CurvStructs_evec,
                    CurvStructs_theta, CurvStructs_pitch, 1.0, X, r1D, r2D, r3D);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        b_EvalTransP5(CurvStructs_CoeffP5, 1.0, X, r1D, r2D, r3D);
        break;

      default:
        for (loop_ub = 0; loop_ub < 30; loop_ub++) {
            message[loop_ub] = cv[loop_ub];
        }

        c_assert_(&message[0]);
        break;
    }

    /*  norm */
    /*  */
    /*      q_val   = qSpl.fast_eval(u_vec); */
    /*      qD_val  = qSpl.derivative.fast_eval(u_vec); */
    /*      qDD_val = qSpl.derivative(2).fast_eval(u_vec); */
    /*  TODO: Optimize this with a single call to eval, and maybe a basis */
    /*  precompute? */
    loop_ub = Coeff_size[0];
    if (0 <= loop_ub - 1) {
        memcpy(&coeffs_data[0], &Coeff_data[0], loop_ub * sizeof(double));
    }

    /*  void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]); */
    c_bspline_eval(&Bl_handle, &coeffs_data[0], 1.0, &X[0]);

    /*  */
    /*      r1D_norm.*sqrt(q_val') */
    *v_norm = sqrt((pow(r1D[0], 2.0) + pow(r1D[1], 2.0)) + pow(r1D[2], 2.0)) *
        sqrt(X[0]);
    a[0] = r2D[0] * X[0] + 0.5 * (r1D[0] * X[1]);
    a[1] = r2D[1] * X[0] + 0.5 * (r1D[1] * X[1]);
    a[2] = r2D[2] * X[0] + 0.5 * (r1D[2] * X[1]);

    /* zeros(3, size(tmp2, 2)); */
}

/*
 * File trailer for CalcVAJ_v5.c
 *
 * [EOF]
 */
