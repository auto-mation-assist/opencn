/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: G2_Hermite_Interpolation.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef G2_HERMITE_INTERPOLATION_H
#define G2_HERMITE_INTERPOLATION_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void G2_Hermite_Interpolation(const double r0D0[3], const double r0D1[3],
    const double r0D2[3], const double r1D0[3], const double r1D1[3], const
    double r1D2[3], double p5_3D[6][3]);

#endif

/*
 * File trailer for G2_Hermite_Interpolation.h
 *
 * [EOF]
 */
