/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_alloc_matrix.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef C_ALLOC_MATRIX_H
#define C_ALLOC_MATRIX_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void c_alloc_matrix(double M, double N, emxArray_real_T *A);

#endif

/*
 * File trailer for c_alloc_matrix.h
 *
 * [EOF]
 */
