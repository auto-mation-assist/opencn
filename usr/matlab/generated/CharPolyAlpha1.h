/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CharPolyAlpha1.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef CHARPOLYALPHA1_H
#define CHARPOLYALPHA1_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void CharPolyAlpha1(const double in1[16], double Coeff_Poly_Alpha1[10]);

#endif

/*
 * File trailer for CharPolyAlpha1.h
 *
 * [EOF]
 */
