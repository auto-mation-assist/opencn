/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CalcTransition.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "CalcTransition.h"
#include "BenchmarkFeedratePlanning.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "CutCurvStruct.h"
#include "EvalCurvStruct.h"
#include "EvalHelix.h"
#include "EvalTransP5.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "G2_Hermite_Interpolation.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <stdio.h>

/* Function Definitions */

/*
 * Arguments    : const CurvStruct *CurvStruct1
 *                const CurvStruct *CurvStruct2
 *                double CutOff
 *                CurvStruct *CurvStruct1_C
 *                CurvStruct *CurvStruct_T
 *                CurvStruct *CurvStruct2_C
 * Return Type  : void
 */
void CalcTransition(const CurvStruct *CurvStruct1, const CurvStruct *CurvStruct2,
                    double CutOff, CurvStruct *CurvStruct1_C, CurvStruct
                    *CurvStruct_T, CurvStruct *CurvStruct2_C)
{
    double r0D[3];
    double r1D[3];
    double r2D[3];
    int i;
    double a[3];
    char message[30];
    double b_r0D[3];
    double b_r1D[3];
    double b_r2D[3];
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    if (g_FeedoptConfig.DebugPrint) {
        printf("========== CalcTransition ==========\n");
        fflush(stdout);
    }

    if (g_FeedoptConfig.DebugPrint) {
        printf("CutOff = %.3f\n", CutOff);
        fflush(stdout);
    }

    PrintCurvStruct(CurvStruct1);
    PrintCurvStruct(CurvStruct2);
    CutCurvStruct(CurvStruct1, 0.0, CutOff, CurvStruct1_C);
    CutCurvStruct(CurvStruct2, CutOff, 0.0, CurvStruct2_C);
    if (g_FeedoptConfig.DebugPrint) {
        printf("========== AFTER CUTTING \n");
        fflush(stdout);
    }

    PrintCurvStruct(CurvStruct1_C);
    PrintCurvStruct(CurvStruct2_C);

    /*  */
    /*  */
    r0D[0] = 0.0;
    r1D[0] = 0.0;
    r2D[0] = 0.0;
    r0D[1] = 0.0;
    r1D[1] = 0.0;
    r2D[1] = 0.0;
    r0D[2] = 0.0;
    r1D[2] = 0.0;
    r2D[2] = 0.0;
    switch (CurvStruct1_C->Type) {
      case CurveType_Line:
        /*  line (G01) */
        /*  */
        /*  parametrization of a straight line between P0 and P1 */
        /*  */
        r0D[0] = CurvStruct1_C->P1[0];
        r0D[1] = CurvStruct1_C->P1[1];
        r0D[2] = CurvStruct1_C->P1[2];

        /*  */
        r1D[0] = CurvStruct1_C->P1[0] - CurvStruct1_C->P0[0];
        r1D[1] = CurvStruct1_C->P1[1] - CurvStruct1_C->P0[1];
        r1D[2] = CurvStruct1_C->P1[2] - CurvStruct1_C->P0[2];

        /*  */
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        b_EvalHelix(CurvStruct1_C->P0, CurvStruct1_C->P1, CurvStruct1_C->evec,
                    CurvStruct1_C->theta, CurvStruct1_C->pitch, 1.0, r0D, r1D,
                    r2D, a);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        b_EvalTransP5(CurvStruct1_C->CoeffP5, 1.0, r0D, r1D, r2D, a);
        break;

      default:
        for (i = 0; i < 30; i++) {
            message[i] = cv[i];
        }

        c_assert_(&message[0]);
        break;
    }

    /*  end */
    /*  */
    /*  */
    b_r0D[0] = 0.0;
    b_r1D[0] = 0.0;
    b_r2D[0] = 0.0;
    b_r0D[1] = 0.0;
    b_r1D[1] = 0.0;
    b_r2D[1] = 0.0;
    b_r0D[2] = 0.0;
    b_r1D[2] = 0.0;
    b_r2D[2] = 0.0;
    switch (CurvStruct2_C->Type) {
      case CurveType_Line:
        /*  line (G01) */
        /*  */
        /*  parametrization of a straight line between P0 and P1 */
        /*  */
        b_r0D[0] = CurvStruct2_C->P0[0];
        b_r0D[1] = CurvStruct2_C->P0[1];
        b_r0D[2] = CurvStruct2_C->P0[2];

        /*  */
        b_r1D[0] = CurvStruct2_C->P1[0] - CurvStruct2_C->P0[0];
        b_r1D[1] = CurvStruct2_C->P1[1] - CurvStruct2_C->P0[1];
        b_r1D[2] = CurvStruct2_C->P1[2] - CurvStruct2_C->P0[2];

        /*  */
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        b_EvalHelix(CurvStruct2_C->P0, CurvStruct2_C->P1, CurvStruct2_C->evec,
                    CurvStruct2_C->theta, CurvStruct2_C->pitch, 0.0, b_r0D,
                    b_r1D, b_r2D, a);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        b_EvalTransP5(CurvStruct2_C->CoeffP5, 0.0, b_r0D, b_r1D, b_r2D, a);
        break;

      default:
        for (i = 0; i < 30; i++) {
            message[i] = cv[i];
        }

        c_assert_(&message[0]);
        break;
    }

    /*  begin */
    G2_Hermite_Interpolation(r0D, r1D, r2D, b_r0D, b_r1D, b_r2D,
        CurvStruct_T->CoeffP5);
    CurvStruct_T->FeedRate = CurvStruct1->FeedRate;

    /*  */
    /*  */
    CurvStruct_T->Type = CurveType_TransP5;
    CurvStruct_T->ZSpdMode = ZSpdMode_NN;
    CurvStruct_T->P0[0] = 0.0;
    CurvStruct_T->P1[0] = 0.0;
    CurvStruct_T->evec[0] = 0.0;
    CurvStruct_T->P0[1] = 0.0;
    CurvStruct_T->P1[1] = 0.0;
    CurvStruct_T->evec[1] = 0.0;
    CurvStruct_T->P0[2] = 0.0;
    CurvStruct_T->P1[2] = 0.0;
    CurvStruct_T->evec[2] = 0.0;
    CurvStruct_T->theta = 0.0;
    CurvStruct_T->pitch = 0.0;
}

/*
 * File trailer for CalcTransition.c
 *
 * [EOF]
 */
