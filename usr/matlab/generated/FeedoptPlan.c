/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptPlan.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "FeedoptPlan.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "FeedoptTypes_initialize.h"
#include "FeedratePlanning_v4.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "SmoothCurvStructs.h"
#include "SplitCurvStructs.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "c_spline.h"
#include "linspace.h"
#include "sinspace.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

/* Type Definitions */
#ifndef typedef_Queue
#define typedef_Queue

typedef struct {
    int id;
} Queue;

#endif                                 /*typedef_Queue*/

#ifndef struct_emxArray_real_T_120
#define struct_emxArray_real_T_120

struct emxArray_real_T_120
{
    double data[120];
    int size[1];
};

#endif                                 /*struct_emxArray_real_T_120*/

#ifndef typedef_emxArray_real_T_120
#define typedef_emxArray_real_T_120

typedef struct emxArray_real_T_120 emxArray_real_T_120;

#endif                                 /*typedef_emxArray_real_T_120*/

#ifndef struct_emxArray_real_T_1x200
#define struct_emxArray_real_T_1x200

struct emxArray_real_T_1x200
{
    double data[200];
    int size[2];
};

#endif                                 /*struct_emxArray_real_T_1x200*/

#ifndef typedef_emxArray_real_T_1x200
#define typedef_emxArray_real_T_1x200

typedef struct emxArray_real_T_1x200 emxArray_real_T_1x200;

#endif                                 /*typedef_emxArray_real_T_1x200*/

#ifndef struct_emxArray_real_T_200x120
#define struct_emxArray_real_T_200x120

struct emxArray_real_T_200x120
{
    double data[24000];
    int size[2];
};

#endif                                 /*struct_emxArray_real_T_200x120*/

#ifndef typedef_emxArray_real_T_200x120
#define typedef_emxArray_real_T_200x120

typedef struct emxArray_real_T_200x120 emxArray_real_T_200x120;

#endif                                 /*typedef_emxArray_real_T_200x120*/

#ifndef typedef_struct_T
#define typedef_struct_T

typedef struct {
    int n;
    unsigned long handle;
} struct_T;

#endif                                 /*typedef_struct_T*/

/* Variable Definitions */
static Queue QueueGCode;
static Queue QueueSmooth;
static Queue QueueSplit;
static Queue QueueOpt;
static int Noptimized;
static emxArray_real_T_200x120 BasisVal;
static emxArray_real_T_200x120 BasisValD;
static emxArray_real_T_200x120 BasisValDD;
static emxArray_real_T_1x200 u_vec;
static int k0;
static emxArray_real_T_120x10 Coeff;
static int NCoeff;
static boolean_T ReachedEnd;
static double v_0;
static double v_1;
static double at_0;
static double at_1;
static struct_T Bl;
static boolean_T Bl_not_empty;
static emxArray_real_T_120 BasisIntegr;
static boolean_T TryAgain;

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void Bl_not_empty_init(void)
{
    Bl_not_empty = false;
}

/*
 * Arguments    : Fopt *op
 * Return Type  : void
 */
void FeedoptPlan(Fopt *op)
{
    emxArray_real_T *b_BasisVal;
    emxArray_real_T *b_BasisValD;
    emxArray_real_T *b_BasisValDD;
    emxArray_real_T *b_BasisIntegr;
    emxArray_real_T *y;
    boolean_T guard1 = false;
    int status;
    int i;
    int loop_ub;
    double tmp_data[120];
    CurvStruct value;
    PushStatus push_status;
    int k1temp;
    CurvStruct OptSegment[10];
    static const CurvStruct r = { CurveType_Line,/* Type */
        ZSpdMode_NN,                   /* ZSpdMode */
        { 1.0, 0.0, 0.0 },             /* P0 */
        { 0.0, 0.0, 0.0 },             /* P1 */
        { 0.0, 0.0, 0.0 },             /* evec */
        0.0,                           /* theta */
        0.0,                           /* pitch */
        { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0,
                0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 } },/* CoeffP5 */
        0.2                            /* FeedRate */
    };

    double xvec_data[200];
    int i1;
    double Coeff_data[1200];
    int Coeff_size[2];
    signed char b_tmp_data[120];
    unsigned long Bl_handle;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    emxInit_real_T(&b_BasisVal, 2);
    emxInit_real_T(&b_BasisValD, 2);
    emxInit_real_T(&b_BasisValDD, 2);
    emxInit_real_T(&b_BasisIntegr, 1);
    emxInit_real_T(&y, 2);
    if (!Bl_not_empty) {
        Bl.n = (g_FeedoptConfig.NBreak + g_FeedoptConfig.SplineDegree) - 2;
        c_bspline_create(&Bl.handle, 0.0, 1.0, g_FeedoptConfig.SplineDegree,
                         g_FeedoptConfig.NBreak);
        Bl_not_empty = true;
        linspace(g_FeedoptConfig.NDiscr, y);
        i = y->size[0] * y->size[1];
        y->size[0] = 1;
        emxEnsureCapacity_real_T(y, i);
        loop_ub = y->size[1];
        for (i = 0; i < loop_ub; i++) {
            y->data[i] *= 3.1415926535897931;
        }

        i = y->size[1];
        for (status = 0; status < i; status++) {
            y->data[status] = cos(y->data[status]);
        }

        u_vec.size[0] = 1;
        u_vec.size[1] = y->size[1];
        loop_ub = y->size[1];
        for (i = 0; i < loop_ub; i++) {
            u_vec.data[i] = y->data[i] * 0.5 + 0.5;
        }

        loop_ub = u_vec.size[1];
        if (0 <= loop_ub - 1) {
            memcpy(&xvec_data[0], &u_vec.data[0], loop_ub * sizeof(double));
        }

        /*  n, bspline_n */
        status = u_vec.size[1];
        i = b_BasisVal->size[0] * b_BasisVal->size[1];
        b_BasisVal->size[0] = u_vec.size[1];
        b_BasisVal->size[1] = Bl.n;
        emxEnsureCapacity_real_T(b_BasisVal, i);
        loop_ub = Bl.n;
        for (i = 0; i < loop_ub; i++) {
            for (i1 = 0; i1 < status; i1++) {
                b_BasisVal->data[i1 + b_BasisVal->size[0] * i] = 0.0;
            }
        }

        status = u_vec.size[1];
        i = b_BasisValD->size[0] * b_BasisValD->size[1];
        b_BasisValD->size[0] = u_vec.size[1];
        b_BasisValD->size[1] = Bl.n;
        emxEnsureCapacity_real_T(b_BasisValD, i);
        loop_ub = Bl.n;
        for (i = 0; i < loop_ub; i++) {
            for (i1 = 0; i1 < status; i1++) {
                b_BasisValD->data[i1 + b_BasisValD->size[0] * i] = 0.0;
            }
        }

        status = u_vec.size[1];
        i = b_BasisValDD->size[0] * b_BasisValDD->size[1];
        b_BasisValDD->size[0] = u_vec.size[1];
        b_BasisValDD->size[1] = Bl.n;
        emxEnsureCapacity_real_T(b_BasisValDD, i);
        loop_ub = Bl.n;
        for (i = 0; i < loop_ub; i++) {
            for (i1 = 0; i1 < status; i1++) {
                b_BasisValDD->data[i1 + b_BasisValDD->size[0] * i] = 0.0;
            }
        }

        loop_ub = Bl.n;
        i = b_BasisIntegr->size[0];
        b_BasisIntegr->size[0] = Bl.n;
        emxEnsureCapacity_real_T(b_BasisIntegr, i);
        for (i = 0; i < loop_ub; i++) {
            b_BasisIntegr->data[i] = 0.0;
        }

        /* , */
        c_bspline_base_eval(&Bl.handle, u_vec.size[1], &xvec_data[0],
                            &b_BasisVal->data[0], &b_BasisValD->data[0],
                            &b_BasisValDD->data[0], &b_BasisIntegr->data[0]);
        BasisVal.size[0] = b_BasisVal->size[0];
        BasisVal.size[1] = b_BasisVal->size[1];
        loop_ub = b_BasisVal->size[1];
        for (i = 0; i < loop_ub; i++) {
            k1temp = b_BasisVal->size[0];
            for (i1 = 0; i1 < k1temp; i1++) {
                BasisVal.data[i1 + BasisVal.size[0] * i] = b_BasisVal->data[i1 +
                    b_BasisVal->size[0] * i];
            }
        }

        BasisValD.size[0] = b_BasisValD->size[0];
        BasisValD.size[1] = b_BasisValD->size[1];
        loop_ub = b_BasisValD->size[1];
        for (i = 0; i < loop_ub; i++) {
            k1temp = b_BasisValD->size[0];
            for (i1 = 0; i1 < k1temp; i1++) {
                BasisValD.data[i1 + BasisValD.size[0] * i] = b_BasisValD->
                    data[i1 + b_BasisValD->size[0] * i];
            }
        }

        BasisValDD.size[0] = b_BasisValDD->size[0];
        BasisValDD.size[1] = b_BasisValDD->size[1];
        loop_ub = b_BasisValDD->size[1];
        for (i = 0; i < loop_ub; i++) {
            k1temp = b_BasisValDD->size[0];
            for (i1 = 0; i1 < k1temp; i1++) {
                BasisValDD.data[i1 + BasisValDD.size[0] * i] =
                    b_BasisValDD->data[i1 + b_BasisValDD->size[0] * i];
            }
        }

        BasisIntegr.size[0] = b_BasisIntegr->size[0];
        loop_ub = b_BasisIntegr->size[0];
        for (i = 0; i < loop_ub; i++) {
            BasisIntegr.data[i] = b_BasisIntegr->data[i];
        }

        Coeff.size[0] = g_FeedoptConfig.MaxNCoeff;
        Coeff.size[1] = g_FeedoptConfig.MaxNHorz;
        loop_ub = g_FeedoptConfig.MaxNHorz;
        for (i = 0; i < loop_ub; i++) {
            k1temp = g_FeedoptConfig.MaxNCoeff;
            for (i1 = 0; i1 < k1temp; i1++) {
                Coeff.data[i1 + Coeff.size[0] * i] = 0.0;
            }
        }

        v_0 = g_FeedoptConfig.v_0;
        at_0 = g_FeedoptConfig.at_0;
        v_1 = g_FeedoptConfig.v_1;
        at_1 = g_FeedoptConfig.at_1;
        printf("FEEDOPT_PLAN INITIAL RUN\n");
        fflush(stdout);
    }

    guard1 = false;
    switch (*op) {
      case Fopt_Init:
        QueueGCode.id = (int)QueueId_GCode;
        QueueSmooth.id = (int)QueueId_Smooth;
        QueueSplit.id = (int)QueueId_Split;
        QueueOpt.id = (int)QueueId_Opt;
        printf("Queue.Clear id = %d\n", QueueGCode.id);
        fflush(stdout);
        queue_clear(QueueGCode.id);
        printf("Queue.Clear id = %d\n", QueueSmooth.id);
        fflush(stdout);
        queue_clear(QueueSmooth.id);
        printf("Queue.Clear id = %d\n", QueueSplit.id);
        fflush(stdout);
        queue_clear(QueueSplit.id);
        printf("Queue.Clear id = %d\n", QueueOpt.id);
        fflush(stdout);
        queue_clear(QueueOpt.id);
        *op = Fopt_GCode;
        Noptimized = 0;
        k0 = 1;
        v_0 = g_FeedoptConfig.v_0;
        at_0 = g_FeedoptConfig.at_0;
        v_1 = g_FeedoptConfig.v_1;
        at_1 = g_FeedoptConfig.at_1;
        linspace(g_FeedoptConfig.NDiscr, y);
        i = y->size[0] * y->size[1];
        y->size[0] = 1;
        emxEnsureCapacity_real_T(y, i);
        loop_ub = y->size[1];
        for (i = 0; i < loop_ub; i++) {
            y->data[i] *= 3.1415926535897931;
        }

        i = y->size[1];
        for (status = 0; status < i; status++) {
            y->data[status] = cos(y->data[status]);
        }

        u_vec.size[0] = 1;
        u_vec.size[1] = y->size[1];
        loop_ub = y->size[1];
        for (i = 0; i < loop_ub; i++) {
            u_vec.data[i] = y->data[i] * 0.5 + 0.5;
        }

        c_bspline_destroy(&Bl.handle);
        c_bspline_create(&Bl_handle, 0.0, 1.0, g_FeedoptConfig.SplineDegree,
                         g_FeedoptConfig.NBreak);
        Bl.n = (g_FeedoptConfig.NBreak + g_FeedoptConfig.SplineDegree) - 2;
        Bl.handle = Bl_handle;
        loop_ub = u_vec.size[1];
        if (0 <= loop_ub - 1) {
            memcpy(&xvec_data[0], &u_vec.data[0], loop_ub * sizeof(double));
        }

        /*  n, bspline_n */
        status = u_vec.size[1];
        i = b_BasisVal->size[0] * b_BasisVal->size[1];
        b_BasisVal->size[0] = u_vec.size[1];
        b_BasisVal->size[1] = Bl.n;
        emxEnsureCapacity_real_T(b_BasisVal, i);
        loop_ub = Bl.n;
        for (i = 0; i < loop_ub; i++) {
            for (i1 = 0; i1 < status; i1++) {
                b_BasisVal->data[i1 + b_BasisVal->size[0] * i] = 0.0;
            }
        }

        status = u_vec.size[1];
        i = b_BasisValD->size[0] * b_BasisValD->size[1];
        b_BasisValD->size[0] = u_vec.size[1];
        b_BasisValD->size[1] = Bl.n;
        emxEnsureCapacity_real_T(b_BasisValD, i);
        loop_ub = Bl.n;
        for (i = 0; i < loop_ub; i++) {
            for (i1 = 0; i1 < status; i1++) {
                b_BasisValD->data[i1 + b_BasisValD->size[0] * i] = 0.0;
            }
        }

        status = u_vec.size[1];
        i = b_BasisValDD->size[0] * b_BasisValDD->size[1];
        b_BasisValDD->size[0] = u_vec.size[1];
        b_BasisValDD->size[1] = Bl.n;
        emxEnsureCapacity_real_T(b_BasisValDD, i);
        loop_ub = Bl.n;
        for (i = 0; i < loop_ub; i++) {
            for (i1 = 0; i1 < status; i1++) {
                b_BasisValDD->data[i1 + b_BasisValDD->size[0] * i] = 0.0;
            }
        }

        loop_ub = Bl.n;
        i = b_BasisIntegr->size[0];
        b_BasisIntegr->size[0] = Bl.n;
        emxEnsureCapacity_real_T(b_BasisIntegr, i);
        for (i = 0; i < loop_ub; i++) {
            b_BasisIntegr->data[i] = 0.0;
        }

        /* , */
        c_bspline_base_eval(&Bl.handle, u_vec.size[1], &xvec_data[0],
                            &b_BasisVal->data[0], &b_BasisValD->data[0],
                            &b_BasisValDD->data[0], &b_BasisIntegr->data[0]);
        BasisVal.size[0] = b_BasisVal->size[0];
        BasisVal.size[1] = b_BasisVal->size[1];
        loop_ub = b_BasisVal->size[1];
        for (i = 0; i < loop_ub; i++) {
            k1temp = b_BasisVal->size[0];
            for (i1 = 0; i1 < k1temp; i1++) {
                BasisVal.data[i1 + BasisVal.size[0] * i] = b_BasisVal->data[i1 +
                    b_BasisVal->size[0] * i];
            }
        }

        BasisValD.size[0] = b_BasisValD->size[0];
        BasisValD.size[1] = b_BasisValD->size[1];
        loop_ub = b_BasisValD->size[1];
        for (i = 0; i < loop_ub; i++) {
            k1temp = b_BasisValD->size[0];
            for (i1 = 0; i1 < k1temp; i1++) {
                BasisValD.data[i1 + BasisValD.size[0] * i] = b_BasisValD->
                    data[i1 + b_BasisValD->size[0] * i];
            }
        }

        BasisValDD.size[0] = b_BasisValDD->size[0];
        BasisValDD.size[1] = b_BasisValDD->size[1];
        loop_ub = b_BasisValDD->size[1];
        for (i = 0; i < loop_ub; i++) {
            k1temp = b_BasisValDD->size[0];
            for (i1 = 0; i1 < k1temp; i1++) {
                BasisValDD.data[i1 + BasisValDD.size[0] * i] =
                    b_BasisValDD->data[i1 + b_BasisValDD->size[0] * i];
            }
        }

        BasisIntegr.size[0] = b_BasisIntegr->size[0];
        loop_ub = b_BasisIntegr->size[0];
        for (i = 0; i < loop_ub; i++) {
            BasisIntegr.data[i] = b_BasisIntegr->data[i];
        }

        Coeff.size[0] = g_FeedoptConfig.MaxNCoeff;
        Coeff.size[1] = g_FeedoptConfig.MaxNHorz;
        loop_ub = g_FeedoptConfig.MaxNHorz;
        for (i = 0; i < loop_ub; i++) {
            k1temp = g_FeedoptConfig.MaxNCoeff;
            for (i1 = 0; i1 < k1temp; i1++) {
                Coeff.data[i1 + Coeff.size[0] * i] = 0.0;
            }
        }

        TryAgain = false;
        printf("Size of u_vec: [%d %d]\n", 1, u_vec.size[1]);
        fflush(stdout);
        printf("Size of BasisIntegr: [%d %d]\n", BasisIntegr.size[0], 1);
        fflush(stdout);
        ReachedEnd = false;
        NCoeff = 0;
        printf("Starting optimization with NHorz = %d\n", g_FeedoptConfig.NHorz);
        fflush(stdout);
        break;

      case Fopt_GCode:
        printf("Queue.Clear id = %d\n", QueueGCode.id);
        fflush(stdout);
        queue_clear(QueueGCode.id);
        printf("Queue.Clear id = %d\n", QueueSmooth.id);
        fflush(stdout);
        queue_clear(QueueSmooth.id);
        printf("Queue.Clear id = %d\n", QueueSplit.id);
        fflush(stdout);
        queue_clear(QueueSplit.id);
        printf("Queue.Clear id = %d\n", QueueOpt.id);
        fflush(stdout);
        queue_clear(QueueOpt.id);
        status = 1;
        while (status != 0) {
            /*  Wrapper for pulling the next gcode line from the interpreter */
            status = feedopt_read_gcode(QueueGCode.id);
        }

        *op = Fopt_Smooth;
        break;

      case Fopt_Smooth:
        SmoothCurvStructs(QueueGCode.id, QueueSmooth.id, g_FeedoptConfig.CutOff);
        *op = Fopt_Split;
        break;

      case Fopt_Split:
        SplitCurvStructs(QueueSmooth.id, QueueSplit.id, g_FeedoptConfig.LSplit);
        *op = Fopt_Opt;
        break;

      case Fopt_Opt:
        status = queue_size(QueueSplit.id);
        if (status == 0) {
            *op = Fopt_Finished;
        } else {
            *op = Fopt_Opt;

            /*          C1 = QueueSplit.Get(1); */
            /*          C1.FeedRate = 10; */
            /*          QueueSplit.Set(1, C1); */
            status = queue_size(QueueSplit.id);
            if (Noptimized < status) {
                /*  @TODO: Handle NN, NZ ... here */
                k1temp = (k0 + g_FeedoptConfig.NHorz) - 1;
                status = queue_size(QueueSplit.id);
                if (k1temp > status) {
                    ReachedEnd = true;
                    k1temp = queue_size(QueueSplit.id);
                }

                /*              fprintf('Opt %d, k0 = %2d, k1 = %2d\n', Noptimized + 1, k0, k1); */
                if (TryAgain) {
                    /*  Do nothing, we already have the last one optimized */
                    guard1 = true;
                } else if (!ReachedEnd) {
                    for (i = 0; i < 10; i++) {
                        OptSegment[i] = r;
                    }

                    if (g_FeedoptConfig.DebugPrint) {
                        printf("============= FEEDRATE PLANNING ================\n");
                        fflush(stdout);
                    }

                    for (status = k0; status <= k1temp; status++) {
                        /*  Forward Get */
                        OptSegment[status - k0] = queue_get(QueueSplit.id,
                            status - 1);
                        PrintCurvStruct(&OptSegment[status - k0]);
                        if ((status < k1temp) && g_FeedoptConfig.DebugPrint) {
                            printf("-----------------------------------\n");
                            fflush(stdout);
                        }
                    }

                    if (g_FeedoptConfig.DebugPrint) {
                        printf("================================================\n");
                        fflush(stdout);
                    }

                    b_FeedratePlanning_v4(OptSegment, g_FeedoptConfig.amax,
                                          g_FeedoptConfig.jmax, &v_0, &at_0, v_1,
                                          at_1, BasisVal.data, BasisVal.size,
                                          BasisValD.data, BasisValD.size,
                                          BasisValDD.data, BasisValDD.size,
                                          BasisIntegr.data, BasisIntegr.size,
                                          Bl.handle, u_vec.data, u_vec.size,
                                          g_FeedoptConfig.NHorz, Coeff_data,
                                          Coeff_size, &NCoeff, &status);
                    Coeff.size[0] = Coeff_size[0];
                    Coeff.size[1] = Coeff_size[1];
                    loop_ub = Coeff_size[1];
                    for (i = 0; i < loop_ub; i++) {
                        k1temp = Coeff_size[0];
                        for (i1 = 0; i1 < k1temp; i1++) {
                            Coeff.data[i1 + Coeff.size[0] * i] = Coeff_data[i1 +
                                Coeff_size[0] * i];
                        }
                    }

                    if (status == 0) {
                        printf("OPTIMIZATION FAILED!\n");
                        fflush(stdout);
                        *op = Fopt_Finished;
                    } else {
                        guard1 = true;
                    }
                } else {
                    /*  If we have reached the end of the optimizing segment, we */
                    /*  can just copy out the coefficients for the whole horizon */
                    if (2 > Coeff.size[1]) {
                        i = 0;
                        i1 = 0;
                    } else {
                        i = 1;
                        i1 = Coeff.size[1];
                    }

                    loop_ub = (signed char)(Coeff.size[0] - 1);
                    for (status = 0; status <= loop_ub; status++) {
                        b_tmp_data[status] = (signed char)status;
                    }

                    loop_ub = Coeff.size[0];
                    k1temp = i1 - i;
                    for (i1 = 0; i1 < k1temp; i1++) {
                        for (status = 0; status < loop_ub; status++) {
                            Coeff_data[status + loop_ub * i1] =
                                Coeff.data[status + Coeff.size[0] * (i + i1)];
                        }
                    }

                    for (i = 0; i < k1temp; i++) {
                        for (i1 = 0; i1 < loop_ub; i1++) {
                            Coeff.data[b_tmp_data[i1] + Coeff.size[0] * i] =
                                Coeff_data[i1 + loop_ub * i];
                        }
                    }

                    guard1 = true;
                }
            } else {
                *op = Fopt_Finished;
            }
        }
        break;

      default:
        *op = Fopt_Finished;
        break;
    }

    if (guard1) {
        loop_ub = Coeff.size[0];
        if (0 <= loop_ub - 1) {
            memcpy(&tmp_data[0], &Coeff.data[0], loop_ub * sizeof(double));
        }

        /*  Forward Get */
        value = queue_get(QueueSplit.id, Noptimized);
        push_status = feedopt_push_opt(value, &tmp_data[0], NCoeff);
        switch (push_status) {
          case PushStatus_Success:
            TryAgain = false;
            k0++;
            Noptimized++;
            break;

          case PushStatus_TryAgain:
            TryAgain = true;
            break;

          default:
            TryAgain = false;
            *op = Fopt_Finished;
            break;
        }
    }

    emxFree_real_T(&y);
    emxFree_real_T(&b_BasisIntegr);
    emxFree_real_T(&b_BasisValDD);
    emxFree_real_T(&b_BasisValD);
    emxFree_real_T(&b_BasisVal);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void FeedoptPlan_init(void)
{
    BasisIntegr.size[0] = 0;
    Coeff.size[1] = 0;
    u_vec.size[1] = 0;
    BasisValDD.size[1] = 0;
    BasisValD.size[1] = 0;
    BasisVal.size[1] = 0;
    QueueGCode.id = (int)QueueId_GCode;
    QueueSmooth.id = (int)QueueId_Smooth;
    QueueSplit.id = (int)QueueId_Split;
    QueueOpt.id = (int)QueueId_Opt;
    Noptimized = 0;
    k0 = 1;
    ReachedEnd = false;
    NCoeff = 0;
    TryAgain = false;
}

/*
 * File trailer for FeedoptPlan.c
 *
 * [EOF]
 */
