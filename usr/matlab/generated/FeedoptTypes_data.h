/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptTypes_data.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef FEEDOPTTYPES_DATA_H
#define FEEDOPTTYPES_DATA_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Variable Declarations */
extern FeedoptConfigStruct g_FeedoptConfig;
extern const char cv[30];
extern const char cv1[16];
extern const char cv2[13];
extern const char cv3[29];
extern boolean_T isInitialized_FeedoptTypes;

#endif

/*
 * File trailer for FeedoptTypes_data.h
 *
 * [EOF]
 */
