/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: sinspace.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "sinspace.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "linspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : double x0
 *                double x1
 *                int N
 *                double x_data[]
 *                int x_size[2]
 * Return Type  : void
 */
void sinspace(double x0, double x1, int N, double x_data[], int x_size[2])
{
    emxArray_real_T *y;
    int i;
    int loop_ub;
    double b;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    emxInit_real_T(&y, 2);
    linspace(N, y);
    i = y->size[0] * y->size[1];
    y->size[0] = 1;
    emxEnsureCapacity_real_T(y, i);
    loop_ub = y->size[1];
    for (i = 0; i < loop_ub; i++) {
        y->data[i] *= 3.1415926535897931;
    }

    i = y->size[1];
    for (loop_ub = 0; loop_ub < i; loop_ub++) {
        y->data[loop_ub] = cos(y->data[loop_ub]);
    }

    b = x1 - x0;
    x_size[0] = 1;
    x_size[1] = y->size[1];
    loop_ub = y->size[1];
    for (i = 0; i < loop_ub; i++) {
        x_data[i] = (y->data[i] * 0.5 + 0.5) * b + x0;
    }

    emxFree_real_T(&y);
}

/*
 * File trailer for sinspace.c
 *
 * [EOF]
 */
