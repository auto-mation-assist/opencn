/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: ConstrHelixStruct.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "ConstrHelixStruct.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : const double P0[3]
 *                const double P1[3]
 *                const double evec[3]
 *                double theta
 *                double pitch
 *                double FeedRate
 *                ZSpdMode b_ZSpdMode
 *                CurvStruct *b_CurvStruct
 * Return Type  : void
 */
void ConstrHelixStruct(const double P0[3], const double P1[3], const double
                       evec[3], double theta, double pitch, double FeedRate,
                       ZSpdMode b_ZSpdMode, CurvStruct *b_CurvStruct)
{
    int i;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    /*  */
    b_CurvStruct->ZSpdMode = b_ZSpdMode;
    b_CurvStruct->theta = theta;
    b_CurvStruct->pitch = pitch;
    b_CurvStruct->FeedRate = FeedRate;

    /*  */
    /*  */
    b_CurvStruct->Type = CurveType_Helix;
    b_CurvStruct->P0[0] = P0[0];
    b_CurvStruct->P1[0] = P1[0];
    b_CurvStruct->evec[0] = evec[0];
    b_CurvStruct->P0[1] = P0[1];
    b_CurvStruct->P1[1] = P1[1];
    b_CurvStruct->evec[1] = evec[1];
    b_CurvStruct->P0[2] = P0[2];
    b_CurvStruct->P1[2] = P1[2];
    b_CurvStruct->evec[2] = evec[2];
    for (i = 0; i < 6; i++) {
        b_CurvStruct->CoeffP5[i][0] = 0.0;
        b_CurvStruct->CoeffP5[i][1] = 0.0;
        b_CurvStruct->CoeffP5[i][2] = 0.0;
    }
}

/*
 * File trailer for ConstrHelixStruct.c
 *
 * [EOF]
 */
