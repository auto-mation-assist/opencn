/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: power.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "power.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const double a_data[]
 *                const int a_size[2]
 *                double y_data[]
 *                int y_size[2]
 * Return Type  : void
 */
void power(const double a_data[], const int a_size[2], double y_data[], int
           y_size[2])
{
    int N;
    int k;
    int y_data_tmp;
    y_size[0] = 3;
    y_size[1] = (unsigned char)a_size[1];
    N = (unsigned char)a_size[1];
    for (k = 0; k < N; k++) {
        y_data[3 * k] = pow(a_data[3 * k], 2.0);
        y_data_tmp = 3 * k + 1;
        y_data[y_data_tmp] = pow(a_data[y_data_tmp], 2.0);
        y_data_tmp = 3 * k + 2;
        y_data[y_data_tmp] = pow(a_data[y_data_tmp], 2.0);
    }
}

/*
 * File trailer for power.c
 *
 * [EOF]
 */
