/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_simplex.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "c_simplex.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include "vertcat.h"
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : const double f_data[]
 *                const int f_size[2]
 *                const coder_internal_sparse A
 *                const double b_data[]
 *                const int b_size[1]
 *                const double Aeq_data[]
 *                const int Aeq_size[2]
 *                const double beq_data[]
 *                const int beq_size[1]
 *                double C_data[]
 *                int C_size[1]
 *                int *success
 * Return Type  : void
 */
void c_simplex(const double f_data[], const int f_size[2], const
               coder_internal_sparse A, const double b_data[], const int b_size
               [1], const double Aeq_data[], const int Aeq_size[2], const double
               beq_data[], const int beq_size[1], double C_data[], int C_size[1],
               int *success)
{
    emxArray_real_T *Atot_d;
    emxArray_int32_T *Atot_colidx;
    emxArray_int32_T *Atot_rowidx;
    int Atot_m;
    int Atot_n;
    int nx;
    emxArray_int32_T *ii;
    emxArray_int32_T *jj;
    emxArray_real_T *v;
    int i;
    emxArray_real_T *Avs;
    int idx;
    int col;
    emxArray_int32_T *Ais;
    emxArray_int32_T *Ajs;
    int fsize[2];
    int Asize[2];
    int bsize[2];
    int beqsize[2];
    int Csize[2];
    emxInit_real_T(&Atot_d, 1);
    emxInit_int32_T(&Atot_colidx, 1);
    emxInit_int32_T(&Atot_rowidx, 1);
    PROF_IN(c_simplex);
    PROF_IN(join_A);
    sparse_vertcat(A.d, A.colidx, A.rowidx, A.m, A.n, Aeq_data, Aeq_size, Atot_d,
                   Atot_colidx, Atot_rowidx, &Atot_m, &Atot_n);
    PROF_OUT(join_A);
    C_size[0] = 1200;
    memset(&C_data[0], 0, 1200U * sizeof(double));
    nx = Atot_colidx->data[Atot_colidx->size[0] - 1] - 2;
    emxInit_int32_T(&ii, 1);
    emxInit_int32_T(&jj, 1);
    emxInit_real_T(&v, 1);
    if (Atot_colidx->data[Atot_colidx->size[0] - 1] - 1 == 0) {
        ii->size[0] = 0;
        jj->size[0] = 0;
        v->size[0] = 0;
    } else {
        i = ii->size[0];
        ii->size[0] = Atot_colidx->data[Atot_colidx->size[0] - 1] - 1;
        emxEnsureCapacity_int32_T(ii, i);
        i = jj->size[0];
        jj->size[0] = Atot_colidx->data[Atot_colidx->size[0] - 1] - 1;
        emxEnsureCapacity_int32_T(jj, i);
        i = v->size[0];
        v->size[0] = Atot_colidx->data[Atot_colidx->size[0] - 1] - 1;
        emxEnsureCapacity_real_T(v, i);
        for (idx = 0; idx <= nx; idx++) {
            ii->data[idx] = Atot_rowidx->data[idx];
            v->data[idx] = Atot_d->data[idx];
        }

        idx = 0;
        col = 1;
        while (idx < nx + 1) {
            if (idx == Atot_colidx->data[col] - 1) {
                col++;
            } else {
                idx++;
                jj->data[idx - 1] = col;
            }
        }

        if (Atot_colidx->data[Atot_colidx->size[0] - 1] - 1 == 1) {
            if (idx == 0) {
                ii->size[0] = 0;
                jj->size[0] = 0;
                v->size[0] = 0;
            }
        } else {
            i = ii->size[0];
            if (1 > idx) {
                ii->size[0] = 0;
            } else {
                ii->size[0] = idx;
            }

            emxEnsureCapacity_int32_T(ii, i);
            i = jj->size[0];
            if (1 > idx) {
                jj->size[0] = 0;
            } else {
                jj->size[0] = idx;
            }

            emxEnsureCapacity_int32_T(jj, i);
            i = v->size[0];
            if (1 > idx) {
                v->size[0] = 0;
            } else {
                v->size[0] = idx;
            }

            emxEnsureCapacity_real_T(v, i);
        }
    }

    emxFree_int32_T(&Atot_rowidx);
    emxFree_int32_T(&Atot_colidx);
    emxFree_real_T(&Atot_d);
    emxInit_real_T(&Avs, 1);
    i = Avs->size[0];
    Avs->size[0] = v->size[0];
    emxEnsureCapacity_real_T(Avs, i);
    nx = v->size[0];
    for (i = 0; i < nx; i++) {
        Avs->data[i] = v->data[i];
    }

    emxInit_int32_T(&Ais, 1);
    i = Ais->size[0];
    Ais->size[0] = ii->size[0];
    emxEnsureCapacity_int32_T(Ais, i);
    nx = ii->size[0];
    for (i = 0; i < nx; i++) {
        Ais->data[i] = ii->data[i] - 1;
    }

    emxFree_int32_T(&ii);
    emxInit_int32_T(&Ajs, 1);
    i = Ajs->size[0];
    Ajs->size[0] = jj->size[0];
    emxEnsureCapacity_int32_T(Ajs, i);
    nx = jj->size[0];
    for (i = 0; i < nx; i++) {
        Ajs->data[i] = jj->data[i] - 1;
    }

    emxFree_int32_T(&jj);

    /*  25000 was estimated when running it with limit values */
    fsize[0] = f_size[0];
    fsize[1] = f_size[1];
    Asize[0] = Atot_m;
    Asize[1] = Atot_n;
    nx = 0;
    i = v->size[0];
    for (col = 0; col < i; col++) {
        if (v->data[col] != 0.0) {
            nx++;
        }
    }

    emxFree_real_T(&v);
    bsize[0] = b_size[0];
    bsize[1] = 1;
    beqsize[0] = beq_size[0];
    beqsize[1] = 1;
    Csize[0] = 1200;
    Csize[1] = 1;
    *success = simplex_solve(&f_data[0], &fsize[0], &Avs->data[0], &Ais->data[0],
        &Ajs->data[0], &Asize[0], nx, &b_data[0], &bsize[0], &beq_data[0],
        &beqsize[0], &C_data[0], &Csize[0]);

    /*          C = solution.solution;  */
    PROF_OUT(c_simplex);
    emxFree_int32_T(&Ajs);
    emxFree_int32_T(&Ais);
    emxFree_real_T(&Avs);
}

/*
 * File trailer for c_simplex.c
 *
 * [EOF]
 */
