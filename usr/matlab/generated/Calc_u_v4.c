/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: Calc_u_v4.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "Calc_u_v4.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "c_spline.h"
#include "sinspace.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

/* Function Definitions */

/*
 * import splines.*
 * Arguments    : const struct0_T *b_Bl
 *                const double Coeff_data[]
 *                const int Coeff_size[1]
 *                double dt
 *                double u[10000]
 *                int *N
 * Return Type  : void
 */
void Calc_u_v4(const struct0_T *b_Bl, const double Coeff_data[], const int
               Coeff_size[1], double dt, double u[10000], int *N)
{
    double ukp1;
    boolean_T exitg1;
    int loop_ub;
    double coeffs_data[120];
    double qk;
    double X[3];
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    memset(&u[0], 0, 10000U * sizeof(double));

    /*  */
    ukp1 = 0.0;

    /*  u    = uk; */
    *N = 1;

    /*  number of samples */
    /*  */
    /*  qSpl  = Function(Bl, Coeff(:, 1)); */
    /*  qSplD = qSpl.derivative; */
    /*  */
    /*  */
    exitg1 = false;
    while ((!exitg1) && (*N <= 10000)) {
        /*      qk   = qSpl.fast_eval(uk); */
        /*      dk   = qSplD.fast_eval(uk); */
        loop_ub = Coeff_size[0];
        if (0 <= loop_ub - 1) {
            memcpy(&coeffs_data[0], &Coeff_data[0], loop_ub * sizeof(double));
        }

        qk = ukp1;

        /*  void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]); */
        if (ukp1 < 0.0) {
            printf("ERROR: C_BSPLINE_EVAL: X < 0 (%f)\n", ukp1);
            fflush(stdout);
            qk = 0.0;
        }

        c_bspline_eval(&b_Bl->handle, &coeffs_data[0], qk, &X[0]);
        ukp1 = (ukp1 + X[1] * pow(dt, 2.0) / 4.0) + sqrt(X[0]) * dt;

        /*  */
        if (ukp1 <= 0.999) {
            u[*N - 1] = ukp1;
            (*N)++;

            /*      else */
            /*  %         u_cell(k) = u;   % store previous calculated values */
            /*          ukp1      = 1; */
            /*  %         qkp1      = qSpl.fast_eval(ukp1); */
            /*          [qkp1,~] = c_bspline_eval(Bl, Coeff(:,k), ukp1); */
            /*          Trest     = 2*(ukp1 - uk) / (sqrt(qkp1) + sqrt(qk)); */
            /*          k         = k + 1; */
            /*          % */
            /*          if k > Ncrv */
            /*              break; */
            /*          end */
            /*  %         qSpl      = Function(Bl, Coeff(:, k)); */
            /*  %         qSplD     = qSpl.derivative; */
            /*          dt_begin  = dt - Trest; */
            /*          uk        = 0; */
            /*          % */
            /*  %         qk        = qSpl.fast_eval(uk); */
            /*  %         dk        = qSplD.fast_eval(uk); */
            /*          [qk, dk] = c_bspline_eval(Bl, Coeff(:,k), uk); */
            /*          ukp1      = uk + dk*dt_begin^2/4 + sqrt(qk)*dt_begin;  */
            /*          u         = ukp1; */
            /*          N         = N + 1; */
        } else {
            exitg1 = true;
        }
    }

    /*  */
    /*  t = 0:dt:(N-1)*dt; */
}

/*
 * File trailer for Calc_u_v4.c
 *
 * [EOF]
 */
