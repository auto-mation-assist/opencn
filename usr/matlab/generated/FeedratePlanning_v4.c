/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedratePlanning_v4.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "FeedratePlanning_v4.h"
#include "BenchmarkFeedratePlanning.h"
#include "BuildConstrJerk_v4.h"
#include "BuildConstr_v4.h"
#include "CalcTransition.h"
#include "CalcVAJ_v5.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "c_simplex.h"
#include "repmat.h"
#include "sinspace.h"
#include "vertcat.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

/* Variable Definitions */
static emxArray_real_T_120x10 f;

/* Function Definitions */

/*
 * Arguments    : const CurvStruct CurvStructs0_data[]
 *                const double amax[3]
 *                const double jmax[3]
 *                double b_v_0
 *                double b_at_0
 *                double b_v_1
 *                double b_at_1
 *                const emxArray_real_T *b_BasisVal
 *                const emxArray_real_T *b_BasisValD
 *                const emxArray_real_T *b_BasisValDD
 *                const emxArray_real_T *b_BasisIntegr
 *                unsigned long Bl_handle
 *                const double u_vec_data[]
 *                const int u_vec_size[2]
 *                int N_Hor
 *                double Coeff_data[]
 *                int Coeff_size[2]
 * Return Type  : void
 */
void FeedratePlanning_v4(const CurvStruct CurvStructs0_data[], const double
    amax[3], const double jmax[3], double b_v_0, double b_at_0, double b_v_1,
    double b_at_1, const emxArray_real_T *b_BasisVal, const emxArray_real_T
    *b_BasisValD, const emxArray_real_T *b_BasisValDD, const emxArray_real_T
    *b_BasisIntegr, unsigned long Bl_handle, const double u_vec_data[], const
    int u_vec_size[2], int N_Hor, double Coeff_data[], int Coeff_size[2])
{
    emxArray_real_T *r;
    int loop_ub;
    int b_loop_ub;
    int i;
    int c_loop_ub;
    int i1;
    int CurvStructs0_size[2];
    CurvStruct b_CurvStructs0_data[10];
    coder_internal_sparse A;
    static double b_data[14000];
    int b_size[1];
    static double Aeq_data[26400];
    int Aeq_size[2];
    double beq_data[22];
    int beq_size[1];
    double Coeff0_data[1200];
    int Coeff0_size[1];
    int N[2];
    emxArray_real_T *Atot_d;
    emxArray_int32_T *Atot_colidx;
    emxArray_int32_T *Atot_rowidx;
    coder_internal_sparse A_jerk;
    coder_internal_sparse expl_temp;
    double b_jerk_data[12000];
    int b_jerk_size[1];
    int b_b_size[1];
    static double b_b_data[26000];
    int b_Coeff0_size[1];
    double b_Coeff0_data[1200];
    double c_v_0;
    double a_0[3];
    double r1D[3];
    emxInit_real_T(&r, 2);

    /*  */
    PROF_IN(FeedratePlanning_v4);
    if (1 > N_Hor) {
        loop_ub = 0;
    } else {
        loop_ub = N_Hor;
    }

    /*  */
    /*  FIRST setup of Linear Program (LP) WITHOUT jerk constraint */
    c_repmat(b_BasisIntegr, (signed char)loop_ub, r);
    f.size[0] = r->size[0];
    f.size[1] = r->size[1];
    b_loop_ub = r->size[1];
    for (i = 0; i < b_loop_ub; i++) {
        c_loop_ub = r->size[0];
        for (i1 = 0; i1 < c_loop_ub; i1++) {
            f.data[i1 + f.size[0] * i] = -r->data[i1 + r->size[0] * i];
        }
    }

    emxFree_real_T(&r);

    /*  maximize integral of q */
    /*  equality constraints */
    CurvStructs0_size[0] = 1;
    CurvStructs0_size[1] = loop_ub;
    if (0 <= loop_ub - 1) {
        memcpy(&b_CurvStructs0_data[0], &CurvStructs0_data[0], loop_ub * sizeof
               (CurvStruct));
    }

    c_emxInitStruct_coder_internal_(&A);
    BuildConstr_v4(b_CurvStructs0_data, CurvStructs0_size, amax, b_v_0, b_at_0,
                   b_v_1, b_at_1, b_BasisVal, b_BasisValD, u_vec_data,
                   u_vec_size, &A, b_data, b_size, Aeq_data, Aeq_size, beq_data,
                   beq_size);

    /*  */
    /*  */
    /*  LP solver options         */
    /*  options = optimoptions('linprog', 'Algorithm', 'interior-point', 'OptimalityTolerance', 1e-6, ... */
    /*                         'ConstraintTolerance', 1e-6, 'MaxIterations', 2000); */
    /*  options = optimoptions('linprog', 'OptimalityTolerance', 1e-6, ... */
    /*                         'ConstraintTolerance', 1e-6, 'Display', 'off'); */
    /*  run LP solver first time */
    /*  Coeff0 = linprog(f, A, b, Aeq, beq, [], [], options); */
    c_simplex(f.data, f.size, A, b_data, b_size, Aeq_data, Aeq_size, beq_data,
              beq_size, Coeff0_data, Coeff0_size, &b_loop_ub);

    /*  */
    /*  tic */
    /*  Coeff1 = linprog(f, A, b, Aeq, beq, [], [], options); */
    /*  toc Coeff(:, end-N_Hor+2:end) = C(:, 2:end); */
    /*  */
    if (b_loop_ub == 0) {
        Coeff_size[0] = 0;
        Coeff_size[1] = 0;
    } else {
        if (g_FeedoptConfig.DebugPrint) {
            printf("Coeff1 = ");
            fflush(stdout);
        }

        i = b_BasisVal->size[1];
        for (b_loop_ub = 0; b_loop_ub < i; b_loop_ub++) {
            if (g_FeedoptConfig.DebugPrint) {
                printf("%.4f ", Coeff0_data[b_loop_ub]);
                fflush(stdout);
            }
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("\n");
            fflush(stdout);
        }

        /*  */
        /*  SECOND setup of Linear Program (LP) WITH jerk constraint */
        N[0] = b_BasisVal->size[1];
        N[1] = (signed char)loop_ub;
        CurvStructs0_size[0] = 1;
        CurvStructs0_size[1] = loop_ub;
        if (0 <= loop_ub - 1) {
            memcpy(&b_CurvStructs0_data[0], &CurvStructs0_data[0], loop_ub *
                   sizeof(CurvStruct));
        }

        emxInit_real_T(&Atot_d, 1);
        emxInit_int32_T(&Atot_colidx, 1);
        emxInit_int32_T(&Atot_rowidx, 1);
        c_emxInitStruct_coder_internal_(&A_jerk);
        c_emxInitStruct_coder_internal_(&expl_temp);
        BuildConstrJerk_v4(b_CurvStructs0_data, CurvStructs0_size, Coeff0_data,
                           N, jmax, b_BasisVal, b_BasisValD, b_BasisValDD,
                           u_vec_data, u_vec_size, &A_jerk, b_jerk_data,
                           b_jerk_size);

        /*  */
        b_sparse_vertcat(A, A_jerk, Atot_d, Atot_colidx, Atot_rowidx, &b_loop_ub,
                         &c_loop_ub);

        /*      */
        /*  */
        /*  run LP solver second time */
        /*  Coeff2 = linprog(f, Atot, btot, Aeq, beq, [], [], options); */
        expl_temp.n = c_loop_ub;
        expl_temp.m = b_loop_ub;
        i = expl_temp.rowidx->size[0];
        expl_temp.rowidx->size[0] = Atot_rowidx->size[0];
        emxEnsureCapacity_int32_T(expl_temp.rowidx, i);
        b_loop_ub = Atot_rowidx->size[0];
        c_emxFreeStruct_coder_internal_(&A_jerk);
        for (i = 0; i < b_loop_ub; i++) {
            expl_temp.rowidx->data[i] = Atot_rowidx->data[i];
        }

        emxFree_int32_T(&Atot_rowidx);
        i = expl_temp.colidx->size[0];
        expl_temp.colidx->size[0] = Atot_colidx->size[0];
        emxEnsureCapacity_int32_T(expl_temp.colidx, i);
        b_loop_ub = Atot_colidx->size[0];
        for (i = 0; i < b_loop_ub; i++) {
            expl_temp.colidx->data[i] = Atot_colidx->data[i];
        }

        emxFree_int32_T(&Atot_colidx);
        i = expl_temp.d->size[0];
        expl_temp.d->size[0] = Atot_d->size[0];
        emxEnsureCapacity_real_T(expl_temp.d, i);
        b_loop_ub = Atot_d->size[0];
        for (i = 0; i < b_loop_ub; i++) {
            expl_temp.d->data[i] = Atot_d->data[i];
        }

        emxFree_real_T(&Atot_d);
        b_b_size[0] = b_size[0] + b_jerk_size[0];
        b_loop_ub = b_size[0];
        if (0 <= b_loop_ub - 1) {
            memcpy(&b_b_data[0], &b_data[0], b_loop_ub * sizeof(double));
        }

        b_loop_ub = b_jerk_size[0];
        for (i = 0; i < b_loop_ub; i++) {
            b_b_data[i + b_size[0]] = b_jerk_data[i];
        }

        c_simplex(f.data, f.size, expl_temp, b_b_data, b_b_size, Aeq_data,
                  Aeq_size, beq_data, beq_size, Coeff0_data, Coeff0_size,
                  &b_loop_ub);
        c_emxFreeStruct_coder_internal_(&expl_temp);
        if (b_loop_ub == 0) {
            Coeff_size[0] = 0;
            Coeff_size[1] = 0;
        } else {
            /*  */
            /*  tic */
            /*  Coeff1 = linprog(f, Atot, btot, Aeq, beq, [], [], options); */
            /*  toc */
            /*  */
            if (g_FeedoptConfig.DebugPrint) {
                printf("Coeff3 = ");
                fflush(stdout);
            }

            i = b_BasisVal->size[1];
            for (b_loop_ub = 0; b_loop_ub < i; b_loop_ub++) {
                if (g_FeedoptConfig.DebugPrint) {
                    printf("%.4f ", Coeff0_data[b_loop_ub]);
                    fflush(stdout);
                }
            }

            if (g_FeedoptConfig.DebugPrint) {
                printf("\n");
                fflush(stdout);
            }

            /*  %  */
            b_loop_ub = b_BasisVal->size[1];
            b_Coeff0_size[0] = b_BasisVal->size[1];
            if (0 <= b_loop_ub - 1) {
                memcpy(&b_Coeff0_data[0], &Coeff0_data[0], b_loop_ub * sizeof
                       (double));
            }

            CalcVAJ_v5(CurvStructs0_data[0].Type, CurvStructs0_data[0].P0,
                       CurvStructs0_data[0].P1, CurvStructs0_data[0].evec,
                       CurvStructs0_data[0].theta, CurvStructs0_data[0].pitch,
                       *(double (*)[6][3])&CurvStructs0_data[0].CoeffP5[0][0],
                       Bl_handle, b_Coeff0_data, b_Coeff0_size, &c_v_0, a_0);
            b_EvalCurvStruct(CurvStructs0_data[0].Type, CurvStructs0_data[0].P0,
                             CurvStructs0_data[0].P1, CurvStructs0_data[0].evec,
                             CurvStructs0_data[0].theta, CurvStructs0_data[0].
                             pitch, *(double (*)[6][3])&CurvStructs0_data[0].
                             CoeffP5[0][0], a_0, r1D);

            /*  unit tangential vector */
            /*  tangential acceleration at the end of first piece in horizon */
            /*  Coeff = Coeff3(:, 1); */
            Coeff_size[0] = 120;
            Coeff_size[1] = 10;
            for (i = 0; i < 10; i++) {
                memset(&Coeff_data[i * 120], 0, 120U * sizeof(double));
            }

            N[0] = b_BasisVal->size[1];
            loop_ub = (signed char)loop_ub;
            for (i = 0; i < loop_ub; i++) {
                b_loop_ub = N[0];
                for (i1 = 0; i1 < b_loop_ub; i1++) {
                    Coeff_data[i1 + 120 * i] = Coeff0_data[i1 + N[0] * i];
                }
            }

            PROF_OUT(FeedratePlanning_v4);
        }
    }

    c_emxFreeStruct_coder_internal_(&A);

    /*  CurOptStruct = struct('Coeff', Coeff(1:N, 1)); */
    /*  u = 0; */
    /*  uvec = zeros(10000, 1); */
    /*  for ktick = 1:length(uvec) */
    /*      u = ResampleTick(CurOptStruct, Bl, u, 1e-4); */
    /*      uvec(ktick) = u; */
    /*  end */
    /*   */
    /*  plot(uvec); */
    /*  ylim([-0.1, 1.1]) */
    /*   */
    /*  uvec */
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void FeedratePlanning_v4_init(void)
{
    f.size[1] = 0;
}

/*
 * Arguments    : const CurvStruct CurvStructs0[10]
 *                const double amax[3]
 *                const double jmax[3]
 *                double *b_v_0
 *                double *b_at_0
 *                double b_v_1
 *                double b_at_1
 *                const double BasisVal_data[]
 *                const int BasisVal_size[2]
 *                const double BasisValD_data[]
 *                const int BasisValD_size[2]
 *                const double BasisValDD_data[]
 *                const int BasisValDD_size[2]
 *                const double BasisIntegr_data[]
 *                const int BasisIntegr_size[1]
 *                unsigned long Bl_handle
 *                const double u_vec_data[]
 *                const int u_vec_size[2]
 *                int N_Hor
 *                double Coeff_data[]
 *                int Coeff_size[2]
 *                int *b_NCoeff
 *                int *success
 * Return Type  : void
 */
void b_FeedratePlanning_v4(const CurvStruct CurvStructs0[10], const double amax
    [3], const double jmax[3], double *b_v_0, double *b_at_0, double b_v_1,
    double b_at_1, const double BasisVal_data[], const int BasisVal_size[2],
    const double BasisValD_data[], const int BasisValD_size[2], const double
    BasisValDD_data[], const int BasisValDD_size[2], const double
    BasisIntegr_data[], const int BasisIntegr_size[1], unsigned long Bl_handle,
    const double u_vec_data[], const int u_vec_size[2], int N_Hor, double
    Coeff_data[], int Coeff_size[2], int *b_NCoeff, int *success)
{
    emxArray_real_T *r;
    int loop_ub;
    emxArray_real_T b_BasisIntegr_data;
    int b_loop_ub;
    int i;
    int c_loop_ub;
    int i1;
    int CurvStructs0_size[2];
    CurvStruct CurvStructs0_data[10];
    coder_internal_sparse A;
    emxArray_real_T b_BasisVal_data;
    emxArray_real_T b_BasisValD_data;
    static double b_data[14000];
    int b_size[1];
    static double Aeq_data[26400];
    int Aeq_size[2];
    double beq_data[22];
    int beq_size[1];
    double Coeff0_data[1200];
    int Coeff0_size[1];
    int N[2];
    emxArray_real_T *Atot_d;
    emxArray_int32_T *Atot_colidx;
    emxArray_int32_T *Atot_rowidx;
    coder_internal_sparse A_jerk;
    coder_internal_sparse expl_temp;
    emxArray_real_T c_BasisVal_data;
    emxArray_real_T c_BasisValD_data;
    emxArray_real_T b_BasisValDD_data;
    double b_jerk_data[12000];
    int b_jerk_size[1];
    int b_b_size[1];
    static double b_b_data[26000];
    int b_Coeff0_size[1];
    double b_Coeff0_data[120];
    double a_0[3];
    double unusedU1[3];
    double r1D[3];
    double n;
    emxInit_real_T(&r, 2);

    /*  */
    PROF_IN(FeedratePlanning_v4);
    if (1 > N_Hor) {
        loop_ub = 0;
    } else {
        loop_ub = N_Hor;
    }

    /*  */
    /*  FIRST setup of Linear Program (LP) WITHOUT jerk constraint */
    b_BasisIntegr_data.data = (double *)&BasisIntegr_data[0];
    b_BasisIntegr_data.size = (int *)&BasisIntegr_size[0];
    b_BasisIntegr_data.allocatedSize = -1;
    b_BasisIntegr_data.numDimensions = 1;
    b_BasisIntegr_data.canFreeData = false;
    c_repmat(&b_BasisIntegr_data, (signed char)loop_ub, r);
    f.size[0] = r->size[0];
    f.size[1] = r->size[1];
    b_loop_ub = r->size[1];
    for (i = 0; i < b_loop_ub; i++) {
        c_loop_ub = r->size[0];
        for (i1 = 0; i1 < c_loop_ub; i1++) {
            f.data[i1 + f.size[0] * i] = -r->data[i1 + r->size[0] * i];
        }
    }

    emxFree_real_T(&r);

    /*  maximize integral of q */
    /*  equality constraints */
    CurvStructs0_size[0] = 1;
    CurvStructs0_size[1] = loop_ub;
    if (0 <= loop_ub - 1) {
        memcpy(&CurvStructs0_data[0], &CurvStructs0[0], loop_ub * sizeof
               (CurvStruct));
    }

    c_emxInitStruct_coder_internal_(&A);
    b_BasisVal_data.data = (double *)&BasisVal_data[0];
    b_BasisVal_data.size = (int *)&BasisVal_size[0];
    b_BasisVal_data.allocatedSize = -1;
    b_BasisVal_data.numDimensions = 2;
    b_BasisVal_data.canFreeData = false;
    b_BasisValD_data.data = (double *)&BasisValD_data[0];
    b_BasisValD_data.size = (int *)&BasisValD_size[0];
    b_BasisValD_data.allocatedSize = -1;
    b_BasisValD_data.numDimensions = 2;
    b_BasisValD_data.canFreeData = false;
    BuildConstr_v4(CurvStructs0_data, CurvStructs0_size, amax, *b_v_0, *b_at_0,
                   b_v_1, b_at_1, &b_BasisVal_data, &b_BasisValD_data,
                   u_vec_data, u_vec_size, &A, b_data, b_size, Aeq_data,
                   Aeq_size, beq_data, beq_size);

    /*  */
    /*  */
    /*  LP solver options         */
    /*  options = optimoptions('linprog', 'Algorithm', 'interior-point', 'OptimalityTolerance', 1e-6, ... */
    /*                         'ConstraintTolerance', 1e-6, 'MaxIterations', 2000); */
    /*  options = optimoptions('linprog', 'OptimalityTolerance', 1e-6, ... */
    /*                         'ConstraintTolerance', 1e-6, 'Display', 'off'); */
    /*  run LP solver first time */
    /*  Coeff0 = linprog(f, A, b, Aeq, beq, [], [], options); */
    c_simplex(f.data, f.size, A, b_data, b_size, Aeq_data, Aeq_size, beq_data,
              beq_size, Coeff0_data, Coeff0_size, success);

    /*  */
    /*  tic */
    /*  Coeff1 = linprog(f, A, b, Aeq, beq, [], [], options); */
    /*  toc Coeff(:, end-N_Hor+2:end) = C(:, 2:end); */
    /*  */
    if (*success == 0) {
        Coeff_size[0] = 0;
        Coeff_size[1] = 0;
        *b_NCoeff = 0;
    } else {
        if (g_FeedoptConfig.DebugPrint) {
            printf("Coeff1 = ");
            fflush(stdout);
        }

        i = BasisVal_size[1];
        for (b_loop_ub = 0; b_loop_ub < i; b_loop_ub++) {
            if (g_FeedoptConfig.DebugPrint) {
                printf("%.4f ", Coeff0_data[b_loop_ub]);
                fflush(stdout);
            }
        }

        if (g_FeedoptConfig.DebugPrint) {
            printf("\n");
            fflush(stdout);
        }

        /*  */
        /*  SECOND setup of Linear Program (LP) WITH jerk constraint */
        N[0] = BasisVal_size[1];
        N[1] = (signed char)loop_ub;
        CurvStructs0_size[0] = 1;
        CurvStructs0_size[1] = loop_ub;
        if (0 <= loop_ub - 1) {
            memcpy(&CurvStructs0_data[0], &CurvStructs0[0], loop_ub * sizeof
                   (CurvStruct));
        }

        emxInit_real_T(&Atot_d, 1);
        emxInit_int32_T(&Atot_colidx, 1);
        emxInit_int32_T(&Atot_rowidx, 1);
        c_emxInitStruct_coder_internal_(&A_jerk);
        c_emxInitStruct_coder_internal_(&expl_temp);
        c_BasisVal_data.data = (double *)&BasisVal_data[0];
        c_BasisVal_data.size = (int *)&BasisVal_size[0];
        c_BasisVal_data.allocatedSize = -1;
        c_BasisVal_data.numDimensions = 2;
        c_BasisVal_data.canFreeData = false;
        c_BasisValD_data.data = (double *)&BasisValD_data[0];
        c_BasisValD_data.size = (int *)&BasisValD_size[0];
        c_BasisValD_data.allocatedSize = -1;
        c_BasisValD_data.numDimensions = 2;
        c_BasisValD_data.canFreeData = false;
        b_BasisValDD_data.data = (double *)&BasisValDD_data[0];
        b_BasisValDD_data.size = (int *)&BasisValDD_size[0];
        b_BasisValDD_data.allocatedSize = -1;
        b_BasisValDD_data.numDimensions = 2;
        b_BasisValDD_data.canFreeData = false;
        BuildConstrJerk_v4(CurvStructs0_data, CurvStructs0_size, Coeff0_data, N,
                           jmax, &c_BasisVal_data, &c_BasisValD_data,
                           &b_BasisValDD_data, u_vec_data, u_vec_size, &A_jerk,
                           b_jerk_data, b_jerk_size);

        /*  */
        b_sparse_vertcat(A, A_jerk, Atot_d, Atot_colidx, Atot_rowidx, &b_loop_ub,
                         &c_loop_ub);

        /*      */
        /*  */
        /*  run LP solver second time */
        /*  Coeff2 = linprog(f, Atot, btot, Aeq, beq, [], [], options); */
        expl_temp.n = c_loop_ub;
        expl_temp.m = b_loop_ub;
        i = expl_temp.rowidx->size[0];
        expl_temp.rowidx->size[0] = Atot_rowidx->size[0];
        emxEnsureCapacity_int32_T(expl_temp.rowidx, i);
        b_loop_ub = Atot_rowidx->size[0];
        c_emxFreeStruct_coder_internal_(&A_jerk);
        for (i = 0; i < b_loop_ub; i++) {
            expl_temp.rowidx->data[i] = Atot_rowidx->data[i];
        }

        emxFree_int32_T(&Atot_rowidx);
        i = expl_temp.colidx->size[0];
        expl_temp.colidx->size[0] = Atot_colidx->size[0];
        emxEnsureCapacity_int32_T(expl_temp.colidx, i);
        b_loop_ub = Atot_colidx->size[0];
        for (i = 0; i < b_loop_ub; i++) {
            expl_temp.colidx->data[i] = Atot_colidx->data[i];
        }

        emxFree_int32_T(&Atot_colidx);
        i = expl_temp.d->size[0];
        expl_temp.d->size[0] = Atot_d->size[0];
        emxEnsureCapacity_real_T(expl_temp.d, i);
        b_loop_ub = Atot_d->size[0];
        for (i = 0; i < b_loop_ub; i++) {
            expl_temp.d->data[i] = Atot_d->data[i];
        }

        emxFree_real_T(&Atot_d);
        b_b_size[0] = b_size[0] + b_jerk_size[0];
        b_loop_ub = b_size[0];
        if (0 <= b_loop_ub - 1) {
            memcpy(&b_b_data[0], &b_data[0], b_loop_ub * sizeof(double));
        }

        b_loop_ub = b_jerk_size[0];
        for (i = 0; i < b_loop_ub; i++) {
            b_b_data[i + b_size[0]] = b_jerk_data[i];
        }

        c_simplex(f.data, f.size, expl_temp, b_b_data, b_b_size, Aeq_data,
                  Aeq_size, beq_data, beq_size, Coeff0_data, Coeff0_size,
                  success);
        c_emxFreeStruct_coder_internal_(&expl_temp);
        if (*success == 0) {
            Coeff_size[0] = 0;
            Coeff_size[1] = 0;
            *b_NCoeff = 0;
        } else {
            /*  */
            /*  tic */
            /*  Coeff1 = linprog(f, Atot, btot, Aeq, beq, [], [], options); */
            /*  toc */
            /*  */
            if (g_FeedoptConfig.DebugPrint) {
                printf("Coeff3 = ");
                fflush(stdout);
            }

            i = BasisVal_size[1];
            for (b_loop_ub = 0; b_loop_ub < i; b_loop_ub++) {
                if (g_FeedoptConfig.DebugPrint) {
                    printf("%.4f ", Coeff0_data[b_loop_ub]);
                    fflush(stdout);
                }
            }

            if (g_FeedoptConfig.DebugPrint) {
                printf("\n");
                fflush(stdout);
            }

            /*  %  */
            b_loop_ub = BasisVal_size[1];
            b_Coeff0_size[0] = BasisVal_size[1];
            if (0 <= b_loop_ub - 1) {
                memcpy(&b_Coeff0_data[0], &Coeff0_data[0], b_loop_ub * sizeof
                       (double));
            }

            CalcVAJ_v5(CurvStructs0[0].Type, CurvStructs0[0].P0, CurvStructs0[0]
                       .P1, CurvStructs0[0].evec, CurvStructs0[0].theta,
                       CurvStructs0[0].pitch, *(double (*)[6][3])&CurvStructs0[0]
                       .CoeffP5[0][0], Bl_handle, b_Coeff0_data, b_Coeff0_size,
                       b_v_0, a_0);
            b_EvalCurvStruct(CurvStructs0[0].Type, CurvStructs0[0].P0,
                             CurvStructs0[0].P1, CurvStructs0[0].evec,
                             CurvStructs0[0].theta, CurvStructs0[0].pitch,
                             *(double (*)[6][3])&CurvStructs0[0].CoeffP5[0][0],
                             unusedU1, r1D);
            n = sqrt((pow(r1D[0], 2.0) + pow(r1D[1], 2.0)) + pow(r1D[2], 2.0));

            /*  unit tangential vector */
            *b_at_0 = (a_0[0] * (r1D[0] / n) + a_0[1] * (r1D[1] / n)) + a_0[2] *
                (r1D[2] / n);

            /*  tangential acceleration at the end of first piece in horizon */
            /*  Coeff = Coeff3(:, 1); */
            Coeff_size[0] = 120;
            Coeff_size[1] = 10;
            for (i = 0; i < 10; i++) {
                memset(&Coeff_data[i * 120], 0, 120U * sizeof(double));
            }

            N[0] = BasisVal_size[1];
            loop_ub = (signed char)loop_ub;
            for (i = 0; i < loop_ub; i++) {
                b_loop_ub = N[0];
                for (i1 = 0; i1 < b_loop_ub; i1++) {
                    Coeff_data[i1 + 120 * i] = Coeff0_data[i1 + N[0] * i];
                }
            }

            *b_NCoeff = BasisVal_size[1];
            PROF_OUT(FeedratePlanning_v4);
        }
    }

    c_emxFreeStruct_coder_internal_(&A);

    /*  CurOptStruct = struct('Coeff', Coeff(1:N, 1)); */
    /*  u = 0; */
    /*  uvec = zeros(10000, 1); */
    /*  for ktick = 1:length(uvec) */
    /*      u = ResampleTick(CurOptStruct, Bl, u, 1e-4); */
    /*      uvec(ktick) = u; */
    /*  end */
    /*   */
    /*  plot(uvec); */
    /*  ylim([-0.1, 1.1]) */
    /*   */
    /*  uvec */
}

/*
 * File trailer for FeedratePlanning_v4.c
 *
 * [EOF]
 */
