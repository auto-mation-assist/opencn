/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CalcAlpha0.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef CALCALPHA0_H
#define CALCALPHA0_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void CalcAlpha0(const double alpha1_data[], const int alpha1_size[1],
                       const double in2[16], double alpha0_s_data[], int
                       alpha0_s_size[1]);

#endif

/*
 * File trailer for CalcAlpha0.h
 *
 * [EOF]
 */
