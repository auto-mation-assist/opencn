/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CutCurvStruct.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "CutCurvStruct.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "EvalHelix.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const CurvStruct *b_CurvStruct
 *                double d0
 *                double d1
 *                CurvStruct *CurvStruct1
 * Return Type  : void
 */
void CutCurvStruct(const CurvStruct *b_CurvStruct, double d0, double d1,
                   CurvStruct *CurvStruct1)
{
    int i;
    double scale;
    double absxk;
    char message[29];
    double L;
    double t;
    double P0P1[3];
    char b_message[27];
    static const char c_message[27] = { 'U', 'n', 'k', 'n', 'o', 'w', 'n', ' ',
        'C', 'u', 'r', 'v', 'e', ' ', 'T', 'y', 'p', 'e', ' ', 'f', 'o', 'r',
        ' ', 'C', 'u', 't', '.' };

    double CP0[3];

    /*  */
    switch (b_CurvStruct->Type) {
      case CurveType_Helix:
        /*  */
        /*  */
        /*  */
        scale = b_CurvStruct->P1[0] - b_CurvStruct->P0[0];
        P0P1[0] = scale;
        absxk = scale * b_CurvStruct->evec[0];
        scale = b_CurvStruct->P1[1] - b_CurvStruct->P0[1];
        P0P1[1] = scale;
        absxk += scale * b_CurvStruct->evec[1];
        scale = b_CurvStruct->P1[2] - b_CurvStruct->P0[2];
        absxk += scale * b_CurvStruct->evec[2];
        t = 1.0 / tan(b_CurvStruct->theta / 2.0);

        /*  */
        L = b_CurvStruct->theta * sqrt(pow(sqrt((pow(b_CurvStruct->P0[0] -
            ((b_CurvStruct->P0[0] + (b_CurvStruct->P1[0] - absxk *
            b_CurvStruct->evec[0])) + t * (b_CurvStruct->evec[1] * scale -
            b_CurvStruct->evec[2] * P0P1[1])) / 2.0, 2.0) + pow(b_CurvStruct->
            P0[1] - ((b_CurvStruct->P0[1] + (b_CurvStruct->P1[1] - absxk *
            b_CurvStruct->evec[1])) + t * (b_CurvStruct->evec[2] * P0P1[0] -
            b_CurvStruct->evec[0] * scale)) / 2.0, 2.0)) + pow(b_CurvStruct->P0
            [2] - ((b_CurvStruct->P0[2] + (b_CurvStruct->P1[2] - absxk *
            b_CurvStruct->evec[2])) + t * (b_CurvStruct->evec[0] * P0P1[1] -
            b_CurvStruct->evec[1] * P0P1[0])) / 2.0, 2.0)), 2.0) + pow
            (b_CurvStruct->pitch / 6.2831853071795862, 2.0));
        break;

      case CurveType_Line:
        scale = 3.3121686421112381E-170;
        absxk = fabs(b_CurvStruct->P1[0] - b_CurvStruct->P0[0]);
        if (absxk > 3.3121686421112381E-170) {
            L = 1.0;
            scale = absxk;
        } else {
            t = absxk / 3.3121686421112381E-170;
            L = t * t;
        }

        absxk = fabs(b_CurvStruct->P1[1] - b_CurvStruct->P0[1]);
        if (absxk > scale) {
            t = scale / absxk;
            L = L * t * t + 1.0;
            scale = absxk;
        } else {
            t = absxk / scale;
            L += t * t;
        }

        absxk = fabs(b_CurvStruct->P1[2] - b_CurvStruct->P0[2]);
        if (absxk > scale) {
            t = scale / absxk;
            L = L * t * t + 1.0;
            scale = absxk;
        } else {
            t = absxk / scale;
            L += t * t;
        }

        L = scale * sqrt(L);
        break;

      default:
        for (i = 0; i < 29; i++) {
            message[i] = cv3[i];
        }

        c_assert_(&message[0]);
        L = 0.0;
        break;
    }

    if ((d0 >= L) || (L - d1 <= 0.0)) {
        *CurvStruct1 = *b_CurvStruct;
    } else {
        switch (b_CurvStruct->Type) {
          case CurveType_Line:
            /*  line (G01) */
            scale = sqrt((pow(b_CurvStruct->P1[0] - b_CurvStruct->P0[0], 2.0) +
                          pow(b_CurvStruct->P1[1] - b_CurvStruct->P0[1], 2.0)) +
                         pow(b_CurvStruct->P1[2] - b_CurvStruct->P0[2], 2.0));
            absxk = d0 / scale;
            scale = d1 / scale;
            P0P1[0] = absxk * b_CurvStruct->P1[0] + (1.0 - absxk) *
                b_CurvStruct->P0[0];
            CP0[0] = (1.0 - scale) * b_CurvStruct->P1[0] + scale *
                b_CurvStruct->P0[0];
            P0P1[1] = absxk * b_CurvStruct->P1[1] + (1.0 - absxk) *
                b_CurvStruct->P0[1];
            CP0[1] = (1.0 - scale) * b_CurvStruct->P1[1] + scale *
                b_CurvStruct->P0[1];
            P0P1[2] = absxk * b_CurvStruct->P1[2] + (1.0 - absxk) *
                b_CurvStruct->P0[2];
            CP0[2] = (1.0 - scale) * b_CurvStruct->P1[2] + scale *
                b_CurvStruct->P0[2];
            ConstrLineStruct(P0P1, CP0, b_CurvStruct->FeedRate,
                             b_CurvStruct->ZSpdMode, CurvStruct1);
            break;

          case CurveType_Helix:
            /*  arc of circle / helix (G02, G03) */
            /*  */
            /*  */
            /*  */
            /*  */
            scale = b_CurvStruct->P1[0] - b_CurvStruct->P0[0];
            P0P1[0] = scale;
            absxk = scale * b_CurvStruct->evec[0];
            scale = b_CurvStruct->P1[1] - b_CurvStruct->P0[1];
            P0P1[1] = scale;
            absxk += scale * b_CurvStruct->evec[1];
            scale = b_CurvStruct->P1[2] - b_CurvStruct->P0[2];
            absxk += scale * b_CurvStruct->evec[2];
            t = 1.0 / tan(b_CurvStruct->theta / 2.0);

            /*  */
            scale = sqrt((pow(b_CurvStruct->P0[0] - ((b_CurvStruct->P0[0] +
                             (b_CurvStruct->P1[0] - absxk * b_CurvStruct->evec[0]))
                            + t * (b_CurvStruct->evec[1] * scale -
                                   b_CurvStruct->evec[2] * P0P1[1])) / 2.0, 2.0)
                          + pow(b_CurvStruct->P0[1] - ((b_CurvStruct->P0[1] +
                             (b_CurvStruct->P1[1] - absxk * b_CurvStruct->evec[1]))
                            + t * (b_CurvStruct->evec[2] * P0P1[0] -
                                   b_CurvStruct->evec[0] * scale)) / 2.0, 2.0))
                         + pow(b_CurvStruct->P0[2] - ((b_CurvStruct->P0[2] +
                            (b_CurvStruct->P1[2] - absxk * b_CurvStruct->evec[2]))
                           + t * (b_CurvStruct->evec[0] * P0P1[1] -
                                  b_CurvStruct->evec[1] * P0P1[0])) / 2.0, 2.0));
            L = b_CurvStruct->theta * sqrt(pow(scale, 2.0) + pow
                (b_CurvStruct->pitch / 6.2831853071795862, 2.0));

            /*  */
            /*  */
            c_EvalHelix(b_CurvStruct->P0, b_CurvStruct->P1, b_CurvStruct->evec,
                        b_CurvStruct->theta, b_CurvStruct->pitch, d0 / L, P0P1);
            c_EvalHelix(b_CurvStruct->P0, b_CurvStruct->P1, b_CurvStruct->evec,
                        b_CurvStruct->theta, b_CurvStruct->pitch, 1.0 - d1 / L,
                        CP0);

            /*  */
            /*  */
            *CurvStruct1 = *b_CurvStruct;
            CurvStruct1->P0[0] = P0P1[0];
            CurvStruct1->P1[0] = CP0[0];
            CurvStruct1->P0[1] = P0P1[1];
            CurvStruct1->P1[1] = CP0[1];
            CurvStruct1->P0[2] = P0P1[2];
            CurvStruct1->P1[2] = CP0[2];
            CurvStruct1->theta = b_CurvStruct->theta - (d0 + d1) / scale;

            /*  PITCH NOT YET RECALCULATED  !!! */
            break;

          default:
            *CurvStruct1 = *b_CurvStruct;
            CurvStruct1->Type = CurveType_None;
            for (i = 0; i < 27; i++) {
                b_message[i] = c_message[i];
            }

            c_assert_(&b_message[0]);
            break;
        }
    }
}

/*
 * File trailer for CutCurvStruct.c
 *
 * [EOF]
 */
