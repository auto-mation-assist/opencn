/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: main.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/

/* Include Files */
#include "main.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxAPI.h"
#include "FeedoptTypes_terminate.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Declarations */
static void argInit_1024x1_char_T(char result[1024]);
static void argInit_120x1_real_T(double result[120]);
static void argInit_1xd10_CurvStruct(CurvStruct result_data[], int result_size[2]);
static void argInit_1xd200_real_T(double result_data[], int result_size[2]);
static void argInit_3x1_real_T(double result[3]);
static void argInit_3x6_real_T(double result[6][3]);
static void argInit_CurvStruct(CurvStruct *result);
static CurveType argInit_CurveType(void);
static void argInit_FeedoptConfigStruct(FeedoptConfigStruct *result);
static Fopt argInit_Fopt(void);
static void argInit_OptStruct(OptStruct *result);
static PushStatus argInit_PushStatus(void);
static QueueId argInit_QueueId(void);
static ZSpdMode argInit_ZSpdMode(void);
static boolean_T argInit_boolean_T(void);
static char argInit_char_T(void);
static void argInit_d120x1_real_T(double result_data[], int result_size[1]);
static int argInit_int32_T(void);
static double argInit_real_T(void);
static struct0_T argInit_struct0_T(void);
static unsigned long argInit_uint64_T(void);
static void main_BenchmarkFeedratePlanning(void);
static void main_CalcTransition(void);
static void main_Calc_u_v4(void);
static void main_ConstrHelixStruct(void);
static void main_ConstrLineStruct(void);
static void main_EvalCurvStruct(void);
static void main_FeedoptDefaultConfig(void);
static void main_FeedoptPlan(void);
static void main_FeedoptTypes(void);
static void main_InitConfig(void);
static void main_PrintCurvStruct(void);
static void main_ResampleTick(void);
static void main_c_alloc_matrix(void);
static void main_c_linspace(void);
static void main_c_roots_(void);
static void main_sinspace(void);

/* Function Definitions */

/*
 * Arguments    : char result[1024]
 * Return Type  : void
 */
static void argInit_1024x1_char_T(char result[1024])
{
    int idx0;

    /* Loop over the array to initialize each element. */
    for (idx0 = 0; idx0 < 1024; idx0++) {
        /* Set the value of the array element.
           Change this value to the value that the application requires. */
        result[idx0] = argInit_char_T();
    }
}

/*
 * Arguments    : double result[120]
 * Return Type  : void
 */
static void argInit_120x1_real_T(double result[120])
{
    int idx0;

    /* Loop over the array to initialize each element. */
    for (idx0 = 0; idx0 < 120; idx0++) {
        /* Set the value of the array element.
           Change this value to the value that the application requires. */
        result[idx0] = argInit_real_T();
    }
}

/*
 * Arguments    : CurvStruct result_data[]
 *                int result_size[2]
 * Return Type  : void
 */
static void argInit_1xd10_CurvStruct(CurvStruct result_data[], int result_size[2])
{
    int idx1;

    /* Set the size of the array.
       Change this size to the value that the application requires. */
    result_size[0] = 1;
    result_size[1] = 2;

    /* Loop over the array to initialize each element. */
    for (idx1 = 0; idx1 < 2; idx1++) {
        /* Set the value of the array element.
           Change this value to the value that the application requires. */
        argInit_CurvStruct(&result_data[idx1]);
    }
}

/*
 * Arguments    : double result_data[]
 *                int result_size[2]
 * Return Type  : void
 */
static void argInit_1xd200_real_T(double result_data[], int result_size[2])
{
    int idx1;

    /* Set the size of the array.
       Change this size to the value that the application requires. */
    result_size[0] = 1;
    result_size[1] = 2;

    /* Loop over the array to initialize each element. */
    for (idx1 = 0; idx1 < 2; idx1++) {
        /* Set the value of the array element.
           Change this value to the value that the application requires. */
        result_data[idx1] = argInit_real_T();
    }
}

/*
 * Arguments    : double result[3]
 * Return Type  : void
 */
static void argInit_3x1_real_T(double result[3])
{
    double result_tmp_tmp;

    /* Loop over the array to initialize each element. */
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result_tmp_tmp = argInit_real_T();
    result[0] = result_tmp_tmp;

    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[1] = result_tmp_tmp;

    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[2] = result_tmp_tmp;
}

/*
 * Arguments    : double result[6][3]
 * Return Type  : void
 */
static void argInit_3x6_real_T(double result[6][3])
{
    int idx0;
    int idx1;

    /* Loop over the array to initialize each element. */
    for (idx0 = 0; idx0 < 3; idx0++) {
        for (idx1 = 0; idx1 < 6; idx1++) {
            /* Set the value of the array element.
               Change this value to the value that the application requires. */
            result[idx1][idx0] = argInit_real_T();
        }
    }
}

/*
 * Arguments    : CurvStruct *result
 * Return Type  : void
 */
static void argInit_CurvStruct(CurvStruct *result)
{
    double result_tmp_tmp[3];
    double result_tmp_tmp_tmp_tmp;

    /* Set the value of each structure field.
       Change this value to the value that the application requires. */
    result->Type = argInit_CurveType();
    result->ZSpdMode = argInit_ZSpdMode();
    argInit_3x1_real_T(result_tmp_tmp);
    result->P0[0] = result_tmp_tmp[0];
    result->P1[0] = result_tmp_tmp[0];
    result->evec[0] = result_tmp_tmp[0];
    result->P0[1] = result_tmp_tmp[1];
    result->P1[1] = result_tmp_tmp[1];
    result->evec[1] = result_tmp_tmp[1];
    result->P0[2] = result_tmp_tmp[2];
    result->P1[2] = result_tmp_tmp[2];
    result->evec[2] = result_tmp_tmp[2];
    result_tmp_tmp_tmp_tmp = argInit_real_T();
    result->theta = result_tmp_tmp_tmp_tmp;
    result->pitch = result_tmp_tmp_tmp_tmp;
    argInit_3x6_real_T(result->CoeffP5);
    result->FeedRate = argInit_real_T();
}

/*
 * Arguments    : void
 * Return Type  : CurveType
 */
static CurveType argInit_CurveType(void)
{
    return CurveType_None;
}

/*
 * Arguments    : FeedoptConfigStruct *result
 * Return Type  : void
 */
static void argInit_FeedoptConfigStruct(FeedoptConfigStruct *result)
{
    int result_tmp_tmp_tmp_tmp;
    double result_tmp[3];

    /* Set the value of each structure field.
       Change this value to the value that the application requires. */
    result_tmp_tmp_tmp_tmp = argInit_int32_T();
    result->NDiscr = result_tmp_tmp_tmp_tmp;
    result->NBreak = result_tmp_tmp_tmp_tmp;
    result->NHorz = result_tmp_tmp_tmp_tmp;
    result->MaxNHorz = result_tmp_tmp_tmp_tmp;
    result->MaxNDiscr = result_tmp_tmp_tmp_tmp;
    result->MaxNCoeff = argInit_int32_T();
    result->vmax = argInit_real_T();
    argInit_3x1_real_T(result_tmp);
    result->amax[0] = result_tmp[0];
    result->jmax[0] = result_tmp[0];
    result->amax[1] = result_tmp[1];
    result->jmax[1] = result_tmp[1];
    result->amax[2] = result_tmp[2];
    result->jmax[2] = result_tmp[2];
    result->SplineDegree = argInit_int32_T();
    result->CutOff = argInit_real_T();
    result->LSplit = argInit_real_T();
    result->v_0 = argInit_real_T();
    result->at_0 = argInit_real_T();
    result->v_1 = argInit_real_T();
    result->at_1 = argInit_real_T();
    argInit_1024x1_char_T(result->source);
    result->DebugPrint = argInit_boolean_T();
}

/*
 * Arguments    : void
 * Return Type  : Fopt
 */
static Fopt argInit_Fopt(void)
{
    return Fopt_Init;
}

/*
 * Arguments    : OptStruct *result
 * Return Type  : void
 */
static void argInit_OptStruct(OptStruct *result)
{
    /* Set the value of each structure field.
       Change this value to the value that the application requires. */
    argInit_120x1_real_T(result->Coeff);
    argInit_CurvStruct(&result->CurvStruct);
}

/*
 * Arguments    : void
 * Return Type  : PushStatus
 */
static PushStatus argInit_PushStatus(void)
{
    return PushStatus_Success;
}

/*
 * Arguments    : void
 * Return Type  : QueueId
 */
static QueueId argInit_QueueId(void)
{
    return QueueId_GCode;
}

/*
 * Arguments    : void
 * Return Type  : ZSpdMode
 */
static ZSpdMode argInit_ZSpdMode(void)
{
    return ZSpdMode_NN;
}

/*
 * Arguments    : void
 * Return Type  : boolean_T
 */
static boolean_T argInit_boolean_T(void)
{
    return false;
}

/*
 * Arguments    : void
 * Return Type  : char
 */
static char argInit_char_T(void)
{
    return '?';
}

/*
 * Arguments    : double result_data[]
 *                int result_size[1]
 * Return Type  : void
 */
static void argInit_d120x1_real_T(double result_data[], int result_size[1])
{
    int idx0;

    /* Set the size of the array.
       Change this size to the value that the application requires. */
    result_size[0] = 2;

    /* Loop over the array to initialize each element. */
    for (idx0 = 0; idx0 < 2; idx0++) {
        /* Set the value of the array element.
           Change this value to the value that the application requires. */
        result_data[idx0] = argInit_real_T();
    }
}

/*
 * Arguments    : void
 * Return Type  : int
 */
static int argInit_int32_T(void)
{
    return 0;
}

/*
 * Arguments    : void
 * Return Type  : double
 */
static double argInit_real_T(void)
{
    return 0.0;
}

/*
 * Arguments    : void
 * Return Type  : struct0_T
 */
static struct0_T argInit_struct0_T(void)
{
    struct0_T result;

    /* Set the value of each structure field.
       Change this value to the value that the application requires. */
    result.handle = argInit_uint64_T();
    result.n = argInit_int32_T();
    return result;
}

/*
 * Arguments    : void
 * Return Type  : unsigned long
 */
static unsigned long argInit_uint64_T(void)
{
    return 0UL;
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_BenchmarkFeedratePlanning(void)
{
    CurvStruct CurvStructs_data[10];
    int CurvStructs_size[2];
    double Coeff_data[1200];
    int Coeff_size[2];
    double TElapsed;

    /* Initialize function 'BenchmarkFeedratePlanning' input arguments. */
    /* Initialize function input argument 'CurvStructs'. */
    argInit_1xd10_CurvStruct(CurvStructs_data, CurvStructs_size);

    /* Call the entry-point 'BenchmarkFeedratePlanning'. */
    BenchmarkFeedratePlanning(CurvStructs_data, CurvStructs_size,
        argInit_int32_T(), Coeff_data, Coeff_size, &TElapsed);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_CalcTransition(void)
{
    CurvStruct CurvStruct1_tmp;
    CurvStruct CurvStruct1_C;
    CurvStruct CurvStruct_T;
    CurvStruct CurvStruct2_C;

    /* Initialize function 'CalcTransition' input arguments. */
    /* Initialize function input argument 'CurvStruct1'. */
    argInit_CurvStruct(&CurvStruct1_tmp);

    /* Initialize function input argument 'CurvStruct2'. */
    /* Call the entry-point 'CalcTransition'. */
    CalcTransition(&CurvStruct1_tmp, &CurvStruct1_tmp, argInit_real_T(),
                   &CurvStruct1_C, &CurvStruct_T, &CurvStruct2_C);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_Calc_u_v4(void)
{
    struct0_T b_Bl;
    double Coeff_data[120];
    int Coeff_size[1];
    double u[10000];
    int N;

    /* Initialize function 'Calc_u_v4' input arguments. */
    /* Initialize function input argument 'Bl'. */
    b_Bl = argInit_struct0_T();

    /* Initialize function input argument 'Coeff'. */
    argInit_d120x1_real_T(Coeff_data, Coeff_size);

    /* Call the entry-point 'Calc_u_v4'. */
    Calc_u_v4(&b_Bl, Coeff_data, Coeff_size, argInit_real_T(), u, &N);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_ConstrHelixStruct(void)
{
    double P0_tmp_tmp[3];
    double theta_tmp_tmp;
    CurvStruct b_CurvStruct;

    /* Initialize function 'ConstrHelixStruct' input arguments. */
    /* Initialize function input argument 'P0'. */
    argInit_3x1_real_T(P0_tmp_tmp);

    /* Initialize function input argument 'P1'. */
    /* Initialize function input argument 'evec'. */
    theta_tmp_tmp = argInit_real_T();

    /* Call the entry-point 'ConstrHelixStruct'. */
    ConstrHelixStruct(P0_tmp_tmp, P0_tmp_tmp, P0_tmp_tmp, theta_tmp_tmp,
                      theta_tmp_tmp, theta_tmp_tmp, argInit_ZSpdMode(),
                      &b_CurvStruct);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_ConstrLineStruct(void)
{
    double P0_tmp[3];
    CurvStruct b_CurvStruct;

    /* Initialize function 'ConstrLineStruct' input arguments. */
    /* Initialize function input argument 'P0'. */
    argInit_3x1_real_T(P0_tmp);

    /* Initialize function input argument 'P1'. */
    /* Call the entry-point 'ConstrLineStruct'. */
    ConstrLineStruct(P0_tmp, P0_tmp, argInit_real_T(), argInit_ZSpdMode(),
                     &b_CurvStruct);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_EvalCurvStruct(void)
{
    CurvStruct b_CurvStruct;
    double u_vec_data[200];
    int u_vec_size[2];
    double r0D_data[600];
    int r0D_size[2];
    double r1D_data[600];
    int r1D_size[2];
    double r2D_data[600];
    int r2D_size[2];
    double r3D_data[600];
    int r3D_size[2];

    /* Initialize function 'EvalCurvStruct' input arguments. */
    /* Initialize function input argument 'CurvStruct'. */
    argInit_CurvStruct(&b_CurvStruct);

    /* Initialize function input argument 'u_vec'. */
    argInit_1xd200_real_T(u_vec_data, u_vec_size);

    /* Call the entry-point 'EvalCurvStruct'. */
    EvalCurvStruct(&b_CurvStruct, u_vec_data, u_vec_size, r0D_data, r0D_size,
                   r1D_data, r1D_size, r2D_data, r2D_size, r3D_data, r3D_size);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_FeedoptDefaultConfig(void)
{
    FeedoptConfigStruct cfg;

    /* Call the entry-point 'FeedoptDefaultConfig'. */
    FeedoptDefaultConfig(&cfg);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_FeedoptPlan(void)
{
    Fopt op;

    /* Initialize function 'FeedoptPlan' input arguments. */
    /* Call the entry-point 'FeedoptPlan'. */
    op = argInit_Fopt();
    FeedoptPlan(&op);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_FeedoptTypes(void)
{
    CurvStruct r;
    FeedoptConfigStruct r1;

    /* Initialize function 'FeedoptTypes' input arguments. */
    /* Initialize function input argument 'CurvStruct'. */
    /* Initialize function input argument 'FeedoptConfig'. */
    /* Call the entry-point 'FeedoptTypes'. */
    argInit_CurvStruct(&r);
    argInit_FeedoptConfigStruct(&r1);
    FeedoptTypes(&r, &r1, argInit_QueueId(), argInit_PushStatus());
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_InitConfig(void)
{
    /* Call the entry-point 'InitConfig'. */
    InitConfig();
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_PrintCurvStruct(void)
{
    CurvStruct r;

    /* Initialize function 'PrintCurvStruct' input arguments. */
    /* Initialize function input argument 'S'. */
    /* Call the entry-point 'PrintCurvStruct'. */
    argInit_CurvStruct(&r);
    PrintCurvStruct(&r);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_ResampleTick(void)
{
    OptStruct CurOptStruct_tmp;
    struct0_T b_Bl;
    double u_tmp;
    double u;
    boolean_T pop_next;
    boolean_T finished;

    /* Initialize function 'ResampleTick' input arguments. */
    /* Initialize function input argument 'CurOptStruct'. */
    argInit_OptStruct(&CurOptStruct_tmp);

    /* Initialize function input argument 'NextOptStruct'. */
    /* Initialize function input argument 'Bl'. */
    b_Bl = argInit_struct0_T();
    u_tmp = argInit_real_T();

    /* Call the entry-point 'ResampleTick'. */
    u = u_tmp;
    ResampleTick(&CurOptStruct_tmp, &CurOptStruct_tmp, &b_Bl, &u, u_tmp,
                 &pop_next, &finished);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_c_alloc_matrix(void)
{
    emxArray_real_T *A;
    double M_tmp;
    emxInitArray_real_T(&A, 2);

    /* Initialize function 'c_alloc_matrix' input arguments. */
    M_tmp = argInit_real_T();

    /* Call the entry-point 'c_alloc_matrix'. */
    c_alloc_matrix(M_tmp, M_tmp, A);
    emxDestroyArray_real_T(A);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_c_linspace(void)
{
    double x0_tmp;
    double A_data[200];
    int A_size[2];

    /* Initialize function 'c_linspace' input arguments. */
    x0_tmp = argInit_real_T();

    /* Call the entry-point 'c_linspace'. */
    c_linspace(x0_tmp, x0_tmp, argInit_int32_T(), A_data, A_size);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_c_roots_(void)
{
    double coeffs_data[12];
    int coeffs_size[2];
    creal_T Y_data[11];
    int Y_size[1];

    /* Initialize function 'c_roots_' input arguments. */
    /* Initialize function input argument 'coeffs'. */
    argInit_1xd200_real_T(coeffs_data, coeffs_size);

    /* Call the entry-point 'c_roots_'. */
    c_roots_(coeffs_data, coeffs_size, Y_data, Y_size);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_sinspace(void)
{
    double x0_tmp;
    double x_data[200];
    int x_size[2];

    /* Initialize function 'sinspace' input arguments. */
    x0_tmp = argInit_real_T();

    /* Call the entry-point 'sinspace'. */
    sinspace(x0_tmp, x0_tmp, argInit_int32_T(), x_data, x_size);
}

/*
 * Arguments    : int argc
 *                const char * const argv[]
 * Return Type  : int
 */
int main(int argc, const char * const argv[])
{
    (void)argc;
    (void)argv;

    /* The initialize function is being called automatically from your entry-point function. So, a call to initialize is not included here. */
    /* Invoke the entry-point functions.
       You can call entry-point functions multiple times. */
    main_FeedoptTypes();
    main_c_alloc_matrix();
    main_c_linspace();
    main_sinspace();
    main_ConstrLineStruct();
    main_ConstrHelixStruct();
    main_EvalCurvStruct();
    main_Calc_u_v4();
    main_c_roots_();
    main_FeedoptPlan();
    main_PrintCurvStruct();
    main_InitConfig();
    main_CalcTransition();
    main_FeedoptDefaultConfig();
    main_BenchmarkFeedratePlanning();
    main_ResampleTick();

    /* Terminate the application.
       You do not need to do this more than one time. */
    FeedoptTypes_terminate();
    return 0;
}

/*
 * File trailer for main.c
 *
 * [EOF]
 */
