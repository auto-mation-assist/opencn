/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: SplitCurvStructs.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "SplitCurvStructs.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "EvalHelix.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <math.h>
#include <stdio.h>

/* Function Definitions */

/*
 * NOT YET READY FOR CODE GENERATION  !
 * Arguments    : int QueueIn_id
 *                int QueueOut_id
 *                double L_split
 * Return Type  : void
 */
void SplitCurvStructs(int QueueIn_id, int QueueOut_id, double L_split)
{
    int N;
    int k;
    CurvStruct value;
    int i;
    double scale;
    double P0P1_idx_0;
    char message[29];
    double L;
    double t;
    double P0P1_idx_1;
    int b_k;
    CurvStruct TmpCrvStrct;
    N = queue_size(QueueIn_id);

    /*  N = length(CurvStructs); */
    if (N != 0) {
        printf("Queue.Clear id = %d\n", QueueOut_id);
        fflush(stdout);
        queue_clear(QueueOut_id);

        /*  CrvStrctSplit = []; */
        /*  */
        for (k = 0; k < N; k++) {
            /*  Forward Get */
            value = queue_get(QueueIn_id, k);

            /*  */
            /*  */
            switch (value.Type) {
              case CurveType_Line:
                /*  line (G01) */
                switch (value.Type) {
                  case CurveType_Helix:
                    /*  */
                    /*  */
                    /*  */
                    scale = value.P1[0] - value.P0[0];
                    P0P1_idx_0 = scale;
                    t = scale * value.evec[0];
                    scale = value.P1[1] - value.P0[1];
                    P0P1_idx_1 = scale;
                    t += scale * value.evec[1];
                    scale = value.P1[2] - value.P0[2];
                    t += scale * value.evec[2];
                    L = 1.0 / tan(value.theta / 2.0);

                    /*  */
                    L = value.theta * sqrt(pow(sqrt((pow(value.P0[0] -
                        ((value.P0[0] + (value.P1[0] - t * value.evec[0])) + L *
                         (value.evec[1] * scale - value.evec[2] * P0P1_idx_1)) /
                        2.0, 2.0) + pow(value.P0[1] - ((value.P0[1] + (value.P1
                        [1] - t * value.evec[1])) + L * (value.evec[2] *
                        P0P1_idx_0 - value.evec[0] * scale)) / 2.0, 2.0)) + pow
                        (value.P0[2] - ((value.P0[2] + (value.P1[2] - t *
                        value.evec[2])) + L * (value.evec[0] * P0P1_idx_1 -
                        value.evec[1] * P0P1_idx_0)) / 2.0, 2.0)), 2.0) + pow
                                           (value.pitch / 6.2831853071795862,
                                            2.0));
                    break;

                  case CurveType_Line:
                    scale = 3.3121686421112381E-170;
                    P0P1_idx_0 = fabs(value.P1[0] - value.P0[0]);
                    if (P0P1_idx_0 > 3.3121686421112381E-170) {
                        L = 1.0;
                        scale = P0P1_idx_0;
                    } else {
                        t = P0P1_idx_0 / 3.3121686421112381E-170;
                        L = t * t;
                    }

                    P0P1_idx_0 = fabs(value.P1[1] - value.P0[1]);
                    if (P0P1_idx_0 > scale) {
                        t = scale / P0P1_idx_0;
                        L = L * t * t + 1.0;
                        scale = P0P1_idx_0;
                    } else {
                        t = P0P1_idx_0 / scale;
                        L += t * t;
                    }

                    P0P1_idx_0 = fabs(value.P1[2] - value.P0[2]);
                    if (P0P1_idx_0 > scale) {
                        t = scale / P0P1_idx_0;
                        L = L * t * t + 1.0;
                        scale = P0P1_idx_0;
                    } else {
                        t = P0P1_idx_0 / scale;
                        L += t * t;
                    }

                    L = scale * sqrt(L);
                    break;

                  default:
                    for (i = 0; i < 29; i++) {
                        message[i] = cv3[i];
                    }

                    c_assert_(&message[0]);
                    L = 0.0;
                    break;
                }

                if (L > L_split * 2.0) {
                    L = sqrt((pow(value.P1[0] - value.P0[0], 2.0) + pow
                              (value.P1[1] - value.P0[1], 2.0)) + pow(value.P1[2]
                              - value.P0[2], 2.0));

                    /*  length of original line segment */
                    t = ceil(L / L_split);
                    P0P1_idx_1 = L / t;

                    /*  corrected L_split */
                    /* CrvStrctSplit = repmat(CrvStrct, 1, N); % preallocation (MALLOC TO BE RECHECKED !) */
                    P0P1_idx_0 = 0.0;

                    /*  */
                    i = (int)t;
                    for (b_k = 0; b_k < i; b_k++) {
                        /* CrvStrctSplit(k).P0 = (1-alpha_old)*P0 + alpha_old*P1; */
                        /*  */
                        /*      alpha               = k*L_split/L; */
                        /*      CrvStrctSplit(k).P1 = (1-alpha)*P0 + alpha*P1; */
                        /*      alpha_old           = alpha; */
                        TmpCrvStrct = value;
                        scale = ((double)b_k + 1.0) * P0P1_idx_1 / L;
                        TmpCrvStrct.P0[0] = (1.0 - P0P1_idx_0) * value.P0[0] +
                            P0P1_idx_0 * value.P1[0];
                        TmpCrvStrct.P1[0] = (1.0 - scale) * value.P0[0] + scale *
                            value.P1[0];
                        TmpCrvStrct.P0[1] = (1.0 - P0P1_idx_0) * value.P0[1] +
                            P0P1_idx_0 * value.P1[1];
                        TmpCrvStrct.P1[1] = (1.0 - scale) * value.P0[1] + scale *
                            value.P1[1];
                        TmpCrvStrct.P0[2] = (1.0 - P0P1_idx_0) * value.P0[2] +
                            P0P1_idx_0 * value.P1[2];
                        TmpCrvStrct.P1[2] = (1.0 - scale) * value.P0[2] + scale *
                            value.P1[2];
                        P0P1_idx_0 = scale;
                        queue_push(QueueOut_id, TmpCrvStrct);
                    }
                } else {
                    queue_push(QueueOut_id, value);
                }
                break;

              case CurveType_Helix:
                /*  arc of circle / helix (G02, G03) */
                switch (value.Type) {
                  case CurveType_Helix:
                    /*  */
                    /*  */
                    /*  */
                    scale = value.P1[0] - value.P0[0];
                    P0P1_idx_0 = scale;
                    t = scale * value.evec[0];
                    scale = value.P1[1] - value.P0[1];
                    P0P1_idx_1 = scale;
                    t += scale * value.evec[1];
                    scale = value.P1[2] - value.P0[2];
                    t += scale * value.evec[2];
                    L = 1.0 / tan(value.theta / 2.0);

                    /*  */
                    L = value.theta * sqrt(pow(sqrt((pow(value.P0[0] -
                        ((value.P0[0] + (value.P1[0] - t * value.evec[0])) + L *
                         (value.evec[1] * scale - value.evec[2] * P0P1_idx_1)) /
                        2.0, 2.0) + pow(value.P0[1] - ((value.P0[1] + (value.P1
                        [1] - t * value.evec[1])) + L * (value.evec[2] *
                        P0P1_idx_0 - value.evec[0] * scale)) / 2.0, 2.0)) + pow
                        (value.P0[2] - ((value.P0[2] + (value.P1[2] - t *
                        value.evec[2])) + L * (value.evec[0] * P0P1_idx_1 -
                        value.evec[1] * P0P1_idx_0)) / 2.0, 2.0)), 2.0) + pow
                                           (value.pitch / 6.2831853071795862,
                                            2.0));
                    break;

                  case CurveType_Line:
                    scale = 3.3121686421112381E-170;
                    P0P1_idx_0 = fabs(value.P1[0] - value.P0[0]);
                    if (P0P1_idx_0 > 3.3121686421112381E-170) {
                        L = 1.0;
                        scale = P0P1_idx_0;
                    } else {
                        t = P0P1_idx_0 / 3.3121686421112381E-170;
                        L = t * t;
                    }

                    P0P1_idx_0 = fabs(value.P1[1] - value.P0[1]);
                    if (P0P1_idx_0 > scale) {
                        t = scale / P0P1_idx_0;
                        L = L * t * t + 1.0;
                        scale = P0P1_idx_0;
                    } else {
                        t = P0P1_idx_0 / scale;
                        L += t * t;
                    }

                    P0P1_idx_0 = fabs(value.P1[2] - value.P0[2]);
                    if (P0P1_idx_0 > scale) {
                        t = scale / P0P1_idx_0;
                        L = L * t * t + 1.0;
                        scale = P0P1_idx_0;
                    } else {
                        t = P0P1_idx_0 / scale;
                        L += t * t;
                    }

                    L = scale * sqrt(L);
                    break;

                  default:
                    for (i = 0; i < 29; i++) {
                        message[i] = cv3[i];
                    }

                    c_assert_(&message[0]);
                    L = 0.0;
                    break;
                }

                if (L > L_split * 2.0) {
                    /*  */
                    /*  */
                    /*  */
                    /*  */
                    /*  */
                    scale = value.P1[0] - value.P0[0];
                    P0P1_idx_0 = scale;
                    t = scale * value.evec[0];
                    scale = value.P1[1] - value.P0[1];
                    P0P1_idx_1 = scale;
                    t += scale * value.evec[1];
                    scale = value.P1[2] - value.P0[2];
                    t += scale * value.evec[2];
                    L = 1.0 / tan(value.theta / 2.0);

                    /*  */
                    L = value.theta * sqrt(pow(sqrt((pow(value.P0[0] -
                        ((value.P0[0] + (value.P1[0] - t * value.evec[0])) + L *
                         (value.evec[1] * scale - value.evec[2] * P0P1_idx_1)) /
                        2.0, 2.0) + pow(value.P0[1] - ((value.P0[1] + (value.P1
                        [1] - t * value.evec[1])) + L * (value.evec[2] *
                        P0P1_idx_0 - value.evec[0] * scale)) / 2.0, 2.0)) + pow
                        (value.P0[2] - ((value.P0[2] + (value.P1[2] - t *
                        value.evec[2])) + L * (value.evec[0] * P0P1_idx_1 -
                        value.evec[1] * P0P1_idx_0)) / 2.0, 2.0)), 2.0) + pow
                                           (value.pitch / 6.2831853071795862,
                                            2.0));

                    /*  */
                    t = ceil(L / L_split);
                    P0P1_idx_1 = L / t;

                    /*  corrected L_split */
                    /*  CrvStrctSplit = repmat(CrvStrct, 1, N); % preallocation (MALLOC TO BE RECHECKED !) */
                    /*  */
                    scale = 0.0;

                    /*  */
                    i = (int)t;
                    for (b_k = 0; b_k < i; b_k++) {
                        TmpCrvStrct = value;

                        /*      CrvStrctSplit(k).theta = theta/N; */
                        /*      CrvStrctSplit(k).pitch = pitch/N; */
                        /*      % */
                        /*      P0prim = EvalHelix(CrvStrct, u_old); */
                        /*      CrvStrctSplit(k).P0 = P0prim;     */
                        /*      % */
                        /*      u     = k*L_split/L; */
                        /*      P1prim = EvalHelix(CrvStrct, u); */
                        /*      CrvStrctSplit(k).P1 = P1prim; */
                        /*      % */
                        /*      u_old = u; */
                        TmpCrvStrct.theta = value.theta / t;
                        TmpCrvStrct.pitch = value.pitch / t;

                        /*  */
                        c_EvalHelix(value.P0, value.P1, value.evec, value.theta,
                                    value.pitch, scale, TmpCrvStrct.P0);

                        /*  */
                        scale = ((double)b_k + 1.0) * P0P1_idx_1 / L;
                        c_EvalHelix(value.P0, value.P1, value.evec, value.theta,
                                    value.pitch, scale, TmpCrvStrct.P1);

                        /*  */
                        queue_push(QueueOut_id, TmpCrvStrct);
                    }
                } else {
                    queue_push(QueueOut_id, value);
                }
                break;

              default:
                queue_push(QueueOut_id, value);

                /*  CrvStrctSplit = CrvStrct; */
                break;
            }
        }
    }
}

/*
 * File trailer for SplitCurvStructs.c
 *
 * [EOF]
 */
