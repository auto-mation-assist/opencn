/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_simplex.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef C_SIMPLEX_H
#define C_SIMPLEX_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void c_simplex(const double f_data[], const int f_size[2], const
                      coder_internal_sparse A, const double b_data[], const int
                      b_size[1], const double Aeq_data[], const int Aeq_size[2],
                      const double beq_data[], const int beq_size[1], double
                      C_data[], int C_size[1], int *success);

#endif

/*
 * File trailer for c_simplex.h
 *
 * [EOF]
 */
