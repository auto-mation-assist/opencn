/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalCostIntegral.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef EVALCOSTINTEGRAL_H
#define EVALCOSTINTEGRAL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern double EvalCostIntegral(double alpha0, double beta0, double alpha1,
    double beta1, const double in5[3], const double in6[3], const double in7[3],
    double kappa0, const double in9[3], const double in10[3], const double in11
    [3], double kappa1);

#endif

/*
 * File trailer for EvalCostIntegral.h
 *
 * [EOF]
 */
