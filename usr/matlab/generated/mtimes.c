/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: mtimes.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "mtimes.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : const double A[3]
 *                const emxArray_real_T *B
 *                emxArray_real_T *C
 * Return Type  : void
 */
void mtimes(const double A[3], const emxArray_real_T *B, emxArray_real_T *C)
{
    int n;
    int j;
    n = B->size[1];
    j = C->size[0] * C->size[1];
    C->size[0] = 1;
    C->size[1] = B->size[1];
    emxEnsureCapacity_real_T(C, j);
    for (j = 0; j < n; j++) {
        C->data[j] = (B->data[3 * j] * A[0] + B->data[3 * j + 1] * A[1]) +
            B->data[3 * j + 2] * A[2];
    }
}

/*
 * File trailer for mtimes.c
 *
 * [EOF]
 */
