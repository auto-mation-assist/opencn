function Bl = bspline_create(x0, x1, deg, nbreak) %#codegen
    bspline_n = nbreak + deg - 2;
    
    h = uint64(0);
    if coder.target('rtw') || coder.target('mex')
        coder.updateBuildInfo('addSourceFiles','c_spline.c');
        coder.updateBuildInfo('addLinkFlags', '-lgsl');
        coder.cinclude('c_spline.h');
        coder.ceval('c_bspline_create', coder.wref(h), x0, x1, int32(deg), int32(nbreak));
        Bl.n = bspline_n;
        Bl.handle = h;
    else
        Bl = bspline_create_mex(x0,x1, int32(deg), int32(nbreak));
    end
end