function  TestAll
global g_FeedoptConfig
g_FeedoptConfig = FeedoptDefaultConfig;
% TestLineLineStep
% TestLineHelixOrtho
TestLineLineColinear
end

function TestLineLineStep

TestStepZ();

    function TestStepZ
        C0 = ConstrLineStruct([0,0,1]', [1,0,1]', 1, ZSpdMode.NN);
        C1 = ConstrLineStruct([1.5,0,0]', [2,0,0]', 1, ZSpdMode.NN);
        
        [C0c, Ct, C1c] = CalcTransition(C0, C1, 0.01);
    end
end

function TestLineHelixOrtho
C0 = ConstrHelixStruct([7.2213    2.2497   -0.6000]',...
    [7.2370    2.1780   -0.6000]', ...
    [0,0,1]', 0.8636, 0, 1, ZSpdMode.NN);
C1 = ConstrLineStruct([7.2370    2.1780   -0.6000]', ...
    [7.2370    2.1780   -0.8000]', 1, ZSpdMode.NN);

[C0c, Ct, C1c] = CalcTransition(C0, C1, 0.05);
end

function TestLineLineColinear
global g_FeedoptConfig
u_vec  = linspace(0, 1, g_FeedoptConfig.NDiscr);
Bl = c_bspline_create(0, 1, g_FeedoptConfig.SplineDegree, g_FeedoptConfig.NBreak);
[BasisVal, BasisValD, BasisValDD, BasisIntegr] = bspline_base_eval(Bl, u_vec);
C0 = ConstrLineStruct([0,0,0]', [1,0,0]', 35, ZSpdMode.NN);
C1 = ConstrLineStruct([1,0,0]', [2,0,0]', 1, ZSpdMode.NN);
v_0 = g_FeedoptConfig.v_0;
at_0 = g_FeedoptConfig.at_0;
v_1 = g_FeedoptConfig.v_1;
at_1 = g_FeedoptConfig.at_1;
[Coeff, NCoeff, v_0, at_0, success] = FeedratePlanning_v4([C0,C1], g_FeedoptConfig.amax, g_FeedoptConfig.jmax,...
                    v_0, at_0, v_1, at_1, BasisVal, BasisValD, BasisValDD, BasisIntegr,...
                    Bl, u_vec, 2);
end

function DoTest(tabs, f)

end