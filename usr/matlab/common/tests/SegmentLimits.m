function SegmentLimits

F = linspace(0, 16500,100);
L = 1e-6;

dt = 1e-4;

Lmin = F/60*dt;

figure
semilogx(F, Lmin*1000)
xlabel('Feedrate [mm/min]')
ylabel('Segment Length [um]')

end