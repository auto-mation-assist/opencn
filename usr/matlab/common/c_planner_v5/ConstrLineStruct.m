function CurvStruct = ConstrLineStruct(P0, P1, FeedRate, ZSpdMode)
CoeffP5     = zeros(3, 6);
evec        = zeros(3, 1);
theta       = 0;
pitch       = 0;
CurvStruct  = ConstrCurvStruct(CurveType.Line, ZSpdMode, P0, P1, evec, theta, pitch, CoeffP5, FeedRate);