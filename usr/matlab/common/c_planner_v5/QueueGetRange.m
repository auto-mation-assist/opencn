function values = QueueGetRange(id, kbeg, kend)
global g_QueueData
values = g_QueueData{id}(kbeg:kend);
end