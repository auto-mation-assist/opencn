classdef Fopt < int32
    enumeration
        Init(0)
        GCode(1)
        Smooth(2)
        Split(3)
        Opt(4)
        Finished(5)
    end
    
    methods(Static)
        function value = addClassNameToEnumNames()
            value = true;
        end
    end
end