#ifndef MATLAB_HEADERS_H
#define MATLAB_HEADERS_H

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(MATLAB_MEX_FILE)


#ifndef USE_FULL_MATLAB_GEN
#include <opencn/matlab/generated/FeedoptTypes_types.h>
#include <opencn/matlab/generated/EvalCurvStruct.h>
#include <opencn/matlab/generated/FeedoptDefaultConfig.h>
#include <opencn/matlab/generated/PrintCurvStruct.h>
#include <opencn/matlab/generated/ResampleTick.h>
#else // USE_FULL_MATLAB_GEN
#include <matlab/generated/FeedoptTypes_types.h>
#include <matlab/generated/FeedoptTypes_data.h>
#include <matlab/generated/ConstrHelixStruct.h>
#include <matlab/generated/CalcTransition.h>
#include <matlab/generated/EvalCurvStruct.h>
#include <matlab/generated/FeedoptDefaultConfig.h>
#include <matlab/generated/PrintCurvStruct.h>
#include <matlab/generated/ResampleTick.h>
#include <matlab/generated/FeedoptPlan.h>
#include <matlab/generated/InitConfig.h>
#include <matlab/generated/BenchmarkFeedratePlanning.h>
#include <matlab/generated/ConstrHelixStruct.h>
#include <matlab/generated/ConstrLineStruct.h>
//#include <matlab/generated/PushStatus.h>
#endif // USE_FULL_MATLAB_GEN


#include <opencn/uapi/feedopt.h>

#else // MATLAB_MEX_FILE
	#include "rtwtypes.h"
#endif // MATLAB_MEX_FILE

#ifdef __cplusplus
} // extern C
#endif


#endif // MATLAB_HEADERS_H
