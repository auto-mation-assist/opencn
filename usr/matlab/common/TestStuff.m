close all; clear; clc;

u = linspace(0,1,100);
R = 2;
% x^2 + y^2 = R^2
% y^2 = R^2 - x^2
% y = sqrt(R^2 - x^2)
plot(u, sqrt(R^2-u.^2))
axis square
