
/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <linux/irqreturn.h>

#include <soo/avz.h>
#include <soo/evtchn.h>

#include <soo/dev/vdummy.h>

#include <opencn/backend/vdummy.h>

static vdummy_back_ring_t ring;
static int ring_irq;

static struct {
	vdummy_sring_t *sring;
	uint32_t evtchn;
	uint32_t domid;
} args_init_gate;

irqreturn_t vdummy_interrupt(int irq, void *dev_id) {
	RING_IDX i, rp;
	vdummy_request_t *ring_req;
	vdummy_response_t *ring_rsp;

	rp = ring.sring->req_prod;
	mb();

	for (i = ring.sring->req_cons; i != rp; i++) {

		ring_req = RING_GET_REQUEST(&ring, i);

		ring_rsp = RING_GET_RESPONSE(&ring, ring.rsp_prod_pvt);

		DBG("%s, cons=%d, prod=%d\n", __func__, i, ring.rsp_prod_pvt);

		lprintk("## now received the request: %s on CPU %d\n", ring_req->buffer, smp_processor_id());

		memcpy(ring_rsp->buffer, ring_req->buffer, VDUMMY_PACKET_SIZE);

		mb();
		ring.rsp_prod_pvt++;

		RING_PUSH_RESPONSES(&ring);

		notify_remote_via_irq(ring_irq);
	}

	ring.sring->req_cons = i;

	return IRQ_HANDLED;
}

void vdummyback_setup_sring(domid_t domid, vdummy_sring_t *sring, uint32_t evtchn) {

	SHARED_RING_INIT(sring);
	BACK_RING_INIT(&ring, sring, VDUMMY_RING_SIZE);

	ring_irq = bind_interdomain_evtchn_to_irqhandler(domid, evtchn, vdummy_interrupt, NULL, 0, VDUMMY_NAME "-backend", NULL);
	BUG_ON(ring_irq < 0);

}

/*
 * Free the ring and unbind evtchn.
 */
void vdummyback_free_sring(void) {

	/* Prepare to empty all buffers */
	BACK_RING_INIT(&ring, ring.sring, VDUMMY_RING_SIZE);

	unbind_from_irqhandler(ring_irq, NULL);
}

/*
 * Called by the RT domain to propagate args to the backend in the non-RT domain.
 */
void probe_vdummyback(vdummy_sring_t *sring, uint32_t evtchn) {
	BUG_ON(smp_processor_id() != OPENCN_RT_CPU);

	args_init_gate.evtchn = evtchn;
	args_init_gate.sring = sring;
	args_init_gate.domid = smp_processor_id();

	/* Propagate the probe operation on CPU #0 */
	rtdm_do_sync_dom(OPENCN_CPU0, DC_VDUMMY_INIT);
}

/*
 * Called by the dc_event on CPU #0
 */
void vdummyback_init_gate(dc_event_t dc_event) {
	BUG_ON(smp_processor_id() != OPENCN_CPU0);

	vdummyback_setup_sring(args_init_gate.domid, args_init_gate.sring, args_init_gate.evtchn);
}

int vdummyback_init(void) {
	register_dc_event_callback(DC_VDUMMY_INIT, vdummyback_init_gate);

	return 0;
}

device_initcall(vdummyback_init);


