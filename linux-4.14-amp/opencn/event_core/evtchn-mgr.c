/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/*
 * evtchn manager
 *
 * Functions are in charge of processing event channels at the low level.
 */

#include <linux/smp.h>
#include <linux/slab.h>
#include <linux/irqflags.h>

#include <linux/sched/debug.h>

#include <asm/cacheflush.h>
#include <asm/tlbflush.h>

#include <opencn/opencn.h>

#include <opencn/uapi/debug.h>

#include <opencn/event/event_channel.h>
#include <opencn/event/evtchn-mgr.h>
#include <opencn/event/evtchn.h>

#include <opencn/opencn.h>

/* Event channels used for directcomm channel between CPU0 and CPU1 */
unsigned int dc_evtchn[MAX_DOMAINS];

spinlock_t dc_lock;

/*
 * @rtdm_dc_evtchn is the event channel to be used by the non-realtime CPU to send the dc event to the realtime CPU.
 */
unsigned int rtdm_dc_evtchn;

DEFINE_PER_CPU(struct domain, domain);

/*
 * In OpenCN, domID refers to the CPU_ID (domID 0 is CPU #0, domID 1 is CPU #1, etc.)
 */

/*
 * Used to keep track of the target domain for a certain (outgoing) dc_event.
 * Value -1 means no dc_event in progress.
 */
atomic_t dc_outgoing_domID[DC_EVENT_MAX];

dc_event_fn_t *dc_event_callback[DC_EVENT_MAX];

/*
 * Used to store the domID issuing a (incoming) dc_event
 */
atomic_t dc_incoming_domID[DC_EVENT_MAX];

static struct completion dc_stable_lock[DC_EVENT_MAX];


static int evtchn_set_pending(struct domain *d, int evtchn);
extern void smp_send_event_check_cpu(int cpu);

/* Realtime */
static rtdm_event_t rtdm_dc_stable_event[DC_EVENT_MAX];

static rtdm_event_t dc_isr_event;

static rtdm_irq_t dc_irq_handle;

static rtdm_task_t rtdm_dc_isr_task;
static rtdm_task_t rtdm_vbus_task;

/*
 * Used to keep track of the target domain for a certain (outgoing) dc_event.
 * Value -1 means no dc_event in progress.
 */
atomic_t rtdm_dc_outgoing_domID[DC_EVENT_MAX];

/*
 * Used to store the domID issuing a (incoming) dc_event
 */
atomic_t rtdm_dc_incoming_domID[DC_EVENT_MAX];

dc_event_fn_t *rtdm_dc_event_callback[DC_EVENT_MAX];

int get_free_evtchn(struct domain *d) {
	int i = 0;

	for (i = 0; i < NR_EVTCHN; i++)
		if (d->evtchn[i].state == ECS_FREE)
			return i;

	return -ENOSPC;
}

static long evtchn_alloc_unbound(evtchn_alloc_unbound_t *alloc) {
	struct evtchn *chn;
	int evtchn;
	long rc = 0;
	struct domain *d;

	d = ((alloc->dom == DOMID_SELF) ? &per_cpu(domain, smp_processor_id()) : &per_cpu(domain, alloc->dom));

	spin_lock(&d->event_lock);

	if ((evtchn = get_free_evtchn(d)) < 0)
		BUG();

	chn = &d->evtchn[evtchn];

	d->evtchn[evtchn].state = ECS_UNBOUND;

	chn->unbound.remote_domid = alloc->remote_dom;

	alloc->evtchn = evtchn;

	spin_unlock(&d->event_lock);

	return rc;
}

static long evtchn_bind_interdomain(evtchn_bind_interdomain_t *bind) {
	struct evtchn *lchn, *rchn;
	struct domain *ld, *rd;
	int levtchn, revtchn;
	domid_t rdom;
	long rc = 0;
	int valid = 0;

	ld = &per_cpu(domain, smp_processor_id());

	rdom = bind->remote_dom;
	revtchn = bind->remote_evtchn;

	if (rdom == DOMID_SELF)
		rdom = per_cpu(domain, smp_processor_id()).domain_id;

	rd = &per_cpu(domain, rdom);

	/* Avoid deadlock by first acquiring lock of domain with smaller id. */
	if (ld < rd) {
		spin_lock(&ld->event_lock);
		spin_lock(&rd->event_lock);
	} else {
		if (ld != rd)
			spin_lock(&rd->event_lock);
		spin_lock(&ld->event_lock);
	}

	if ((levtchn = get_free_evtchn(ld)) < 0)
		BUG();

	lchn = &ld->evtchn[levtchn];
	rchn = &rd->evtchn[revtchn];

	valid = ((rchn->state == ECS_INTERDOMAIN) && (rchn->interdomain.remote_dom == NULL));

	if (!valid && ((rchn->state != ECS_UNBOUND) || (rchn->unbound.remote_domid != ld->domain_id)))
		BUG();

	lchn->interdomain.remote_dom = rd;
	lchn->interdomain.remote_evtchn = revtchn;
	lchn->state = ECS_INTERDOMAIN;

	rchn->interdomain.remote_dom = ld;
	rchn->interdomain.remote_evtchn = levtchn;
	rchn->state = ECS_INTERDOMAIN;

	bind->local_evtchn = levtchn;

	spin_unlock(&ld->event_lock);

	if (ld != rd)
		spin_unlock(&rd->event_lock);

	return rc;
}

long __evtchn_close(struct domain *d1, int chn) {
	struct domain *d2 = NULL;
	struct evtchn *chn1, *chn2;
	int evtchn2;
	long rc = 0;

	again:
	spin_lock(&d1->event_lock);

	chn1 = &d1->evtchn[chn];

	switch (chn1->state) {
	case ECS_FREE:
	case ECS_RESERVED:
		rc = -EINVAL;
		goto out;

	case ECS_UNBOUND:
		break;

	case ECS_VIRQ:
		/* Not handled */
		BUG();
		break;

	case ECS_INTERDOMAIN:

		if (d2 == NULL) {

			d2 = chn1->interdomain.remote_dom;

			if (d2 != NULL) {
				/* If we unlock d1 then we could lose d2. Must get a reference. */

				if (d1 < d2) {
					spin_lock(&d2->event_lock);
				} else if (d1 != d2) {
					spin_unlock(&d1->event_lock);
					spin_lock(&d2->event_lock);
					goto again;
				}
			}
		} else if (d2 != chn1->interdomain.remote_dom) {
			/*
			 * We can only get here if the evtchn was closed and re-bound after
			 * unlocking d1 but before locking d2 above. We could retry but
			 * it is easier to return the same error as if we had seen the
			 * evtchn in ECS_CLOSED. It must have passed through that state for
			 * us to end up here, so it's a valid error to return.
			 */
			rc = -EINVAL;
			goto out;
		}

		if (d2 != NULL) {
			evtchn2 = chn1->interdomain.remote_evtchn;

			chn2 = &d2->evtchn[evtchn2];

			BUG_ON(chn2->state != ECS_INTERDOMAIN);
			BUG_ON(chn2->interdomain.remote_dom != d1);

			chn2->state = ECS_UNBOUND;
			chn2->unbound.remote_domid = d1->domain_id;
		}
		break;

	default:
		BUG();
	}

	/* Clear pending event to avoid unexpected behavior on re-bind. */
	clear_bit(chn, (unsigned long *)&d1->shared_info->evtchn_pending);

	/* Reset binding when the channel is freed. */
	chn1->state = ECS_FREE;

	out:

	if (d2 != NULL) {
		if (d1 != d2)
			spin_unlock(&d2->event_lock);
	}

	spin_unlock(&d1->event_lock);

	return rc;
}


static long evtchn_close(evtchn_close_t *close) {
	return __evtchn_close(&per_cpu(domain, smp_processor_id()), close->evtchn);
}

int evtchn_send(struct domain *d, unsigned int levtchn) {
	struct evtchn *lchn;
	struct domain *ld = d, *rd;
	int revtchn = 0, ret = 0;

	lchn = &ld->evtchn[levtchn];

	rd = lchn->interdomain.remote_dom;

	if (lchn->state != ECS_INTERDOMAIN) {
		/* Abnormal situation */
		printk("%s: failure, undefined state: %d, local domain: %d, remote domain: %d, revtchn: %d, levtchn: %d, CPU: %d\n", __func__, lchn->state, ld->domain_id, ((rd != NULL) ? rd->domain_id : -1), revtchn, levtchn, smp_processor_id());

		BUG();
	}

	/* Avoid deadlock by first acquiring lock of domain with smaller id. */
	if (ld < rd) {
		spin_lock(&ld->event_lock);
		spin_lock(&rd->event_lock);
	} else {
		if (ld != rd)
			spin_lock(&rd->event_lock);
		spin_lock(&ld->event_lock);
	}

	revtchn = lchn->interdomain.remote_evtchn;

	evtchn_set_pending(rd, revtchn);
	spin_unlock(&ld->event_lock);

	if (ld != rd)
		spin_unlock(&rd->event_lock);

	return ret;
}

static int evtchn_set_pending(struct domain *d, int evtchn) {

	/*
	 * The following bit operations must happen in strict order.
	 */
	BUG_ON(!irqs_disabled());

	/* Here, it is necessary to use a transactional set bit function for manipulating the bitmap since
	 * we are working at the bit level and some simultaneous write can be done by the CPU0 or CPU1.
	 * Hence, we avoid to be overwritten by such a write.
	 */

	transaction_set_bit(evtchn, (unsigned long *) &d->shared_info->evtchn_pending);

	d->shared_info->evtchn_upcall_pending = 1;

	mb();

	if (smp_processor_id() != d->processor)
		smp_send_event_check_cpu(d->processor);

	return 0;
}

static long evtchn_status(evtchn_status_t *status) {
	struct domain *d = &per_cpu(domain, status->dom);
	int evtchn = status->evtchn;
	struct evtchn *chn;
	long rc = 0;

	spin_lock(&d->event_lock);

	chn = &d->evtchn[evtchn];

	switch (chn->state) {
	case ECS_FREE:
	case ECS_RESERVED:
		status->status = EVTCHNSTAT_closed;
		break;

	case ECS_UNBOUND:
		status->status = EVTCHNSTAT_unbound;
		status->u.unbound.dom = chn->unbound.remote_domid;
		break;

	case ECS_INTERDOMAIN:
		status->status = EVTCHNSTAT_interdomain;
		status->u.interdomain.dom =
				chn->interdomain.remote_dom->domain_id;
		status->u.interdomain.evtchn = chn->interdomain.remote_evtchn;
		break;

	case ECS_VIRQ:
		status->status = EVTCHNSTAT_virq;
		status->u.virq = chn->virq;
		break;

	default:
		BUG();
	}

	spin_unlock(&d->event_lock);

	return rc;
}

long do_event_channel_op(int cmd, void *args) {
	struct evtchn_alloc_unbound alloc_unbound;

	switch (cmd) {
	case EVTCHNOP_alloc_unbound: {

		memcpy(&alloc_unbound, args, sizeof(struct evtchn_alloc_unbound));
		evtchn_alloc_unbound(&alloc_unbound);
		memcpy(args, &alloc_unbound, sizeof(struct evtchn_alloc_unbound));

		break;
	}

	case EVTCHNOP_bind_interdomain: {

		struct evtchn_bind_interdomain bind_interdomain;

		memcpy(&bind_interdomain, args, sizeof(struct evtchn_bind_interdomain));
		evtchn_bind_interdomain(&bind_interdomain);
		memcpy(args, &bind_interdomain, sizeof(struct evtchn_bind_interdomain));

		break;
	}

	case EVTCHNOP_bind_existing_interdomain: {

		/* Not handled... */
		BUG();

		break;
	}

	case EVTCHNOP_bind_virq: {

		/* Not handled... */
		BUG();
	}

	case EVTCHNOP_close: {

		struct evtchn_close close;

		memcpy(&close, args, sizeof(struct evtchn_close));

		evtchn_close(&close);
		break;
	}

	case EVTCHNOP_send: {

		struct evtchn_send send;

		memcpy(&send, args, sizeof(struct evtchn_send));

		evtchn_send(&per_cpu(domain, smp_processor_id()), send.evtchn);

		break;
	}

	case EVTCHNOP_status: {

		struct evtchn_status status;

		memcpy(&status, args, sizeof(struct evtchn_status));
		evtchn_status(&status);
		memcpy(args, &status, sizeof(struct evtchn_status));

		break;
	}

	default:
		BUG();
		break;
	}

	return 0;
}

int evtchn_init(struct domain *d) {
	int i;

	spin_lock_init(&d->event_lock);

	d->evtchn[0].state = ECS_RESERVED;
	d->evtchn[0].can_notify = true;

	for (i = 1; i < NR_EVTCHN; i++) {
		d->evtchn[i].state = ECS_FREE;
		d->evtchn[i].can_notify = true;
	}

	return 0;
}

void evtchn_destroy(struct domain *d) {
	int i;

	/* After this barrier no new event-channel allocations can occur. */
	spin_lock(&d->event_lock);

	/* Close all existing event channels. */
	for (i = 0; i < NR_EVTCHN; i++)
		(void) __evtchn_close(d, i);
}

static void domain_dump_evtchn_info(struct domain *d) {
	unsigned int i;

	spin_lock(&d->event_lock);

	for (i = 1; i < NR_EVTCHN; i++) {
		const struct evtchn *chn;

		chn = &d->evtchn[i];
		if (chn->state == ECS_FREE)
			continue;

		printk("  Dom: %d  chn: %d pending:%d: state: %d",
				d->domain_id, i,
				!!test_bit(i, (unsigned long *) &d->shared_info->evtchn_pending),
				chn->state);

		switch (chn->state) {
		case ECS_UNBOUND:
			printk(" unbound:remote_domid:%d",
					chn->unbound.remote_domid);
			break;

		case ECS_INTERDOMAIN:
			printk(" interdomain remote_dom:%d remove_evtchn: %d",
					chn->interdomain.remote_dom->domain_id,
					chn->interdomain.remote_evtchn);
			break;

		case ECS_VIRQ:
			printk(" VIRQ: %d", chn->virq);
			break;
		}

		printk("\n");

	}

	spin_unlock(&d->event_lock);
}

void dump_evtchn_info(void) {
	int i;

	printk("*** Dumping event-channel info\n");

	for (i = 0; i < MAX_DOMAINS; i++)
		domain_dump_evtchn_info(&per_cpu(domain, i));

}

void evtchn_mgr_init(void) {
	int i;
	printk("Event channel manager init...\n");

	for (i = 0; i < DC_EVENT_MAX; i++) {
		atomic_set(&dc_outgoing_domID[i], -1);
		atomic_set(&dc_incoming_domID[i], -1);

		dc_event_callback[i] = NULL;

		init_completion(&dc_stable_lock[i]);
	}

	for (i = 0; i < MAX_DOMAINS; i++) {
		memset(&per_cpu(domain, i), 0, sizeof(struct domain));
		per_cpu(domain, i).shared_info = kzalloc(sizeof(shared_info_t), GFP_KERNEL);
		spin_lock_init(&per_cpu(domain, i).event_lock);
	}

	/* Current domain/CPU topology */

	per_cpu(domain, OPENCN_CPU0).domain_id = DOMID_CPU0;
	per_cpu(domain, OPENCN_CPU0).processor = OPENCN_CPU0;

	per_cpu(domain, OPENCN_RT_CPU).domain_id = DOMID_RT_CPU;
	per_cpu(domain, OPENCN_RT_CPU).processor = OPENCN_RT_CPU;

	opencn_init_irq();
}

void dc_stable(int dc_event)
{
	/* It may happen that the thread which performs the down did not have time to perform the call and is not suspended.
	 * In this case, complete() will increment the count and wait_for_completion() will go straightforward.
	 */
	atomic_set(&dc_outgoing_domID[dc_event], -1);
	atomic_set(&dc_incoming_domID[dc_event], -1);

	complete(&dc_stable_lock[dc_event]);
}

void register_dc_event_callback(dc_event_t dc_event, dc_event_fn_t *callback) {
	dc_event_callback[dc_event] = callback;
}

void set_dc_event(domid_t domid, dc_event_t dc_event) {
	struct domain *dom;

	dom = &per_cpu(domain, domid);

	/* The shared info page is set as non cacheable, i.e. if a CPU tries to update it, it becomes visible to other CPUs */
	while (atomic_cmpxchg(&dom->shared_info->dc_event, DC_NO_EVENT, dc_event) != DC_NO_EVENT) ;
}

/*
 * Sends a ping event to a remote domain in order to get synchronized.
 * Various types of event (dc_event) can be sent.
 *
 * To perform a ping from the RT domain, please use rtdm_do_sync_cpu0() in rtdm_vbus.c
 *
 * As for the domain table, index 0 is for CPU 0 while 1 is for CPU #1 (RT).
 *
 * @domID: the target domain
 * @dc_event: type of event used in the synchronization
 */
void do_sync_dom(int domID, dc_event_t dc_event)
{
	/* Ping the remote domain to perform the task associated to the DC event */
	DBG("%s: ping domain %d...\n", __func__, domID);

	/* Make sure a previous transaction is not ongoing. */
	while (atomic_cmpxchg(&dc_outgoing_domID[dc_event], -1, domID) != -1) { }

	/* Configure the dc event on the target domain */

	set_dc_event(domID, dc_event);

	notify_remote_via_evtchn(dc_evtchn[domID]);

	/* Wait for the response from the outgoing domain */
	wait_for_completion(&dc_stable_lock[dc_event]);
}

/*
 * Tell a specific domain that we are now in a stable state regarding the dc_event actions.
 * Typically, this comes after receiving a dc_event leading to a dc_event related action.
 */
void tell_dc_stable(int dc_event)  {
	int domID;

	domID = atomic_read(&dc_incoming_domID[dc_event]);

	BUG_ON(domID == -1);

	DBG("vbus_stable: now pinging domain %d back\n", domID);

	set_dc_event(domID, dc_event);

	/* Ping the remote domain to perform the task associated to the DC event */
	DBG("%s: ping domain %d...\n", __func__, dc_incoming_domID[dc_event]);

	atomic_set(&dc_incoming_domID[dc_event], -1);

	notify_remote_via_evtchn(dc_evtchn[domID]);
}



void dump_threads(void)
{
	struct task_struct *p;

	for_each_process(p) {

		lprintk("  Backtrace for process pid: %d\n\n", p->pid);

		show_stack(p, NULL);
	}
}

void perform_task(dc_event_t dc_event)
{
	/* Perform the associated callback function to this particular dc_event */
	if (dc_event_callback[dc_event] != NULL)
		(*dc_event_callback[dc_event])(dc_event);

	tell_dc_stable(dc_event);
}

void __flush_all(void)
{
	/* Flush all cache */
	flush_cache_all();
	flush_tlb_all();
}



/**
 * Allocate an event channel for the given vbus_device, assigning the newly
 * created local evtchn to *evtchn.  Return 0 on success, or -errno on error.  On
 * error, the device will switch to VbusStateClosing, and the error will be
 * saved in the store.
 */
int alloc_evtchn(domid_t domid, int *evtchn)
{
	struct evtchn_alloc_unbound alloc_unbound;
	int err;

	alloc_unbound.dom = DOMID_SELF;
	alloc_unbound.remote_dom = domid;

	err = HYPERVISOR_event_channel_op(EVTCHNOP_alloc_unbound, &alloc_unbound);
	if (err) {
	  lprintk("%s - line %d: allocating event channel failed / evtchn: %d\n", __func__, __LINE__, evtchn);
		BUG();
	} else
		*evtchn = alloc_unbound.evtchn;

	return err;
}

/*
 * Perform a bottom half (deferred) processing on the receival of dc_event.
 * Here, we avoid to use a worqueue.
 */
static irqreturn_t directcomm_isr_thread(int irq, void *args) {
	dc_event_t dc_event;

	dc_event = atomic_read((const atomic_t *) &cpu_shared_info->dc_event);

	/* Reset the dc_event now so that the domain can send another dc_event */
	atomic_set((atomic_t *) &cpu_shared_info->dc_event, DC_NO_EVENT);

	perform_task(dc_event);

	return IRQ_HANDLED;
}

/*
 * Interrupt routine for direct communication event channel
 * IRQs are off
 */
static irqreturn_t directcomm_isr(int irq, void *args) {
	dc_event_t dc_event;
	unsigned int domID = *((unsigned int *) args);

	dc_event = atomic_read((const atomic_t *) &cpu_shared_info->dc_event);

	DBG("Received directcomm interrupt for event: %d\n", cpu_shared_info->dc_event);

	switch (dc_event) {

	/*
	 * Some of these events are processed in CPU #0 *only* when the remote domain will complete the dc_event (dc_stable).
	 * If no dc_event is in progress, this will fail in perform_task().
	 */

	case DC_VDUMMY_INIT:
	case DC_VLOG_INIT:
	case DC_VLOG_FLUSH:

		/* Check if it is the response to a dc_event. Can be done immediately in the top half. */
		if (atomic_read(&dc_outgoing_domID[dc_event]) != -1) {

			dc_stable(dc_event);
			break; /* Out of the switch */
		}

		/* We should not receive twice a same dc_event, before it has been fully processed. */
		BUG_ON(atomic_read(&dc_incoming_domID[dc_event]) != -1);
		atomic_set(&dc_incoming_domID[dc_event], domID);

		/* Start the deferred thread */
		return IRQ_WAKE_THREAD;

	default:
		lprintk("%s CPU #%d: something weird happened, directcomm interrupt was triggered with dc_event %d, but no DC event was configured !\n", __func__, smp_processor_id(), dc_event);
		break;
	}

	/* Reset the dc_event now so that the domain can send another dc_event */
	atomic_set((atomic_t *) &cpu_shared_info->dc_event, DC_NO_EVENT);

	return IRQ_HANDLED;
}

/* Realtime */

void rtdm_register_dc_event_callback(dc_event_t dc_event, dc_event_fn_t *callback) {
	rtdm_dc_event_callback[dc_event] = callback;
}

static void rtdm_dc_stable(int dc_event)
{
	atomic_set(&rtdm_dc_outgoing_domID[dc_event], -1);

	/* Wake up the waiter */
	rtdm_event_signal(&rtdm_dc_stable_event[dc_event]);
}

/*
 * Called to inform the non-RT side that we have completed a dc_event processing.
 */
void rtdm_tell_dc_stable(int dc_event)  {
	DBG("Now pinging domain %d back\n", DOMID_CPU0);

	/* Make sure a previous transaction is not being processed */
	set_dc_event(OPENCN_CPU0, dc_event);

	atomic_set(&rtdm_dc_incoming_domID[dc_event], -1);

	notify_remote_via_evtchn(dc_evtchn[DOMID_CPU0]);

}

/*
 * Sends a ping event to a non-realtime CPU in order to get synchronized.
 * Various types of event (dc_event) can be sent.
 *
 * @dc_event: type of event used in the synchronization
 */
void rtdm_do_sync_dom(int domID, dc_event_t dc_event) {

	/* Ping the remote domain to perform the task associated to the DC event */
	DBG("%s: ping domain %d...\n", __func__, domID);

	/* Make sure a previous transaction is not being processed */
	while (atomic_cmpxchg(&rtdm_dc_outgoing_domID[dc_event], -1, domID) != -1) ;

	/* We avoid that a domain running on another CPU tries to update the dc_event field at the same time. */
	set_dc_event(domID, dc_event);

	notify_remote_via_evtchn(dc_evtchn[domID]);

	/* Wait for the response */
	rtdm_event_wait(&rtdm_dc_stable_event[dc_event]);
}

/*
 * Perform deferred processing related to directcomm DC_event processing
 */
static void rtdm_dc_isr_task_fn(void *arg) {
	dc_event_t dc_event;

	while (true) {
		rtdm_event_wait(&dc_isr_event);

		dc_event = atomic_read((const atomic_t *) &cpu_shared_info->dc_event);

		/* Reset the dc_event now so that the domain can send another dc_event */
		atomic_set((atomic_t *) &cpu_shared_info->dc_event, DC_NO_EVENT);

		/* Perform the associated callback function to this particular dc_event */
		if (rtdm_dc_event_callback[dc_event] != NULL)
			(*rtdm_dc_event_callback[dc_event])(dc_event);
	}
}

/*
 * Retrieve the pointer to the directcomm ISR thread task
 */
rtdm_task_t *get_dc_isr(void) {
	return &rtdm_dc_isr_task;
}

/*
 * IRQs off
 */
static int rtdm_dc_isr(rtdm_irq_t *unused) {
	dc_event_t dc_event;

	DBG("(CPU domid %d): Received directcomm interrupt for event: %d\n", ME_domID(), cpu_shared_info->dc_event);

	dc_event = atomic_read((const atomic_t *) &cpu_shared_info->dc_event);

	/* Work to be done in ME */

	switch (dc_event) {

	/* The events are either received as an event to process in the RT domain or
	 * an event corresponding to a reply to an event sent by the RT domain to the non-RT domain.
	 */
	case DC_ENABLE_RT:
	case DC_VDUMMY_INIT:
	case DC_VLOG_INIT:
	case DC_VLOG_FLUSH:
	case DC_LCEC_INIT:

		if (atomic_read(&rtdm_dc_outgoing_domID[dc_event]) != -1)
			rtdm_dc_stable(dc_event);
		else {
			/* We should not receive twice a same dc_event, before it has been fully processed. */
			BUG_ON(atomic_read(&rtdm_dc_incoming_domID[dc_event]) != -1);
			atomic_set(&rtdm_dc_incoming_domID[dc_event], DOMID_CPU0);

			/* Perform deferred processing for these events */
			rtdm_event_signal(&dc_isr_event);

			return RTDM_IRQ_HANDLED;
		}
		break;

	default:
		lprintk("%s: something weird happened on CPU %d, RT directcomm interrupt was triggered, but no DC event (%d) was configured !\n", __func__, smp_processor_id(), cpu_shared_info->dc_event);
		break;
	}

	/* Reset the dc_event now so that the domain can send another dc_event */
	atomic_set((atomic_t *) &cpu_shared_info->dc_event, DC_NO_EVENT);

	return RTDM_IRQ_HANDLED;
}

/*
 * Main realtime task which manages initialization of RT vbus and watch monitoring.
 */
void rtdm_vbus_task_fn(void *unused) {
	int i, irq;

	/* Bind the directcomm handler to a RT ISR which will perform the related work */
	for (i = 0; i < DC_EVENT_MAX; i++) {
		atomic_set(&rtdm_dc_outgoing_domID[i], -1);
		atomic_set(&rtdm_dc_incoming_domID[i], -1);

		rtdm_dc_event_callback[i] = NULL;

		rtdm_event_init(&rtdm_dc_stable_event[i], 0);
	}

	/* Binding the irqhandler to the eventchannel */
	DBG("%s: setting up the directcomm event channel (%d) ...\n", __func__, rtdm_dc_evtchn);

	irq = rtdm_bind_interdomain_evtchn_to_irqhandler(&dc_irq_handle, DOMID_CPU0, rtdm_dc_evtchn, rtdm_dc_isr, 0, "rtdm_dc_isr", NULL);
	if (irq <= 0) {
		printk(KERN_ERR "Error: bind_evtchn_to_irqhandler failed");
		BUG();
	}

	dc_evtchn[0] = evtchn_from_irq(irq);

	DBG("%s: local event channel bound to directcomm towards non-RT CPU : %d\n", __func__, dc_evtchn[0]);

}

static int __rtdm_task_prologue(void *args) {
	rtdm_event_init(&dc_isr_event, 0);

	rtdm_task_init(&rtdm_dc_isr_task, "rtdm_dc_isr", rtdm_dc_isr_task_fn, NULL, DC_ISR_TASK_PRIO, 0);
	rtdm_task_init(&rtdm_vbus_task, "rtdm_vbus", rtdm_vbus_task_fn, NULL, VBUS_TASK_PRIO, 0);

	/* We can leave this thread die. Our system is living anyway... */
	do_exit(0);

	return 0;
}

int rtdm_vbus_init(void) {

	/* Prepare to initiate a Cobalt RT task */
	kernel_thread(__rtdm_task_prologue, NULL, 0);

	return 0;
}


static int __init vbus_init(void)
{
	int res = 0;
	int i;
	int evtchn;
	struct evtchn_alloc_unbound alloc_unbound;
	unsigned int *p_domID;

	res = -ENODEV;

	spin_lock_init(&dc_lock);

	evtchn_mgr_init();

	/*
	 * Set up the directcomm communication channel that
	 * is used between the domains.
	 */

	for (i = 1; i < MAX_DOMAINS; i++) {
		/* Get a free event channel */
		alloc_unbound.dom = DOMID_SELF;
		alloc_unbound.remote_dom = i;

		res = HYPERVISOR_event_channel_op(EVTCHNOP_alloc_unbound, &alloc_unbound);
		if (res == -ENOSYS) {
			printk(KERN_ERR "Error: allocating event channel failed");
			BUG();
		}
		BUG_ON(res);

		dc_evtchn[i] = alloc_unbound.evtchn;

		/* Keep a valid reference to the domID */
		p_domID = kmalloc(sizeof(int), GFP_KERNEL);

		*p_domID = i;

		/* Binding this event channel to an interrupt handler makes the evtchn state not "unbound" anymore */
		evtchn = bind_evtchn_to_irq_handler(dc_evtchn[i], directcomm_isr, directcomm_isr_thread, 0, "directcomm_isr", p_domID);

		if (evtchn <= 0) {
			printk(KERN_ERR "Error: bind_evtchn_to_irqhandler failed");
			BUG();
		}

		rtdm_dc_evtchn = dc_evtchn[i]; /* Temporary */

		DBG("%s: direct communication set up between CPU #0 and CPU %d with event channel: %d irq: %d\n", __func__, i, dc_evtchn[i], evtchn);
	}


	DBG("vbus_init OK!\n");

	rtdm_vbus_init();

	return 0;
}

postcore_initcall(vbus_init);


