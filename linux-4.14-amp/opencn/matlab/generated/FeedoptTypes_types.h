/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptTypes_types.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

#ifndef FEEDOPTTYPES_TYPES_H
#define FEEDOPTTYPES_TYPES_H

/* Include Files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef enum_CurveType
#define enum_CurveType

enum CurveType
{
    CurveType_None = 0,                /* Default value */
    CurveType_Line = 1,
    CurveType_Helix = 2,
    CurveType_TransP5 = 4
};

#endif                                 /*enum_CurveType*/

#ifndef typedef_CurveType
#define typedef_CurveType

typedef enum CurveType CurveType;

#endif                                 /*typedef_CurveType*/

#ifndef enum_ZSpdMode
#define enum_ZSpdMode

enum ZSpdMode
{
    ZSpdMode_NN = 0,                   /* Default value */
    ZSpdMode_ZN,
    ZSpdMode_NZ,
    ZSpdMode_ZZ
};

#endif                                 /*enum_ZSpdMode*/

#ifndef typedef_ZSpdMode
#define typedef_ZSpdMode

typedef enum ZSpdMode ZSpdMode;

#endif                                 /*typedef_ZSpdMode*/

#ifndef typedef_CurvStruct
#define typedef_CurvStruct

typedef struct {
    CurveType Type;
    ZSpdMode ZSpdMode;
    double P0[3];
    double P1[3];
    double evec[3];
    double theta;
    double pitch;
    double CoeffP5[6][3];
    double FeedRate;
} CurvStruct;

#endif                                 /*typedef_CurvStruct*/

#ifndef typedef_FeedoptConfigStruct
#define typedef_FeedoptConfigStruct

typedef struct {
    int NDiscr;
    int NBreak;
    int NHorz;
    int MaxNHorz;
    int MaxNDiscr;
    int MaxNCoeff;
    double vmax;
    double amax[3];
    double jmax[3];
    int SplineDegree;
    double CutOff;
    double LSplit;
    double v_0;
    double at_0;
    double v_1;
    double at_1;
    char source[1024];
    boolean_T DebugPrint;
} FeedoptConfigStruct;

#endif                                 /*typedef_FeedoptConfigStruct*/

#ifndef typedef_OptStruct
#define typedef_OptStruct

typedef struct {
    double Coeff[120];
    CurvStruct CurvStruct;
} OptStruct;

#endif                                 /*typedef_OptStruct*/

#ifndef enum_PushStatus
#define enum_PushStatus

enum PushStatus
{
    PushStatus_Success = 0,            /* Default value */
    PushStatus_TryAgain,
    PushStatus_Finished
};

#endif                                 /*enum_PushStatus*/

#ifndef typedef_PushStatus
#define typedef_PushStatus

typedef enum PushStatus PushStatus;

#endif                                 /*typedef_PushStatus*/

#ifndef enum_QueueId
#define enum_QueueId

enum QueueId
{
    QueueId_GCode = 0,                 /* Default value */
    QueueId_Smooth,
    QueueId_Split,
    QueueId_Opt,
    QueueId_COUNT
};

#endif                                 /*enum_QueueId*/

#ifndef typedef_QueueId
#define typedef_QueueId

typedef enum QueueId QueueId;

#endif                                 /*typedef_QueueId*/

#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
    double *data;
    int *size;
    int allocatedSize;
    int numDimensions;
    boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T

typedef struct emxArray_real_T emxArray_real_T;

#endif                                 /*typedef_emxArray_real_T*/

#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
    unsigned long handle;
    int n;
} struct0_T;

#endif                                 /*typedef_struct0_T*/
#endif

/*
 * File trailer for FeedoptTypes_types.h
 *
 * [EOF]
 */
