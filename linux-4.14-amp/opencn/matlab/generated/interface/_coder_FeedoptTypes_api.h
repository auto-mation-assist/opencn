/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_FeedoptTypes_api.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

#ifndef _CODER_FEEDOPTTYPES_API_H
#define _CODER_FEEDOPTTYPES_API_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"

/* Type Definitions */
#ifndef enum_CurveType
#define enum_CurveType

enum CurveType
{
  CurveType_None = 0,                  /* Default value */
  CurveType_Line = 1,
  CurveType_Helix = 2,
  CurveType_TransP5 = 4
};

#endif                                 /*enum_CurveType*/

#ifndef typedef_CurveType
#define typedef_CurveType

typedef enum CurveType CurveType;

#endif                                 /*typedef_CurveType*/

#ifndef enum_ZSpdMode
#define enum_ZSpdMode

enum ZSpdMode
{
  ZSpdMode_NN = 0,                     /* Default value */
  ZSpdMode_ZN,
  ZSpdMode_NZ,
  ZSpdMode_ZZ
};

#endif                                 /*enum_ZSpdMode*/

#ifndef typedef_ZSpdMode
#define typedef_ZSpdMode

typedef enum ZSpdMode ZSpdMode;

#endif                                 /*typedef_ZSpdMode*/

#ifndef typedef_CurvStruct
#define typedef_CurvStruct

typedef struct {
  CurveType Type;
  ZSpdMode ZSpdMode;
  real_T P0[3];
  real_T P1[3];
  real_T evec[3];
  real_T theta;
  real_T pitch;
  real_T CoeffP5[6][3];
  real_T FeedRate;
} CurvStruct;

#endif                                 /*typedef_CurvStruct*/

#ifndef typedef_FeedoptConfigStruct
#define typedef_FeedoptConfigStruct

typedef struct {
  int32_T NDiscr;
  int32_T NBreak;
  int32_T NHorz;
  int32_T MaxNHorz;
  int32_T MaxNDiscr;
  int32_T MaxNCoeff;
  real_T vmax;
  real_T amax[3];
  real_T jmax[3];
  int32_T SplineDegree;
  real_T CutOff;
  real_T LSplit;
  real_T v_0;
  real_T at_0;
  real_T v_1;
  real_T at_1;
  char_T source[1024];
  boolean_T DebugPrint;
} FeedoptConfigStruct;

#endif                                 /*typedef_FeedoptConfigStruct*/

#ifndef typedef_OptStruct
#define typedef_OptStruct

typedef struct {
  real_T Coeff[120];
  CurvStruct CurvStruct;
} OptStruct;

#endif                                 /*typedef_OptStruct*/

#ifndef enum_PushStatus
#define enum_PushStatus

enum PushStatus
{
  PushStatus_Success = 0,              /* Default value */
  PushStatus_TryAgain,
  PushStatus_Finished
};

#endif                                 /*enum_PushStatus*/

#ifndef typedef_PushStatus
#define typedef_PushStatus

typedef enum PushStatus PushStatus;

#endif                                 /*typedef_PushStatus*/

#ifndef enum_QueueId
#define enum_QueueId

enum QueueId
{
  QueueId_GCode = 0,                   /* Default value */
  QueueId_Smooth,
  QueueId_Split,
  QueueId_Opt,
  QueueId_COUNT
};

#endif                                 /*enum_QueueId*/

#ifndef typedef_QueueId
#define typedef_QueueId

typedef enum QueueId QueueId;

#endif                                 /*typedef_QueueId*/

#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  uint64_T handle;
  int32_T n;
} struct0_T;

#endif                                 /*typedef_struct0_T*/

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void EvalCurvStruct(CurvStruct *b_CurvStruct, real_T u_vec_data[],
  int32_T u_vec_size[2], real_T r0D_data[], int32_T r0D_size[2], real_T
  r1D_data[], int32_T r1D_size[2], real_T r2D_data[], int32_T r2D_size[2],
  real_T r3D_data[], int32_T r3D_size[2]);
extern void EvalCurvStruct_api(const mxArray * const prhs[2], int32_T nlhs,
  const mxArray *plhs[4]);
extern void FeedoptDefaultConfig(FeedoptConfigStruct *cfg);
extern void FeedoptDefaultConfig_api(int32_T nlhs, const mxArray *plhs[1]);
extern void FeedoptTypes(CurvStruct *b_CurvStruct, FeedoptConfigStruct
  *FeedoptConfig, QueueId Q, PushStatus push_status);
extern void FeedoptTypes_api(const mxArray * const prhs[4], int32_T nlhs);
extern void FeedoptTypes_atexit(void);
extern void FeedoptTypes_initialize(void);
extern void FeedoptTypes_terminate(void);
extern void FeedoptTypes_xil_shutdown(void);
extern void FeedoptTypes_xil_terminate(void);
extern void InitConfig(void);
extern void InitConfig_api(int32_T nlhs);
extern void PrintCurvStruct(CurvStruct *S);
extern void PrintCurvStruct_api(const mxArray * const prhs[1], int32_T nlhs);
extern void ResampleTick(OptStruct CurOptStruct, OptStruct NextOptStruct,
  struct0_T Bl, real_T *u, real_T dt, boolean_T *pop_next, boolean_T *finished);
extern void ResampleTick_api(const mxArray * const prhs[5], int32_T nlhs, const
  mxArray *plhs[3]);
extern void c_linspace(real_T x0, real_T x1, int32_T N, real_T A_data[], int32_T
  A_size[2]);
extern void c_linspace_api(const mxArray * const prhs[3], int32_T nlhs, const
  mxArray *plhs[1]);
extern void sinspace(real_T x0, real_T x1, int32_T N, real_T x_data[], int32_T
                     x_size[2]);
extern void sinspace_api(const mxArray * const prhs[3], int32_T nlhs, const
  mxArray *plhs[1]);

#endif

/*
 * File trailer for _coder_FeedoptTypes_api.h
 *
 * [EOF]
 */
