/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: sinspace.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "sinspace.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_linspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : double x0
 *                double x1
 *                int N
 *                double x_data[]
 *                int x_size[2]
 * Return Type  : void
 */
void sinspace(double x0, double x1, int N, double x_data[], int x_size[2])
{
    int n1;
    emxArray_real_T *t;
    int i;
    double delta1;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    n1 = N;
    if (N < 0) {
        n1 = 0;
    }

    emxInit_real_T(&t, 2);
    i = t->size[0] * t->size[1];
    t->size[0] = 1;
    t->size[1] = n1;
    emxEnsureCapacity_real_T(t, i);
    if (n1 >= 1) {
        t->data[n1 - 1] = 0.0;
        if (t->size[1] >= 2) {
            t->data[0] = -1.0;
            if (t->size[1] >= 3) {
                delta1 = 1.0 / ((double)t->size[1] - 1.0);
                i = t->size[1];
                for (n1 = 0; n1 <= i - 3; n1++) {
                    t->data[n1 + 1] = ((double)n1 + 1.0) * delta1 + -1.0;
                }
            }
        }
    }

    i = t->size[0] * t->size[1];
    t->size[0] = 1;
    emxEnsureCapacity_real_T(t, i);
    n1 = t->size[1];
    for (i = 0; i < n1; i++) {
        t->data[i] *= 3.1415926535897931;
    }

    i = t->size[1];
    for (n1 = 0; n1 < i; n1++) {
        t->data[n1] = cos(t->data[n1]);
    }

    delta1 = x1 - x0;
    x_size[0] = 1;
    x_size[1] = t->size[1];
    n1 = t->size[1];
    for (i = 0; i < n1; i++) {
        x_data[i] = (t->data[i] * 0.5 + 0.5) * delta1 + x0;
    }

    emxFree_real_T(&t);
}

/*
 * File trailer for sinspace.c
 *
 * [EOF]
 */
