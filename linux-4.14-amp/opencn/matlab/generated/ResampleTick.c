/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: ResampleTick.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "ResampleTick.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "c_linspace.h"
#include "c_spline.h"
#include "sinspace.h"
#include <math.h>
#include <stdio.h>

/* Function Definitions */

/*
 * Arguments    : const OptStruct *CurOptStruct
 *                const OptStruct *NextOptStruct
 *                const struct0_T *Bl
 *                double *u
 *                double dt
 *                boolean_T *pop_next
 *                boolean_T *finished
 * Return Type  : void
 */
void ResampleTick(const OptStruct *CurOptStruct, const OptStruct *NextOptStruct,
                  const struct0_T *Bl, double *u, double dt, boolean_T *pop_next,
                  boolean_T *finished)
{
    int i;
    double Trest;
    double coeffs[120];
    double X[3];
    double ukp1;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    if (CurOptStruct->CurvStruct.Type == CurveType_None) {
        *u = 0.0;
        *pop_next = false;
        *finished = true;
    } else {
        *finished = false;
        for (i = 0; i < 120; i++) {
            coeffs[i] = CurOptStruct->Coeff[i];
        }

        Trest = *u;

        /*  void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]); */
        if (*u < 0.0) {
            printf("ERROR: C_BSPLINE_EVAL: X < 0 (%f)\n", *u);
            fflush(stdout);
            Trest = 0.0;
        } else {
            if (*u > 1.0) {
                printf("ERROR: C_BSPLINE_EVAL: X > 1 (%f)\n", *u);
                fflush(stdout);
                Trest = 1.0;
            }
        }

        c_bspline_eval(&Bl->handle, &coeffs[0], Trest, &X[0]);
        Trest = sqrt(X[0]);
        ukp1 = (*u + X[1] * pow(dt, 2.0) / 4.0) + Trest * dt;
        if (ukp1 < *u) {
            printf("!!! ukp1 < uk !!!\n");
            fflush(stdout);
            *finished = true;
            *pop_next = false;
            *u = 0.0;
        } else {
            /*  */
            if (ukp1 < 1.0) {
                *u = ukp1;
                *pop_next = false;
            } else {
                for (i = 0; i < 120; i++) {
                    coeffs[i] = CurOptStruct->Coeff[i];
                }

                /*  void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]); */
                c_bspline_eval(&Bl->handle, &coeffs[0], 1.0, &X[0]);
                Trest = 2.0 * (1.0 - *u) / (sqrt(X[0]) + Trest);
                if (Trest > dt) {
                    Trest = dt;
                    printf("!!! Trest > dt !!!\n");
                    fflush(stdout);
                }

                /*  */
                Trest = dt - Trest;
                if (NextOptStruct->CurvStruct.Type != CurveType_None) {
                    for (i = 0; i < 120; i++) {
                        coeffs[i] = NextOptStruct->Coeff[i];
                    }

                    /*  void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[3]); */
                    c_bspline_eval(&Bl->handle, &coeffs[0], 0.0, &X[0]);
                    ukp1 = X[1] * pow(Trest, 2.0) / 4.0 + sqrt(X[0]) * Trest;
                    if (ukp1 < 0.0) {
                        printf("!!! ukp1 < uk !!!\n");
                        fflush(stdout);
                        *finished = true;
                        *pop_next = false;
                        *u = 0.0;
                    } else {
                        *u = ukp1;
                        *pop_next = true;
                    }
                } else {
                    *finished = true;
                    *pop_next = false;
                    *u = 1.0;
                }
            }
        }
    }
}

/*
 * File trailer for ResampleTick.c
 *
 * [EOF]
 */
