/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_linspace.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

#ifndef C_LINSPACE_H
#define C_LINSPACE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void c_linspace(double x0, double x1, int N, double A_data[], int A_size
                       [2]);

#endif

/*
 * File trailer for c_linspace.h
 *
 * [EOF]
 */
