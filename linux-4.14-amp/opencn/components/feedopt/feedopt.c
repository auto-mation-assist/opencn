/********************************************************************
 * Description:  feedopt.c
 *               A HAL component that can be used to capture feedopt_data
 *               from HAL pins at a specific realtime sample rate,
 *		and allows the feedopt_data to be written to stdout.
 *
 * Author: John Kasunich <jmkasunich at sourceforge dot net>
 * License: GPL Version 2
 *
 * Copyright (c) 2006 All rights reserved.
 *
 ********************************************************************/
/** This file, 'feedopt.c', is the realtime part of feedopt.
 *
 */

#include <linux/ctype.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#include <asm/fpu/api.h>

#include <opencn/hal/hal.h>			/* HAL public API decls */
#include <opencn/rtapi/rtapi.h>		/* RTAPI realtime OS API */
#include <opencn/rtapi/rtapi_app.h> /* RTAPI realtime module decls */

#include <opencn/components/feedopt.h>

#include <opencn/ctypes/strings.h>

#include <opencn/strtox.h>

#include <opencn/rtapi/rtapi_errno.h>
#include <opencn/rtapi/rtapi_math.h>
#include <opencn/rtapi/rtapi_string.h>

#include <opencn/uapi/feedopt.h>

#include <matlab_headers.h>

#include <linux/wait.h>

static DECLARE_WAIT_QUEUE_HEAD(write_full_wait_queue);

#include "tools.h"

typedef struct {
	int head, tail;
	OptStruct *feedopt_data;
	int capacity;
	volatile int size;
} fopt_rg_t;

static void fopt_rg_alloc(fopt_rg_t *rg, int capacity)
{
	rg->head = 0;
	rg->tail = 0;
	rg->capacity = capacity;
	rg->size = 0;
	rg->feedopt_data = kmalloc(sizeof(optimized_curv_struct_t) * capacity, GFP_ATOMIC);
}

static void fopt_rg_free(fopt_rg_t *rg)
{
	rg->capacity = 0;
	rg->size = 0;
	rg->tail = 0;
	rg->head = 0;
	kfree(rg->feedopt_data);
}

static int fopt_rg_push(fopt_rg_t *rg, OptStruct value)
{
	if (rg->size < rg->capacity) {
		rg->feedopt_data[rg->head++] = value;
		__sync_fetch_and_add(&rg->size, 1);
		if (rg->head == rg->capacity)
			rg->head = 0;
		return 1;
	}
	return 0;
}

static int fopt_rg_pop(fopt_rg_t *rg, OptStruct *value)
{
	if (rg->size > 0) {
		*value = rg->feedopt_data[rg->tail++];
		__sync_fetch_and_add(&rg->size, -1);
		if (rg->tail == rg->capacity)
			rg->tail = 0;
		return 1;
	}
	return 0;
}

static void fopt_rg_clear(fopt_rg_t *rg)
{
	rg->size = 0;
	rg->head = 0;
	rg->tail = 0;
}

/***********************************************************************
 *                STRUCTURES AND GLOBAL VARIABLES                       *
 ************************************************************************/

/* this structure contains the HAL shared memory feedopt_data for the component
 */

typedef struct {
	hal_float_t *pin_sample_pos_out[3]; /* pins: resampled x,y,z positions */
	// hal_float_t *pin_sample_vel_out[3];
	// hal_float_t *pin_sample_acc_out[3];
	// hal_float_t *pin_sample_jerk_out[3];
    hal_bit_t *rt_active, *rt_single_shot, *us_active;
	hal_bit_t *pin_ready_out;
	hal_u32_t *pin_buffer_underrun_count;
    hal_bit_t *opt_rt_reset, *commit_cfg, *opt_us_reset;
	hal_u32_t *queue_size;
	hal_float_t *pin_step_dt;
	hal_float_t *opt_per_second;
	hal_float_t *current_u;
	hal_float_t *feedrate_scale;
    hal_bit_t *rt_start, *us_start;
    hal_bit_t *rt_pause;
    hal_bit_t *rt_moving;
    hal_bit_t *rt_has_segment;
} feedopt_hal_t;

static feedopt_hal_t *fopt_hal;
/* other globals */
static int comp_id; /* component ID */

static bspline_t *spline_handle;
static double u[FOPT_MAX_NDISCR];
static int u_size[2];
static spline_base_t base;
static struct0_T Bl;
static fopt_rg_t opt_structs;
static OptStruct CurOptStruct, NextOptStruct;
static double resample_t;
static double uk;
static double r0D[3];
static double r1D[3];
static double r2D[3];
static double r3D[3];

static int r0D_size[2];
static int r1D_size[2];
static int r2D_size[2];
static int r3D_size[2];
static int u_eval_size[2];

static int buffer_underrun = 0;

static double current_x = 0, current_y = 0, current_z = 0;
static double DT = 0.0;
static double target_DT = 0.0;

static bool should_accept_push = false;

static FeedoptConfigStruct cfg;

typedef enum {
    FEEDOPT_STATE_INACTIVE,
    FEEDOPT_STATE_RUNNING,
    FEEDOPT_STATE_STOPPING,
    FEEDOPT_STATE_PAUSED,
} FEEDOPT_STATE;

static FEEDOPT_STATE state = FEEDOPT_STATE_INACTIVE;

/***********************************************************************
 *                  LOCAL FUNCTION DECLARATIONS                         *
 ************************************************************************/

static int init_feedopt(int num, feedopt_hal_t *tmp_fifo);
static void feedopt_update(void *arg, long period);
static int init_splines(void);

/***********************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/

static int feedopt_app_main(int n, feedopt_connect_args_t *args)
{
	int retval;

	comp_id = hal_init(__core_hal_user, "feedopt");
	if (comp_id < 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: hal_init() failed\n");
		return -EINVAL;
	}

	fopt_hal = hal_malloc(__core_hal_user, sizeof(feedopt_hal_t));

	retval = init_feedopt(n, fopt_hal);
	if (retval < 0)
		goto fail;

	hal_ready(__core_hal_user, comp_id);

	return 0;

fail:

	hal_exit(__core_hal_user, comp_id);

	return retval;
}

void feedopt_app_exit(void)
{
	fopt_rg_free(&opt_structs);
	hal_exit(__core_hal_user, comp_id);
}

/***********************************************************************
 *            REALTIME COUNTER COUNTING AND UPDATE FUNCTIONS            *
 ************************************************************************/

static void pop_next_segment(void)
{
    //	int i;2
	CurOptStruct = NextOptStruct;
	if (fopt_rg_pop(&opt_structs, &NextOptStruct)) {
		// opencn_cprintf(OPENCN_COLOR_YELLOW, "[FEEDOPT] Trajectory pop\n");
		buffer_underrun = 0;
        //wake_up(&write_full_wait_queue);

		//		PrintCurvStruct(&resample_curv.curve);
		//		opencn_printf("Coeffs (%d): ", resample_curv.coeff_count);
		//		for(i = 0; i < resample_curv.coeff_count; i++) {
		//			opencn_printf("%.3f ", (float)resample_curv.coeffs[i]);
		//		}
		//		opencn_printf("\n");

	} else {
		NextOptStruct.CurvStruct.Type = 0;
		if (!buffer_underrun) {
			buffer_underrun = 1;
			opencn_cprintf(OPENCN_COLOR_BRED, "[FEEDOPT] BUFFER UNDERRUN!\n");
		}
        *fopt_hal->pin_buffer_underrun_count += 1;
	}
}

static void feedopt_reset(void)
{

    *fopt_hal->rt_active = 0;
	fopt_rg_clear(&opt_structs);
	wake_up(&write_full_wait_queue);
	fopt_rg_clear(&opt_structs);
	uk = 0;
	*fopt_hal->pin_buffer_underrun_count = 0;
	CurOptStruct.CurvStruct.Type = 0;
	NextOptStruct.CurvStruct.Type = 0;
    init_splines();
    state = FEEDOPT_STATE_INACTIVE;
}

static void feedopt_resample(void) {
    boolean_T pop_next, finished;
    if (CurOptStruct.CurvStruct.Type == 0) {
        uk = 0;
        pop_next_segment();
        pop_next_segment();
    }

    if (CurOptStruct.CurvStruct.Type != 0) {
        u_eval_size[0] = 1;
        u_eval_size[1] = 1;

        EvalCurvStruct(&CurOptStruct.CurvStruct, &uk, u_eval_size, r0D, r0D_size, r1D, r1D_size, r2D, r2D_size, r3D,
                       r3D_size);

        current_x = r0D[0];
        current_y = r0D[1];
        current_z = r0D[2];

        ResampleTick(&CurOptStruct, &NextOptStruct, &Bl, &uk, DT, &pop_next, &finished);
        *fopt_hal->current_u = uk;
        if (pop_next) {
            pop_next_segment();
        }

        if (finished) {
            opencn_cprintf(OPENCN_COLOR_GREEN, "FEEDOPT_RT stopping, ResampleTick returned finished = 1\n");
            feedopt_reset();
        }

        *fopt_hal->pin_sample_pos_out[0] = current_x;
        *fopt_hal->pin_sample_pos_out[1] = current_y;
        *fopt_hal->pin_sample_pos_out[2] = current_z;
    } else {
        /* We tried to pop a segment but nothing came, we have to stop the resampling */
        opencn_cprintf(OPENCN_COLOR_GREEN, "FEEDOPT_RT stopping, CurvStruct.Type = 0\n");
    }
}

static void feedopt_state_inactive(void) {
    target_DT = 0.0;
    *fopt_hal->rt_active = 0;

    if (*fopt_hal->rt_start) {
        state = FEEDOPT_STATE_RUNNING;
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT running\n");
    }

    else if (*fopt_hal->opt_rt_reset) {
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT(inactive) received reset\n");
        feedopt_reset();
    }

    else if (*fopt_hal->rt_single_shot) {
        feedopt_resample();
    }
}

static void feedopt_state_running(double DT0) {
    *fopt_hal->rt_active = 1;
    feedopt_resample();
    target_DT = *fopt_hal->feedrate_scale * DT0;

    if (*fopt_hal->opt_rt_reset) {
        state = FEEDOPT_STATE_STOPPING;
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT stopping\n");
    }

    else if (*fopt_hal->rt_pause) {
        state = FEEDOPT_STATE_PAUSED;
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT paused\n");
    }
}

static void feedopt_state_stopping(double DT0) {
    *fopt_hal->rt_active = 1;
    feedopt_resample();
    target_DT = 0.0;
    if (DT < DT0*1e-4) {
        state = FEEDOPT_STATE_INACTIVE;
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT inactive\n");
        fopt_rg_clear(&opt_structs);
//        wake_up(&write_full_wait_queue);
        feedopt_reset();
        DT = 0.0;
    }
}

static void feedopt_state_paused(double DT0) {
    *fopt_hal->rt_active = 1;
    feedopt_resample();
    target_DT = 0.0;
    if (*fopt_hal->rt_start) {
        state = FEEDOPT_STATE_RUNNING;
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT running\n");
    }

    else if (*fopt_hal->opt_rt_reset) {
        state = FEEDOPT_STATE_STOPPING;
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT stopping\n");
    }

    if (DT < DT0*1e-4) {
        *fopt_hal->rt_active = 0;
    }
}

static void feedopt_update(void *arg, long period)
{
	const double DT0 = period * 1e-9;
//    if (*fopt_hal->opt_us_reset) {
//        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT feedopt.opt-us-reset is TRUE !!!!\n");
//    }

//	if (opt_structs.size < 45) {
//		target_DT = DT0 * max(opt_structs.size, 2) / 45;
//	}

    if (*fopt_hal->opt_rt_reset) {
        should_accept_push = false;
    }

    switch(state) {
    case FEEDOPT_STATE_INACTIVE: feedopt_state_inactive(); break;
    case FEEDOPT_STATE_RUNNING: feedopt_state_running(DT0); break;
    case FEEDOPT_STATE_PAUSED: feedopt_state_paused(DT0); break;
    case FEEDOPT_STATE_STOPPING: feedopt_state_stopping(DT0); break;
    }

    DT = DT * 0.998 + target_DT * 0.002;

	*fopt_hal->pin_step_dt = DT;
	*fopt_hal->queue_size = opt_structs.size;
	*fopt_hal->pin_ready_out = opt_structs.size > 0;

    *fopt_hal->rt_pause = 0;
    *fopt_hal->rt_start = 0;
    *fopt_hal->opt_rt_reset = 0;
    *fopt_hal->opt_us_reset = 0;
    *fopt_hal->rt_single_shot = 0;
    *fopt_hal->rt_has_segment = CurOptStruct.CurvStruct.Type != 0;
}

static int init_splines()
{
	rtapi_print_msg(RTAPI_MSG_ERR,
				   "init_splines with config: \n"
				   "    SplineDegree = %d\n"
				   "    NBreak       = %d\n"
				   "    NDiscr       = %d\n"
				   "    NHorz        = %d\n",
				   cfg.SplineDegree, cfg.NBreak, cfg.NDiscr, cfg.NHorz);

	c_bspline_create((unsigned long *)&spline_handle, 0.0, 1.0, cfg.SplineDegree, cfg.NBreak);
	u_size[0] = 1;
	u_size[1] = cfg.NDiscr;
	linspace(u, 0.0, 1.0, cfg.NDiscr);
	spline_base_eval((unsigned long *)&spline_handle, cfg.NDiscr, u, &base);

	Bl.n = base.n_coeff;
	Bl.handle = (unsigned long)spline_handle;

	CurOptStruct.CurvStruct.Type = 0;
	NextOptStruct.CurvStruct.Type = 0;

	uk = 0;
	resample_t = 0;

	current_x = 0;
	current_y = 0;
	current_z = 0;
	return 1;
}

static int init_feedopt(int num, feedopt_hal_t *fopt)
{

	int retval, usefp, n;

	FeedoptDefaultConfig(&cfg);

	/* export "standard" pins and params */
	for (n = 0; n < 3; n++) {
		retval = hal_pin_float_newf(__core_hal_user, HAL_OUT, &(fopt->pin_sample_pos_out[n]), comp_id,
									"feedopt.sample-%d", n);
		if (retval != 0) {
			rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.sample-%d' pin export failed\n", n);
			return -EIO;
		}
	}

    if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->rt_active), comp_id, "feedopt.rt-active") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.rt-active' pin export failed\n");
		return -EIO;
	}

    if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->us_active), comp_id, "feedopt.us-active") != 0) {
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.us-active' pin export failed\n");
        return -EIO;
    }

    if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->rt_single_shot), comp_id, "feedopt.rt-single-shot") !=
		0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.rt-single-shot' pin export failed\n");
		return -EIO;
	}

	if (hal_pin_u32_newf(__core_hal_user, HAL_OUT, &(fopt->pin_buffer_underrun_count), comp_id,
						 "feedopt.buffer-underrun-count") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.buffer-underrun-count' pin export failed\n");
		return -EIO;
	}

	if (hal_pin_float_newf(__core_hal_user, HAL_IN, &(fopt->opt_per_second), comp_id, "feedopt.opt-per-second") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.opt-per-second' pin export failed\n");
		return -EIO;
	}

	if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->opt_rt_reset), comp_id, "feedopt.opt-rt-reset") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.opt-rt-reset' pin export failed\n");
		return -EIO;
	}

    if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->opt_us_reset), comp_id, "feedopt.opt-us-reset") != 0) {
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.opt-us-reset' pin export failed\n");
        return -EIO;
    }

    if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->commit_cfg), comp_id, "feedopt.commit-cfg") != 0) {
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.commit-cfg' pin export failed\n");
		return -EIO;
	}

	if (hal_pin_u32_newf(__core_hal_user, HAL_OUT, &(fopt->queue_size), comp_id, "feedopt.queue-size") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.queue-size' pin export failed\n");
		return -EIO;
	}

	if (hal_pin_float_newf(__core_hal_user, HAL_OUT, &(fopt->pin_step_dt), comp_id, "feedopt.step-dt") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.step-dt' pin export failed\n");
		return -EIO;
	}

	if (hal_pin_float_newf(__core_hal_user, HAL_OUT, &(fopt->current_u), comp_id, "feedopt.current-u") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.current-u' pin export failed\n");
		return -EIO;
	}

	if (hal_pin_bit_newf(__core_hal_user, HAL_OUT, &(fopt->pin_ready_out), comp_id, "feedopt.ready") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.ready' pin export failed\n");
		return -EIO;
	}

	if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->rt_start), comp_id, "feedopt.rt-start") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.rt-start' pin export failed\n");
		return -EIO;
	}

    if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->rt_start), comp_id, "feedopt.us-start") != 0) {
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.us-start' pin export failed\n");
        return -EIO;
    }


    if (hal_pin_bit_newf(__core_hal_user, HAL_IN, &(fopt->rt_pause), comp_id, "feedopt.rt-pause") != 0) {
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.rt-pause' pin export failed\n");
        return -EIO;
    }

    if (hal_pin_bit_newf(__core_hal_user, HAL_OUT, &(fopt->rt_has_segment), comp_id, "feedopt.rt-has-segment") != 0) {
        rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.rt-has-segment' pin export failed\n");
        return -EIO;
    }

	if (hal_pin_float_newf(__core_hal_user, HAL_OUT, &(fopt->feedrate_scale), comp_id, "feedopt.feedrate-scale") != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: 'feedopt.feedrate-scale' pin export failed\n");
		return -EIO;
	}

	usefp = 1;

	/* export update function */
	retval = hal_export_funct(__core_hal_user, "feedopt.update", feedopt_update, fopt, usefp, 0, comp_id);
	if (retval != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: ERROR: function export failed\n");
		return retval;
	}

	if (init_splines() != 0) {
#warning Handle gracefuly
	}

	fopt_rg_alloc(&opt_structs, FEEDOPT_RT_QUEUE_SIZE);

	printk("[FEEDOPT]: Initialized\n");

	return 0;
}

/* opencn - This part of code comes from the user space counterpart. */

static int feedopt_user_init(feedopt_connect_args_t *args, int major, int minor)
{
	hal_user_t *hal_user;

	hal_user = find_hal_user_by_dev(major, minor);
	if (!hal_user) {
		hal_user = (hal_user_t *)kmalloc(sizeof(hal_user_t), GFP_ATOMIC);
		if (!hal_user)
			BUG();

		memset(hal_user, 0, sizeof(hal_user_t));

		/* Get the current related PID. */
		hal_user->pid = current->pid;
		hal_user->major = major;
		hal_user->minor = minor;
		hal_user->channel = args->channel;

		add_hal_user(hal_user);
	}

	hal_user->comp_id = hal_init(hal_user, "halfeedopt");

	hal_ready(hal_user, hal_user->comp_id);

	/*if (ret < 0)
  return -EIO;*/

	return 0;
}

int feedopt_open(struct inode *inode, struct file *file) { return 0; }

int feedopt_release(struct inode *inode, struct file *filp) { return 0; }

/*
 * Read a stream feedopt_update and returns the feedopt_update number.
 */
ssize_t feedopt_read(struct file *filp, char __user *buf, size_t len, loff_t *off) { return 0; }

ssize_t feedopt_write(struct file *filp, const char __user *buf, size_t len, loff_t *off)
{
    OptStruct p_curv = *(OptStruct *)buf;
	/*printk(KERN_DEBUG "[FEEDOPT] Trying to push optimized struct\n");*/

    if (!should_accept_push) {
        printk("feedopt_write: RESET received, breaking\n");
        return PushStatus_Finished;
    }

    if(!fopt_rg_push(&opt_structs, p_curv)) {
        schedule();
        return PushStatus_TryAgain;
    }

    //wait_event_interruptible(write_full_wait_queue, fopt_rg_push(&opt_structs, *p_curv));
	/*printk(KERN_DEBUG "[FEEDOPT] Pushed optimized struct \n");*/

    return PushStatus_Success;
}

long feedopt_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int rc = 0, major, minor;
	hal_user_t *hal_user;

	major = imajor(filp->f_path.dentry->d_inode);
	minor = iminor(filp->f_path.dentry->d_inode);

	switch (cmd) {
	case FEEDOPT_IOCTL_CONNECT:
		BUG_ON(minor + 1 > 1);

/* Pure kernel side init */
#warning Check if already present (initialized) ...
		rc = feedopt_app_main(minor, (feedopt_connect_args_t *)arg);

		if (rc) {
			printk("%s: failed to initialize...\n", __func__);
			goto out;
		}

		/* Initialization for this process instance. */
		rc = feedopt_user_init((feedopt_connect_args_t *)arg, major, minor);

		break;

	case FEEDOPT_IOCTL_DISCONNECT:

		hal_user = find_hal_user_by_dev(major, minor);
		BUG_ON(hal_user == NULL);
		feedopt_app_exit();

		break;

	case FEEDOPT_IOCTL_WRITE_CFG: {
		FeedoptConfigStruct *cfg_ptr = (FeedoptConfigStruct *)arg;
		cfg = *cfg_ptr;
        *fopt_hal->rt_active = 0;
        state = FEEDOPT_STATE_INACTIVE;
		rtapi_print_msg(RTAPI_MSG_ERR, "FEEDOPT: Read new configuration\n");
		init_splines();
		fopt_rg_clear(&opt_structs);
        should_accept_push = true;
		/* Tell the US counterpart to read the configuration */
		//		*fopt_hal->us_read_cfg = 1;
	} break;
	case FEEDOPT_IOCTL_RESET: {
        // when a reset is issued, the system could be in the slowdown phase, so we wait until it stops and reports that
        // RT is inactive
        *fopt_hal->opt_rt_reset = 1;
        should_accept_push = false;
        while(*fopt_hal->rt_active) {
            schedule();
        }
		feedopt_reset();
	} break;

	case FEEDOPT_IOCTL_READ_CFG: {
		//		feedopt_opt_config* cfg_ptr = (feedopt_opt_config*)arg;
		//		*cfg_ptr = cfg;
	} break;
	}
out:
	return rc;
}

struct file_operations feedopt_fops = {
	.owner = THIS_MODULE,
	.open = feedopt_open,
	.release = feedopt_release,
	.unlocked_ioctl = feedopt_ioctl,
	.read = feedopt_read,
	.write = feedopt_write,
};

int feedopt_comp_init(void)
{

	int rc;

	printk("OpenCN: feedopt subsystem initialization.\n");

	/* Registering device */
	rc = register_chrdev(FEEDOPT_DEV_MAJOR, FEEDOPT_DEV_NAME, &feedopt_fops);
	if (rc < 0) {
		printk("Cannot obtain the major number %d\n", FEEDOPT_DEV_MAJOR);
		return rc;
	}

	return 0;
}

late_initcall(feedopt_comp_init)
