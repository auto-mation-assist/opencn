#include <opencn/gsl/config.h>

#include <opencn/gsl/gsl_errno.h>
#include <opencn/gsl/gsl_matrix.h>

#define BASE_GSL_COMPLEX_LONG
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_GSL_COMPLEX_LONG

#define BASE_GSL_COMPLEX
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_GSL_COMPLEX

#define BASE_GSL_COMPLEX_FLOAT
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_GSL_COMPLEX_FLOAT

#define BASE_LONG_DOUBLE
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_LONG_DOUBLE

#define BASE_DOUBLE
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_DOUBLE

#define BASE_FLOAT
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_FLOAT

#define BASE_ULONG
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_ULONG

#define BASE_LONG
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_LONG

#define BASE_UINT
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_UINT

#define BASE_INT
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_INT

#define BASE_USHORT
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_USHORT

#define BASE_SHORT
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_SHORT

#define BASE_UCHAR
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_UCHAR

#define BASE_CHAR
#include <opencn/gsl/templates_on.h>
#include "init_source.c"
#include <opencn/gsl/templates_off.h>
#undef  BASE_CHAR
