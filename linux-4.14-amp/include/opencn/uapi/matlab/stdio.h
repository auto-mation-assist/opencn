#ifndef STDIO_H
#define STDIO_H

#ifdef __KERNEL__

#include <opencn/ctypes/strings.h>

#define printf(...) opencn_printf(__VA_ARGS__)
#define fflush(stream)

#endif

#endif /* STDIO_H */
