
#ifndef VDUMMY_FRONT_H
#define VDUMMY_FRONT_H

void vdummy_init(void);

void vdummy_send_data(char *buffer);

#endif /* VDUMMY_FRONT_H */
