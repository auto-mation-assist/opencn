#ifndef __GSL_VECTOR_H__
#define __GSL_VECTOR_H__

#include <opencn/gsl/gsl_vector_complex_long_double.h>
#include <opencn/gsl/gsl_vector_complex_double.h>
#include <opencn/gsl/gsl_vector_complex_float.h>

#include <opencn/gsl/gsl_vector_long_double.h>
#include <opencn/gsl/gsl_vector_double.h>
#include <opencn/gsl/gsl_vector_float.h>

#include <opencn/gsl/gsl_vector_ulong.h>
#include <opencn/gsl/gsl_vector_long.h>

#include <opencn/gsl/gsl_vector_uint.h>
#include <opencn/gsl/gsl_vector_int.h>

#include <opencn/gsl/gsl_vector_ushort.h>
#include <opencn/gsl/gsl_vector_short.h>

#include <opencn/gsl/gsl_vector_uchar.h>
#include <opencn/gsl/gsl_vector_char.h>


#endif /* __GSL_VECTOR_H__ */
