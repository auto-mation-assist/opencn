
#ifndef VDUMMY_BACK_H
#define VDUMMY_BACK_H

#include <soo/dev/vdummy.h>

void probe_vdummyback(vdummy_sring_t *sring, uint32_t evtchn);

#endif /* VDUMMY_BACK_H */
