#include "tools.hpp"

const char *type_to_str(uint8_t type)
{
    switch (type) {
    case Line:
        return "Line ";
    case Helix:
        return "Helix";
    case TransP5:
        return "Trans";
    default:
        return "??   ";
    }
}

void print_vector(const double *values, int N)
{
    printf("( ");
    for (int i = 0; i < N; i++)
        printf("%.2f ", values[i]);
    printf(")");
}

// std::ostream &operator<<(std::ostream &out, const CurvStruct &cs)
//{
//    out << "{ t = " << type_to_str(cs.Type) << ", feedrate = " << cs.FeedRate;
//    switch (cs.Type) {
//    case 1: // Line
//        out << ", points = ";
//        print_vector(out, cs.P0, 3) << "->";
//        print_vector(out, cs.P1, 3);
//        break;
//    }
//    out << " }";
//    return out;
//}

void alloc_spline_base(int n_sample, int n_coeff, emxArray_real_T **BasisVal,
                       emxArray_real_T **BasisValD, emxArray_real_T **BasisValDD,
                       emxArray_real_T **BasisIntegr)
{

    *BasisVal = emxCreate_real_T(n_sample, n_coeff);
    *BasisValD = emxCreate_real_T(n_sample, n_coeff);
    *BasisValDD = emxCreate_real_T(n_sample, n_coeff);
    *BasisIntegr = emxCreate_real_T(n_coeff, 1);
}

void spline_base_eval(uint64_t *handle, size_t n_sample, double *x, SplineBase &base)
{
    base.n_sample = n_sample;
    base.n_coeff = c_bspline_ncoeff(handle);
    base.size_val[0] = base.n_sample;
    base.size_val[1] = base.n_coeff;
    base.size_integr[0] = base.n_coeff;
    base.size_integr[1] = 1;
    c_bspline_base_eval(handle, n_sample, x, base.BasisVal, base.BasisValD, base.BasisValDD,
                        base.BasisIntegr);
}
