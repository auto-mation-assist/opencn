#include "static_test.hpp"
#include "time_prof.h"
#include "tools.hpp"
#include <vector>

static const int N_COEFF_MAX = 50;
static const int MAX_QUEUE = 5;
static const int MAX_DISCR = 100;

#define CONSTR_U 1
#define CONSTR_POINTS 0
#define U_IN_PLANE 1

void static_test()
{
    PROF_START("/tmp/static_test.prof");
    {
        PROF_BLOCK(main);
        PROF_IN(prepare_workspace);
        const int Npoints = 5;
        const double points_data[Npoints * 3] = {0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0};
        int points_size[2] = {3, Npoints};
        double vmax = 1.5;
        double amax[3] = {4.5, 4.5, 4.5};
        double jmax[3] = {100, 100, 100};
        double v_0 = 0.2;
        double at_0 = 0.0;
        double v_1 = 0.2;
        double at_1 = 0.0;
        double cutoff = 0.2;

        SplineBase base;

        const int Ndiscr = 100;
        const int nbreak = 25;
        const int Nhorz = 3;
        const int MAX_CURVE_STRUCTS = 20;

        CurvStruct curv_structs[Npoints - 1];
        CurvStruct opt_curv_structs[(Npoints - 1) * 2 - 1];
        double Coeff[N_COEFF_MAX];
        int coeff_size[2];
        double u[MAX_DISCR];
        int size_u[2];

        //        int input_segment_size[2];

#if CONSTR_POINTS
        //        curv_structs.resize(Npoints - 1);
        for (int i = 0; i < Npoints - 1; i++) {
            ConstrLineStruct(points_data + (3 * i), points_data + (3 * (i + 1)), Feedrate,
                             &curv_structs[i]);
        }
        int input_segment_size[2] = {1, Npoints - 1};
#elif CONSTR_U
        double P0[3] = {-1, 0, 0};
        double P1[3] = {0, 0, 0};
        double P2[3] = {0, 1, 0};
        double P3[3] = {-1, 1, 0};
        double evec[3] = {0, 0, 1};
        double pitch = 0;
        double theta = M_PI;
#if !U_IN_PLANE
        P2[2] = 1;
        P3[3] = 1;
#endif
        ConstrLineStruct(P0, P1, vmax, &curv_structs[0]);
        ConstrHelixStruct(P1, P2, evec, theta, pitch, vmax, &curv_structs[1]);
        ConstrLineStruct(P2, P3, vmax, &curv_structs[2]);

        int input_segment_size[2] = {1, 3};
#endif

        /****************** SPLINE BASE ********************/
        const double x0 = 0.0, x1 = 1.0;
        bspline_t *spline_handle;
        c_bspline_create(reinterpret_cast<uint64_t *>(&spline_handle), x0, x1, 4, nbreak);
        const int Ncoeff = gsl_bspline_ncoeffs(spline_handle->ws);

        //    emxArray_real_T *u = emxCreate_real_T(0, 0);
        c_linspace(x0, x1, Ndiscr, u, size_u);

        //    alloc_spline_base(Ndiscr, Ncoeff, &BasisVal, &BasisValD, &BasisValDD, &BasisIntegr);

        {
            PROF_BLOCK(spline_base_eval);
            spline_base_eval(reinterpret_cast<uint64_t *>(&spline_handle), Ndiscr, u, base);
        }
        /****************** SPLINE BASE *********************/

        int output_segment_size[2];
        //    SmoothCurvStructs(curv_structs, 0.2, vmax, smoothed_segments);
        SmoothCurvStructs(curv_structs, input_segment_size, cutoff, vmax, opt_curv_structs,
                          output_segment_size);

        PROF_OUT(prepare_workspace);

        struct0_T Bl;
        Bl.n = base.n_coeff;
        Bl.handle = (uint64_t)spline_handle;

        for (int i = 0; i < output_segment_size[1] - 1; i++) {
            //            RecedingHorizon(smoothed_segments, amax, jmax, v_0, at_0, v_1, at_1,
            //            BasisVal,
            //                            BasisValD, BasisValDD, BasisIntegr, &Bl, u, Coeff);
            coeff_size[0] = base.n_coeff;
            coeff_size[1] = Nhorz;
            int opt_size[2] = {output_segment_size[0], output_segment_size[1] - i};
            FeedratePlanning_v4(                    //
                opt_curv_structs + i, opt_size,     //
                amax, jmax, &v_0, &at_0, v_1, at_1, //
                base.BasisVal, base.size_val,       //
                base.BasisValD, base.size_val,      //
                base.BasisValDD, base.size_val,     //
                base.BasisIntegr, base.size_integr, //
                &Bl, u, size_u, Nhorz, Coeff, coeff_size);

            DBG(*Coeff);
            std::cout << "Coeff: \n";
            print_vector(Coeff, coeff_size[0] * coeff_size[1]);
            std::cout << std::endl;
        }
    }
    PROF_END();
}
