#pragma once

#include <iostream>

extern "C" {
#include "SmoothCurvStructs.h"
#include "SmoothCurvStructs_emxAPI.h"
}

#define FILENAME (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

//#define DBG(expr)                                                                                  \
//    (std::cout << FILENAME << ":" << __LINE__ << " : " #expr " = " << expr << std::endl, expr)

#define DBG(expr)

static const int SPLINE_COEFF_MAX = 50;
static const int SPLINE_DISCR_MAX = 100;
static const int SPLINE_ELEM_MAX = SPLINE_COEFF_MAX * SPLINE_DISCR_MAX;

struct SplineBase {
    double BasisVal[SPLINE_ELEM_MAX];
    double BasisValD[SPLINE_ELEM_MAX];
    double BasisValDD[SPLINE_ELEM_MAX];
    double BasisIntegr[SPLINE_COEFF_MAX];
    size_t n_sample = 0, n_coeff = 0;
    int size_val[2];
    int size_integr[2];
};

const char *type_to_str(uint8_t type);

std::ostream &operator<<(std::ostream &out, const emxArray_real_T &m);

void print_vector(const double *values, int N);

std::ostream &operator<<(std::ostream &out, const CurvStruct &cs);

void alloc_spline_base(int n_sample, int n_coeff, emxArray_real_T **BasisVal,
                       emxArray_real_T **BasisValD, emxArray_real_T **BasisValDD,
                       emxArray_real_T **BasisIntegr);

void spline_base_eval(uint64_t *handle, size_t n_sample, double *xvec, SplineBase &base);
